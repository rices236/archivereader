################################################################################################################################
# Makefile
#
# This makefile supporting 4 builds (targets):
#   1. Debug . . . . The debug-enabled build of the main executable.
#   2. Release . . . The regular build of the main executable.
#   3. DebugTest . . The debug-enabled unit test build.
#   4. ReleaseTest . The regular unit test build.
#
# In order to make maintenance easier, these builds do not support the mapping of dependent files to objects. Each target build
# starts by cleaning out previous binary and object files, and then re-builds everything.
################################################################################################################################

################################################################################################################################
# Project General Settings
################################################################################################################################

CXX            := g++
CXXFLAGS       := -Weffc++ -Wall -std=c++17 -fexceptions -pthread
HEADER_DIR     := src/header
SOURCE_DIR     := src/source
TEST_DIR       := src/test
PROTO_HDR_DIR  := proto/header
PROTO_OBJ_DIR  := proto/obj
TEST_HDR_DIR   := src/test
BUILD_DIR      := build/mk
OBJDIR         := $(BUILD_DIR)/obj
BINDIR         := $(BUILD_DIR)/bin
LDFLAGS        := -pthread
LDLIBS         := $(addprefix -l, \
	boost_filesystem \
	boost_program_options \
	wgdb \
	grpc++ \
	grpc \
	protobuf \
	grpc++_reflection \
	dl \
)
TEST_LDLIBS    := $(addprefix -l, \
	gtest \
	gtest_main \
)

################################################################################################################################


################################################################################################################################
# Object General Settings
################################################################################################################################

# The main entry point for non-test targets
MAIN_OBJ := src/source/main.o

# The main entry point for test targets
MAIN_TEST_OBJ := src/test/main_test.o

# Objects common to all targets
ALL_TARGET_OBJS := $(addprefix src/, \
	source/archive_reader/config.o \
	source/archive_reader/file_operations.o \
	source/archive_reader/file_type.o \
	source/archive_reader/interrupt/interrupt.o \
	source/archive_reader/json_helper.o \
	source/archive_reader/library_scanner.o \
	source/archive_reader/logger/logger.o \
	source/archive_reader/release.o \
	source/archive_reader/release_scan_handler.o \
	source/archive_reader/service/info_service_impl.o \
	source/archive_reader/service/search_service_impl.o \
	source/archive_reader/service/service.o \
	source/archive_reader/service/util.o \
	source/archive_reader/track.o \
	source/archive_reader/whitedb/condition.o \
	source/archive_reader/whitedb/database.o \
	source/archive_reader/whitedb/query.o \
	source/archive_reader/whitedb/query_parameter_list.o \
	source/archive_reader/whitedb/record/base_record.o \
	source/archive_reader/whitedb/record/release.o \
	source/archive_reader/whitedb/record/track.o \
	source/archive_reader/whitedb/record_handle.o \
	source/archive_reader/whitedb/table_id.o \
	source/archive_reader/whitedb/whitedb_writer.o \
)

# Pre-built gRPC objects
PROTO_GRPC_OBJS := $(addprefix $(PROTO_OBJ_DIR)/, \
	archive_reader/info_service.grpc.pb.o \
	archive_reader/info_service.pb.o \
	archive_reader/search_service.grpc.pb.o \
	archive_reader/search_service.pb.o \
)

# Objects for test targets
TEST_TARGET_OBJS := $(addprefix src/, \
	test/archive_reader/config_test.o \
	test/archive_reader/file_type_test.o \
	test/archive_reader/json_helper_test.o \
	test/archive_reader/release_test.o \
	test/archive_reader/track_test.o \
	test/archive_reader/whitedb/condition_test.o \
	test/archive_reader/whitedb/database_test.o \
	test/archive_reader/whitedb/field_test.o \
	test/archive_reader/whitedb/record/release_test.o \
	test/archive_reader/whitedb/record/track_test.o \
	test/archive_reader/whitedb/record_handle_test.o \
	test/archive_reader/whitedb/test_helper.o \
)

################################################################################################################################


################################################################################################################################
# Debug Settings
################################################################################################################################

DBG_TARGET     := debug
DBG_BINDIR     := $(BINDIR)/
DBG_BIN        := $(DBG_BINDIR)archive-readerd
DBG_OBJDIR     := $(OBJDIR)/$(DBG_TARGET)
DBG_OBJS       := $(addprefix $(DBG_OBJDIR)/, $(MAIN_OBJ) $(ALL_TARGET_OBJS))
DBG_OBJDIRS    := $(sort $(dir $(DBG_OBJS)))
DBG_CXXFLAGS   := -g -O0 -DDEBUG
DBG_LDFLAGS    := $(LDFLAGS)
DBG_LDLIBS     := $(LDLIBS)

################################################################################################################################


################################################################################################################################
# Release Settings
################################################################################################################################

REL_TARGET     := release
REL_BINDIR     := $(BINDIR)/
REL_BIN        := $(REL_BINDIR)archive-reader
REL_OBJDIR     := $(OBJDIR)/$(REL_TARGET)
REL_OBJS       := $(addprefix $(REL_OBJDIR)/, $(MAIN_OBJ) $(ALL_TARGET_OBJS))
REL_OBJDIRS    := $(sort $(dir $(REL_OBJS)))
REL_CXXFLAGS   := -O2
REL_LDFLAGS    := $(LDFLAGS) -s
REL_LDLIBS     := $(LDLIBS)

################################################################################################################################


################################################################################################################################
# DebugTest Settings
################################################################################################################################

DBT_TARGET     := debug_test
DBT_BINDIR     := $(BINDIR)/
DBT_BIN        := $(DBT_BINDIR)archive-reader-testd
DBT_OBJDIR     := $(OBJDIR)/$(DBT_TARGET)
DBT_OBJS       := $(addprefix $(DBT_OBJDIR)/, $(MAIN_TEST_OBJ) $(ALL_TARGET_OBJS) $(TEST_TARGET_OBJS))
DBT_OBJDIRS    := $(sort $(dir $(DBT_OBJS)))
DBT_CXXFLAGS   := -g -O0 -DDEBUG -DTEST
DBT_LDFLAGS    := $(LDFLAGS)
DBT_LDLIBS     := $(LDLIBS) $(TEST_LDLIBS)

################################################################################################################################


################################################################################################################################
# ReleaseTest Settings
################################################################################################################################

RLT_TARGET     := release_test
RLT_BINDIR     := $(BINDIR)/
RLT_BIN        := $(RLT_BINDIR)archive-reader-test
RLT_OBJDIR     := $(OBJDIR)/$(RLT_TARGET)
RLT_OBJS       := $(addprefix $(RLT_OBJDIR)/, $(MAIN_TEST_OBJ) $(ALL_TARGET_OBJS) $(TEST_TARGET_OBJS))
RLT_OBJDIRS    := $(sort $(dir $(RLT_OBJS)))
RLT_CXXFLAGS   := -O2 -DTEST
RLT_LDFLAGS    := $(LDFLAGS) -s
RLT_LDLIBS     := $(LDLIBS) $(TEST_LDLIBS)

################################################################################################################################


.PHONY : all clean version \
         debug debugobjs dbgobjprep dbgbinprep dbgclean \
         release releaseobjs relobjprep relbinprep relclean \
         debugtest debugtestobjs dbtobjprep dbtbinprep dbtclean \
         releasetest releasetestobjs rltobjprep rltbinprep rltclean \
         print

all : debug release debugtest releasetest


################################################################################################################################
# Debug Rules
################################################################################################################################

# Debug binary build
debug : dbgclean $(DBG_BIN)

$(DBG_BIN) : debugobjs | dbgbinprep
	$(CXX) -o $(DBG_BIN) $(DBG_OBJS) $(PROTO_GRPC_OBJS) $(DBG_LDFLAGS) $(DBG_LDLIBS)

# Debug object build
debugobjs : $(DBG_OBJS)

$(DBG_OBJDIR)/%.o : | dbgobjprep
	$(CXX) $(CXXFLAGS) $(DBG_CXXFLAGS) -I$(HEADER_DIR) -I$(PROTO_HDR_DIR) -c $*.cpp -o $@

# Debug object prep
dbgobjprep : $(DBG_OBJDIRS)

# Debug binary prep
dbgbinprep : $(DBG_BINDIR)

dbgclean:
	rm -f $(DBG_OBJS) $(DBG_BIN)

################################################################################################################################


################################################################################################################################
# Release Rules
################################################################################################################################

# Release binary build
release : relclean $(REL_BIN)

$(REL_BIN) : releaseobjs | relbinprep
	$(CXX) -o $(REL_BIN) $(REL_OBJS) $(PROTO_GRPC_OBJS) $(REL_LDFLAGS) $(REL_LDLIBS)

# Release object build
releaseobjs : $(REL_OBJS)

$(REL_OBJDIR)/%.o : | relobjprep
	$(CXX) $(CXXFLAGS) $(REL_CXXFLAGS) -I$(HEADER_DIR) -I$(PROTO_HDR_DIR) -c $*.cpp -o $@

# Release object prep
relobjprep : $(REL_OBJDIRS)

# Release binary prep
relbinprep : $(REL_BINDIR)

relclean:
	rm -f $(REL_OBJS) $(REL_BIN)

################################################################################################################################


################################################################################################################################
# DebugTest Rules
################################################################################################################################

# DebugTest binary build
debugtest : dbtclean $(DBT_BIN)

$(DBT_BIN) : debugtestobjs | dbtbinprep
	$(CXX) -o $(DBT_BIN) $(DBT_OBJS) $(PROTO_GRPC_OBJS) $(DBT_LDFLAGS) $(DBT_LDLIBS)

# Debug object build
debugtestobjs : $(DBT_OBJS)

$(DBT_OBJDIR)/%.o : | dbtobjprep
	$(CXX) $(CXXFLAGS) $(DBT_CXXFLAGS) -I$(HEADER_DIR) -I$(PROTO_HDR_DIR) -I$(TEST_HDR_DIR) -c $*.cpp -o $@

# DebugTest object prep
dbtobjprep : $(DBT_OBJDIRS)

# DebugTest binary prep
dbtbinprep : $(DBT_BINDIR)

dbtclean:
	rm -f $(DBT_OBJS) $(DBT_BIN)

################################################################################################################################


################################################################################################################################
# ReleaseTest Rules
################################################################################################################################

# ReleaseTest binary build
releasetest : rltclean $(RLT_BIN)

$(RLT_BIN) : releasetestobjs | rltbinprep
	$(CXX) -o $(RLT_BIN) $(RLT_OBJS) $(PROTO_GRPC_OBJS) $(RLT_LDFLAGS) $(RLT_LDLIBS)

# Release object build
releasetestobjs : $(RLT_OBJS)

$(RLT_OBJDIR)/%.o : | rltobjprep
	$(CXX) $(CXXFLAGS) $(RLT_CXXFLAGS) -I$(HEADER_DIR) -I$(PROTO_HDR_DIR) -I$(TEST_HDR_DIR) -c $*.cpp -o $@

# ReleaseTest object prep
rltobjprep : $(RLT_OBJDIRS)

# ReleaseTest binary prep
rltbinprep : $(RLT_BINDIR)

rltclean:
	rm -f $(RLT_OBJS) $(RLT_BIN)

################################################################################################################################


################################################################################################################################
# Generic Rules
################################################################################################################################

# Generic directory builders
$(OBJDIR)/%/ :
	mkdir -p $@

$(BINDIR)/ :
	mkdir -p $@

version :
	./script/make_version.sh

clean : dbgclean relclean dbtclean rltclean

################################################################################################################################


################################################################################################################################
# Maintenance Rules (rules to aid in maintaining or debugging this makefile)
################################################################################################################################

# Generic directory builders
print :
	@echo DBG_OBJS $(DBG_OBJS)
	@echo DBG_OBJDIR $(DBG_OBJDIR)

################################################################################################################################
