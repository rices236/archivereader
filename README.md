# ArchiveReader

This is a service component of the Audimation project. Its responsibilities are to scan the library archive at startup, store the information, and provide services  It will also provide archive information to client services.

## Project structure:
- **./proto** C++ header and object files generated by the protocol buffer compiler. Do not edit this code directly.
- **./script** Project shell scripts.
- **./src** Service C++ code (header, source, and test) is under this directory.
- **./src/header** Service header (.hpp) files.
- **./src/source** Service source (.cpp) files.
- **./src/test** Unit tests for service components (mostly .cpp, but also .hpp when needed).

## How to build:

Run the following commands to build with GNU Make:
```
  $ make version
  $ make release
```

A good IDE to use with this project is Code::Blocks v20.03. See http://www.codeblocks.org. **ArchiveReader.cbp** is the Code::Blocks project file.
