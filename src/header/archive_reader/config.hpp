/*******************************************************************************************************************************
 * src/header/archive_reader/config.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_CONFIG_HPP_
#define ARCHIVE_READER_CONFIG_HPP_

#include "archive_reader/config_interface.hpp"

namespace archive_reader {

/**
 * Read configuration options from command arguments.
 **/
class Config : public ConfigInterface
{
public:

  /**
   * Class constructor.
   *
   * @param argc Value to be passed from main() argument count.
   * @param argv Value to be passed from main() argument values.
   */
  Config(int argc, char** argv);

  /**
   * Class copy constructor.
   *
   * @param other The object containing the data to copy.
   */
  Config(const Config& other);

  /**
   * Class move constructor.
   *
   * @param other The object containing the data to move.
   */
  Config(Config&& other);

  /**
   * Class destructor.
   */
  ~Config() = default;

  /**
   * Class copy assignment operator.
   *
   * @param other The object containing the data to copy.
   */
  Config& operator=(const Config& other);

  /**
   * Class move assignment operator.
   *
   * @param other The object containing the data to move.
   */
  Config& operator=(Config&& other);

  /**
   * Get the library root value.
   */
  std::string library_root() const noexcept override;

  /**
   * Get the release file value.
   */
  std::string release_file() const noexcept override;

  /**
   * Get the WhiteDB ID value.
   */
  int db_id() const noexcept override;

  /**
   * Get the WhiteDB database size in bytes.
   */
  int db_size() const noexcept override;

  /**
   * Get the service IP value.
   */
  std::string service_ip() const noexcept override;

  /**
   * Get the service port value.
   */
  int service_port() const noexcept override;

  /**
   * Get the default release file value.
   */
  static std::string release_file_default() noexcept;

  /**
   * Get the default WhiteDB ID value.
   */
  static int db_id_default() noexcept;

  /**
   * Get the default WhiteDB database size in bytes.
   */
  static int db_size_default() noexcept;

  /**
   * Get the default service IP value.
   */
  static std::string service_ip_default() noexcept;

  /**
   * Get the default service port value.
   */
  static int service_port_default() noexcept;

private:
  std::string _library_root;
  std::string _release_file;
  int _db_id;
  int _db_size;
  std::string _service_ip;
  int _service_port;

  static std::string _release_file_default;
  static int _db_id_default;
  static int _db_size_default;
  static std::string _service_ip_default;
  static int _service_port_default;

};

} // namespace archive_reader

#endif // ARCHIVE_READER_CONFIG_HPP_
