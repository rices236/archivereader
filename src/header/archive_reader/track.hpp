/*******************************************************************************************************************************
 * src/header/archive_reader/track.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_TRACK_HPP_
#define ARCHIVE_READER_TRACK_HPP_

#include <ostream>
#include <string>
#include <vector>

#include <boost/filesystem/path.hpp>
#include <nlohmann/json.hpp>

namespace archive_reader {

typedef std::vector<std::string> str_list;

/**
 * Read from JSON object and store track information.
 */
class Track
{
public:

  /**
   * Default constructor.
   */
  Track();

  /**
  * Get the title.
  */
  std::string title() const noexcept;

  /**
   * Get the path of the track audio file relative to the release file.
   */
  std::string path() const noexcept;

  /**
   * Get the list of artists.
   */
  str_list artists() const noexcept;

  /**
   * Get the composer.
   */
  std::string composer() const noexcept;

  /**
   * Get the musical genre.
   */
  std::string genre() const noexcept;

  /**
   * Get the list of musical styles.
   */
  str_list styles() const noexcept;

  /**
   * Get the record label.
   */
  std::string label() const noexcept;

  /**
   * Get the original year of issue.
   */
  int original_year() const noexcept;

  /**
   * Get the year of the performance.
   */
  int performance_year() const noexcept;

  /**
   * Get the comments.
   */
  str_list comments() const noexcept;

  /**
   * Get the full path of the track audio file.
   */
  std::string track_file_path() const noexcept;

  /**
   * Supports syntax such as: Track my_track = j.get<Track>(); where 'j' is an nlohmann::json object.
   *
   * @param j A json object populated with Track information.
   * @param t A reference to a Track object to be written to.
   */
  friend void from_json(const nlohmann::json& j, Track& t);

  /**
   * Supports adding additional information from the path to the Track object.
   *
   * @param path The current path of the release data file.
   * @param t A reference to a Track object to be written to.
   */
  friend void from_path(const boost::filesystem::path& path, Track& t);

  /**
   * Supports streaming Track information to text suitable for logging and debugging.
   *
   * @param os The output stream to write to.
   * @param t The Track object whose information will be written to the stream.
   * @return A reference to the output stream passed in the parameter.
   */
  friend std::ostream& operator<<(std::ostream& os, const Track& t);

private:
  std::string _title;
  std::string _path;
  str_list _artists;
  std::string _composer;
  std::string _genre;
  str_list _styles;
  std::string _label;
  int _original_year;
  int _performance_year;
  str_list _comments;
  std::string _track_file_path;

};

} // namespace archive_reader

#endif // ARCHIVE_READER_TRACK_HPP_
