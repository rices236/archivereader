/*******************************************************************************************************************************
 * src/header/archive_reader/json_helper.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_JSON_HELPER_HPP_
#define ARCHIVE_READER_JSON_HELPER_HPP_

#include <string>
#include <vector>

#include <nlohmann/json.hpp>

namespace archive_reader {

typedef std::vector<std::string> str_list;

/**
 * Get the value of a required JSON string field from a JSON object.
 *
 * @param j A populated JSON object.
 * @param key The field key or name.
 * @return The value of the field identified by the key.
 * @throw JsonSchemaException Thrown if the field does not exist, the value is null, or the value is not a string.
 */
std::string required_str_field(const nlohmann::json& j, const std::string& key);

/**
 * Get the value of an optional JSON string or string array field from a JSON object.
 *
 * @param j A populated JSON object.
 * @param skey The field singular key or name, as in "artist" or "style."
 * @param pkey The field plural key or name, as in "artists" or "styles."
 * @return The value of the field identified by the key, if present; otherwise an empty string.
 * @throw JsonSchemaException Thrown under any of the following conditions: if both singular and plural keys exist; if the
 *        field is singular-keyed and contains array values; if the field is plural-keyed and contains a non-array string
 *        value.
 */
str_list optional_plural_str_field(const nlohmann::json& j, const std::string& skey, const std::string& pkey);

/**
 * Get the value of an optional JSON string field from a JSON object.
 *
 * @param j A populated JSON object.
 * @param key The field key or name.
 * @param default_value The value to return if the key was not found.
 * @return The value of the field identified by the key, if present; otherwise the default value.
 * @throw JsonSchemaException Thrown if the field value is not a string type.
 */
std::string optional_str_field(const nlohmann::json& j, const std::string& key, const std::string& default_value = "");

/**
 * Get the value of an optional JSON unsigned integer field from a JSON object.
 *
 * Note that the returned type of integer is not unsigned. Unsigned is the requirement for the JSON data type.
 *
 * @param j A populated JSON object.
 * @param key The field key or name.
 * @param default_value The value to return if the key was not found.
 * @return The value of the field identified by the key, if present; otherwise the default value.
 * @throw JsonSchemaException Thrown if the field value is not an unsigned number type.
 */
int optional_uint_field(const nlohmann::json& j, const std::string& key, const int default_value = 0);

/**
 * Get the value of an optional JSON Boolean field from a JSON object.
 *
 * @param j A populated JSON object.
 * @param key The field key or name.
 * @param default_value The value to return if the key was not found.
 * @return The value of the field identified by the key, if present; otherwise the default value.
 * @throw JsonSchemaException Thrown if the field value is not a Boolean type.
 */
bool optional_bool_field(const nlohmann::json& j, const std::string& key, const bool default_value = false);

} // namespace archive_reader

#endif // ARCHIVE_READER_JSON_HELPER_HPP_
