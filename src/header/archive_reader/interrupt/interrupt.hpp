/*******************************************************************************************************************************
 * src/header/archive_reader/interrupt/interrupt.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_INTERRUPT_INTERRUPT_HPP_
#define ARCHIVE_READER_INTERRUPT_INTERRUPT_HPP_

namespace archive_reader {
namespace interrupt {

/**
 * Set up interrupt handlers.
 */
void hook_signals();

/**
 * Wait for an interrupt signal.
 */
void wait_for_signal();

} // namespace interrupt
} // namespace archive_reader

#endif // ARCHIVE_READER_INTERRUPT_INTERRUPT_HPP_
