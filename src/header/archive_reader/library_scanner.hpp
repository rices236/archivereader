/*******************************************************************************************************************************
 * src/header/archive_reader/library_scanner.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_LIBRARY_SCANNER_HPP_
#define ARCHIVE_READER_LIBRARY_SCANNER_HPP_

#include <string>

#include "archive_reader/config_interface.hpp"

namespace archive_reader {

/**
 * Performs recursive scans on the library, reading all release information.
 */
class LibraryScanner
{
public:

  /**
   * Class constructor.
   * @param config The configuration object.
   */
  LibraryScanner(const ConfigInterface& config);

  /**
   * Scan for and process release files in the library path.
   */
  void scan();

private:
  const std::string _library_root;
  const std::string _release_file;
  const int _db_id;
  const int _db_size;

  LibraryScanner() = delete;
  LibraryScanner(const LibraryScanner&) = delete;
  LibraryScanner(LibraryScanner&&) = delete;
  LibraryScanner& operator=(const LibraryScanner&) = delete;
  LibraryScanner& operator=(LibraryScanner&&) = delete;
};

} // namespace archive_reader

#endif // ARCHIVE_READER_LIBRARY_SCANNER_HPP_
