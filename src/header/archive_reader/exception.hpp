/*******************************************************************************************************************************
 * src/header/archive_reader/exception.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_EXCEPTION_HPP_
#define ARCHIVE_READER_EXCEPTION_HPP_

#include <stdexcept>
#include <string>

namespace archive_reader {

class ArchiveReaderException : public std::runtime_error
{
public:
  ArchiveReaderException(const std::string& cause) : std::runtime_error(cause) {}
  ArchiveReaderException(const std::string& cause, const std::exception& e)
    : std::runtime_error(cause + e.what()) {}
};

class LogInitializationException : public ArchiveReaderException
{
public:
  LogInitializationException(const std::string& cause) : ArchiveReaderException(cause) {}
  LogInitializationException(const std::string& cause, const std::exception& e) : ArchiveReaderException(cause, e) {}
};

class PathException : public ArchiveReaderException
{
public:
  PathException(const std::string& cause) : ArchiveReaderException(cause) {}
  PathException(const std::string& cause, const std::exception& e) : ArchiveReaderException(cause, e) {}
};

class FileOpenException : public ArchiveReaderException
{
public:
  FileOpenException(const std::string& cause) : ArchiveReaderException(cause) {}
  FileOpenException(const std::string& cause, const std::exception& e) : ArchiveReaderException(cause, e) {}
};

class JsonParseException : public ArchiveReaderException
{
public:
  JsonParseException(const std::string& cause) : ArchiveReaderException(cause) {}
  JsonParseException(const std::string& cause, const std::exception& e) : ArchiveReaderException(cause, e) {}
};

class JsonSchemaException : public ArchiveReaderException
{
public:
  JsonSchemaException(const std::string& cause) : ArchiveReaderException(cause) {}
  JsonSchemaException(const std::string& cause, const std::exception& e) : ArchiveReaderException(cause, e) {}
};

class ReleaseScanException : public ArchiveReaderException
{
public:
  ReleaseScanException(const std::string& cause) : ArchiveReaderException(cause) {}
  ReleaseScanException(const std::string& cause, const std::exception& e) : ArchiveReaderException(cause, e) {}
};

} // namespace archive_reader

#endif // ARCHIVE_READER_EXCEPTION_HPP_
