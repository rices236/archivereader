/*******************************************************************************************************************************
 * src/header/archive_reader/db_writer.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_DB_WRITER_HPP_
#define ARCHIVE_READER_DB_WRITER_HPP_

#include "archive_reader/release.hpp"

namespace archive_reader {

/**
 * Interface for writing Release information to databases.
 */
class DBWriter
{
public:

  /**
   * Override this method to write a release record to the database.
   *
   * @param rel The release data to be written.
   */
  virtual void write(const Release& rel) = 0;

  /**
   * Class destructor.
   */
  virtual ~DBWriter() = default;
};

} // namespace archive_reader

#endif // ARCHIVE_READER_DB_WRITER_HPP_
