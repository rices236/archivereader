/*******************************************************************************************************************************
 * src/header/archive_reader/file_operations.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_FILE_OPERATIONS_HPP_
#define ARCHIVE_READER_FILE_OPERATIONS_HPP_

#include <string>

#include <boost/filesystem/operations.hpp>
#include <nlohmann/json.hpp>

#include "archive_reader/exception.hpp"

namespace archive_reader {

/**
 * Recursively scans the filesystem from a given directory.
 *
 * The handler is a functor that accepts a directory_entry and returns a bool. Scanning will continue until all paths have been
 * handled, or until the handler returns false.
 *
 * @param root The directory to start from.
 * @param handler A functor that can be used to perform operations during the scan.
 * @throws archrdr::PathException Thrown if the path does not exist or the path is not a directory.
 */
template <typename T>
void scan_recursive(const boost::filesystem::path& root, T& handler)
{
  if (!boost::filesystem::exists(root)) {
    throw PathException("Path does not exist: " + std::string(root.c_str()));
  }

  if (!boost::filesystem::is_directory(root)) {
    throw PathException("Path is not a directory: " + std::string(root.c_str()));
  }

  boost::filesystem::recursive_directory_iterator iend;
  for (boost::filesystem::recursive_directory_iterator itr(root); itr != iend; ++itr) {
    if (!handler(*itr)) {
      // Stop scanning if handler returns false
      break;
    }
  }
}

/**
 * Opens and reads a JSON file into a JSON object.
 *
 * @param file_path The path of the file to open.
 * @return A populated JSON object.
 * @throws FileOpenException Thrown if opening the file fails.
 * @throws JsonParseException Thrown if JSON parsing fails.
 */
nlohmann::json open_json(const std::string& file_path);

} // namespace archive_reader

#endif // ARCHIVE_READER_FILE_OPERATIONS_HPP_
