/*******************************************************************************************************************************
 * src/header/archive_reader/service/service.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_SERVICE_SERVICE_HPP_
#define ARCHIVE_READER_SERVICE_SERVICE_HPP_

#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>

#include "archive_reader/config_interface.hpp"
#include "archive_reader/service/info_service_impl.hpp"
#include "archive_reader/service/search_service_impl.hpp"

namespace archive_reader {
namespace service {

/**
 * The gRPC service.
 */
class Service
{
public:

  /**
   * Class constructor.
   * @param config The configuration object.
   */
  Service(const ConfigInterface& config);

  /**
   * Initialize the service.
   */
  void init();

  // TODO: document
  /**
   *
   */
  void wait();

  // TODO: document
  /**
   *
   */
  void shutdown();

  /**
   * Set the serving status.
   *
   * @param status The serving status; true to indicate it is serving.
   */
  void set_serving_status(const bool status) noexcept;

private:
  std::string _db_id;
  std::string _ip_address;
  int _port;
  std::unique_ptr<InfoServiceImpl> _info_svc;
  std::unique_ptr<SearchServiceImpl> _search_svc;
  std::unique_ptr<grpc::Server> _server;
};

} // namespace service;
} // namespace archive_reader;

#endif // ARCHIVE_READER_SERVICE_SERVICE_HPP_
