/*******************************************************************************************************************************
 * src/header/archive_reader/service/search_service_impl.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_SERVICE_SEARCH_SERVICE_IMPL_HPP_
#define ARCHIVE_READER_SERVICE_SEARCH_SERVICE_IMPL_HPP_

#include "archive_reader/search_service.grpc.pb.h"

using archive::service::SearchService;
using archive::service::SearchRequest;
using archive::service::SearchResponse;

namespace archive_reader {
namespace service {

class SearchServiceImpl final : public SearchService::Service
{
public:
  SearchServiceImpl(const std::string& db_id);

private:
  grpc::Status Search(grpc::ServerContext* ctx, const SearchRequest* req, SearchResponse* rep) override;

  std::string _db_id;
};

} // namespace service;
} // namespace archive_reader;

#endif // ARCHIVE_READER_SERVICE_SEARCH_SERVICE_IMPL_HPP_
