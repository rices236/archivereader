/*******************************************************************************************************************************
 * src/header/archive_reader/service/util.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_SERVICE_UTIL_HPP_
#define ARCHIVE_READER_SERVICE_UTIL_HPP_

#include <memory>

#include "archive_reader/whitedb/database.hpp"

namespace archive_reader {
namespace service {

/**
 * Get a pointer to an existing attached database.
 *
 * @param db_id The WhiteDB database key.
 * @return Pointer to an attached database; or a null pointer if the database failed to attach.
 */
std::unique_ptr<archive_reader::whitedb::Database> attached_db(const std::string& db_id) noexcept;

} // namespace service;
} // namespace archive_reader;

#endif // ARCHIVE_READER_SERVICE_UTIL_HPP_
