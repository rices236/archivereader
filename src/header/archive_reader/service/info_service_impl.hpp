/*******************************************************************************************************************************
 * src/header/archive_reader/service/info_service_impl.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_SERVICE_INFO_SERVICE_IMPL_HPP_
#define ARCHIVE_READER_SERVICE_INFO_SERVICE_IMPL_HPP_

#include "archive_reader/info_service.grpc.pb.h"

#include <memory>

#include "archive_reader/whitedb/database.hpp"

using archive::service::InfoService;
using archive::service::DbInfoRequest;
using archive::service::DbInfoResponse;

namespace archive_reader {
namespace service {

class InfoServiceImpl final : public InfoService::Service
{
public:
  InfoServiceImpl(const std::string& db_id);

private:
  grpc::Status DbInfo(grpc::ServerContext* ctx, const DbInfoRequest* req, DbInfoResponse* rep) override;

  std::string _db_id;
};

} // namespace service;
} // namespace archive_reader;

#endif // ARCHIVE_READER_SERVICE_INFO_SERVICE_IMPL_HPP_
