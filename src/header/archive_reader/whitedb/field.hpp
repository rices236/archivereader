/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/field.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_FIELD_HPP_
#define ARCHIVE_READER_WHITEDB_FIELD_HPP_

#include <whitedb/dbapi.h>

#include "archive_reader/whitedb/exception.hpp"
#include "archive_reader/whitedb/record_handle.hpp"
#include "archive_reader/whitedb/table_id.hpp"

namespace archive_reader {
namespace whitedb {

/**
 * This class provides a common interface for interacting with different Field types in WhiteDB databases.
 */
class BaseField {
public:
  BaseField() = default;
  virtual ~BaseField() = default;

  virtual void read(void* dbh, void* rec, const int field_no) = 0;
  virtual void write(void* dbh, void* rec, const int field_no) = 0;
  virtual wg_int encode_query_param(void* dbh) = 0;
};


/**
 * This class template provides common functionality for handling fields of different types in WhiteDB databases.
 *
 * Currently the following types are supported:
 *   - int
 *   - record_id
 *   - TableID
 *   - bool
 *   - std::string
 *   - RecordHandle
 * Other types can be added as required.
 *
 * @param T The type of value stored in the field.
 */
template<typename T>
class Field : public BaseField
{
public:

  /**
   * Default constructor.
   */
  Field();

  /**
   * Class constructor.
   *
   * @value The field value.
   */
  Field(const T& value);

  /**
   * Class destructor.
   */
  ~Field() = default;

  /**
   * Read a value from the WhiteDB database. The value will be written to the _value member.
   *
   * @param dbh The database handle.
   * @param rec The record handle.
   * @param field_no The zero-based index of the record field to be read from.
   */
  void read(void* dbh, void* rec, const int field_no) override;

  /**
   * Write a value to a field in a WhiteDB record. The value to be written will come from the _value member.
   *
   * @param dbh The database handle.
   * @param rec The record handle.
   * @param field_no The zero-based index of the record field to be written to.
   * @throw WhitedbWriteException Thrown on data encoding and field setting errors.
   */
  void write(void* dbh, void* rec, const int field_no) override;

  /**
   * Encode a query parameter in the WhiteDB database. The parameter value will come from the _value member.
   *
   * @param dbh The database handle.
   */
  wg_int encode_query_param(void* dbh) override;

  /**
   * Get the value;
   */
  T value() const noexcept;

  /**
   * Set the value.
   */
   void set_value(const T& value);

  /**
   * Read a value from the WhiteDB database record.
   *
   * This is a helper function to allow reading a value without first creating a Field instance.
   *
   * @param dbh The database handle.
   * @param rec The record handle.
   * @param field_no The zero-based index of the record field to be read from.
   * @return The value read from the field.
   */
   static T read_value(void* dbh, void* rec, const int field_no);

private:

  /**
   * Encode an integer.
   *
   * @param dbh The database handle.
   * @param value The integer value to encode.
   * @return Encoded data.
   * @throw WhitedbWriteException Thrown on data encoding errors.
   */
  static wg_int encode_value(void* dbh, const int value);

  /**
   * Encode an unsigned integer.
   *
   * @param dbh The database handle.
   * @param value The unsigned integer value to encode.
   * @return Encoded data.
   * @throw WhitedbWriteException Thrown on data encoding errors.
   */
  static wg_int encode_value(void* dbh, const unsigned int value);

  /**
   * Encode a TableID.
   *
   * @param dbh The database handle.
   * @param value The TableID value to encode.
   * @return Encoded data.
   * @throw WhitedbWriteException Thrown on data encoding errors.
   */
  static wg_int encode_value(void* dbh, const TableID value);

  /**
   * Encode a bool.
   *
   * For this implementation, Boolean values are encoded as the char values 'T' and 'F'. This is simply a convenience that
   * helps when reading raw values using the wgdb utility.
   *
   * @param dbh The database handle.
   * @param value The bool value to encode.
   * @return Encoded data.
   * @throw WhitedbWriteException Thrown on data encoding errors.
   */
  static wg_int encode_value(void* dbh, const bool value);

  /**
   * Encode a string.
   *
   * @param dbh The database handle.
   * @param value The string value to encode.
   * @return Encoded data.
   * @throw WhitedbWriteException Thrown on data encoding errors.
   */
  static wg_int encode_value(void* dbh, const std::string& value);

  /**
   * Encode a RecordHandle.
   *
   * @param dbh The database handle.
   * @param value The RecordHandle value to encode.
   * @return Encoded data.
   * @throw WhitedbWriteException Thrown on data encoding errors.
   */
  static wg_int encode_value(void* dbh, const RecordHandle& value);

  /**
   * Decode an integer.
   *
   * @param dbh The database handle.
   * @param data The data to decode.
   * @param value_out The integer variable to write to.
   */
  static void decode_data(void* dbh, wg_int data, int& value_out);

  /**
   * Decode an unsigned integer.
   *
   * @param dbh The database handle.
   * @param data The data to decode.
   * @param value_out The unsigned integer variable to write to.
   */
  static void decode_data(void* dbh, wg_int data, unsigned int& value_out);

  /**
   * Decode a TableID.
   *
   * @param dbh The database handle.
   * @param data The data to decode.
   * @param value_out The TableID variable to write to.
   */
  static void decode_data(void* dbh, wg_int data, TableID& value_out);

  /**
   * Decode a bool.
   *
   * Since Boolean values are encoded as the char values 'T' and 'F', when decoding the output will only be true if the
   * database value is 'T'.
   *
   * @param dbh The database handle.
   * @param data The data to decode.
   * @param value_out The bool variable to write to.
   */
  static void decode_data(void* dbh, wg_int data, bool& value_out);

  /**
   * Decode a string.
   *
   * @param dbh The database handle.
   * @param data The data to decode.
   * @param value_out The string variable to write to.
   */
  static void decode_data(void* dbh, wg_int data, std::string& value_out);

  /**
   * Decode a RecordHandle.
   *
   * @param dbh The database handle.
   * @param data The data to decode.
   * @param value_out The RecordHandle variable to write to.
   */
  static void decode_data(void* dbh, wg_int data, RecordHandle& value_out);

  /**
   * Encode an integer query parameter.
   *
   * @param dbh The database handle.
   * @param value The value to encode.
   * @return Encoded data.
   */
  static wg_int encode_qparam(void* dbh, const int value);

  /**
   * Encode an unsigned integer query parameter.
   *
   * @param dbh The database handle.
   * @param value The value to encode.
   * @return Encoded data.
   */
  static wg_int encode_qparam(void* dbh, const unsigned int value);

  /**
   * Encode a TableID query parameter.
   *
   * @param dbh The database handle.
   * @param value The value to encode.
   * @return Encoded data.
   */
  static wg_int encode_qparam(void* dbh, const TableID value);

  /**
   * Encode a bool query parameter.
   *
   * The Boolean values are encoded as the char values 'T' and 'F'.
   *
   * @param dbh The database handle.
   * @param value The value to encode.
   * @return Encoded data.
   */
  static wg_int encode_qparam(void* dbh, const bool value);

  /**
   * Encode a string query parameter.
   *
   * @param dbh The database handle.
   * @param value The value to encode.
   * @return Encoded data.
   */
  static wg_int encode_qparam(void* dbh, const std::string& value);

  /**
   * Encode a RecordHandle query parameter.
   *
   * @param dbh The database handle.
   * @param value The value to encode.
   * @return Encoded data.
   */
  static wg_int encode_qparam(void* dbh, const RecordHandle& value);

  T _value;

};

template<typename T>
Field<T>::Field()
  : _value {}
{
}

template<typename T>
Field<T>::Field(const T& value)
  : _value {value}
{
}

template<typename T>
void Field<T>::read(void* dbh, void* rec, const int field_no)
{
  wg_int data = wg_get_field(dbh, rec, static_cast<wg_int>(field_no));
  decode_data(dbh, data, _value);
}

template<typename T>
void Field<T>::write(void* dbh, void* rec, int field_no)
{
  wg_int enc = encode_value(dbh, _value);
  wg_int set_result = wg_set_field(dbh, rec, static_cast<wg_int>(field_no), enc);
  if (set_result) {
    throw WhitedbWriteException("Set field error: result is " + std::to_string(set_result) + ".");
  }
}

template<typename T>
wg_int Field<T>::encode_query_param(void* dbh)
{
  return encode_qparam(dbh, _value);
}

template<typename T>
T Field<T>::value() const noexcept
{
  return _value;
}

template<typename T>
void Field<T>::set_value(const T& value)
{
  _value = value;
}

template<typename T>
T Field<T>::read_value(void* dbh, void* rec, const int field_no)
{
  wg_int data = wg_get_field(dbh, rec, static_cast<wg_int>(field_no));
  T value {};
  decode_data(dbh, data, value);

  return value;
}

template<typename T>
wg_int Field<T>::encode_value(void* dbh, const int value)
{
  wg_int enc = wg_encode_int(dbh, static_cast<wg_int>(value));
  if (enc == WG_ILLEGAL) {
    throw WhitedbWriteException("Integer encoding caused WG_ILLEGAL error.");
  }

  return enc;
}

template<typename T>
wg_int Field<T>::encode_value(void* dbh, const unsigned int value)
{
  wg_int enc = wg_encode_int(dbh, static_cast<wg_int>(value));
  if (enc == WG_ILLEGAL) {
    throw WhitedbWriteException("Unsigned integer encoding caused WG_ILLEGAL error.");
  }

  return enc;
}

template<typename T>
wg_int Field<T>::encode_value(void* dbh, const TableID value)
{
  wg_int enc = wg_encode_int(dbh, static_cast<wg_int>(value));
  if (enc == WG_ILLEGAL) {
    throw WhitedbWriteException("TableID encoding caused WG_ILLEGAL error.");
  }

  return enc;
}

template<typename T>
wg_int Field<T>::encode_value(void* dbh, const bool value)
{
  wg_int enc = wg_encode_char(dbh, (value ? 'T' : 'F'));
  if (enc == WG_ILLEGAL) {
    throw WhitedbWriteException("Boolean encoding caused WG_ILLEGAL error.");
  }

  return enc;
}

template<typename T>
wg_int Field<T>::encode_value(void* dbh, const std::string& value)
{
  wg_int enc = wg_encode_str(dbh, const_cast<char*>(value.c_str()), NULL);
  if (enc == WG_ILLEGAL) {
    throw WhitedbWriteException("String encoding caused WG_ILLEGAL error.");
  }

  return enc;
}

template<typename T>
wg_int Field<T>::encode_value(void* dbh, const RecordHandle& value)
{
  wg_int enc = static_cast<wg_int>(0);
  if (value) {
    enc = wg_encode_record(dbh, value.handle());
    if (enc == WG_ILLEGAL) {
      throw WhitedbWriteException("RecordHandle encoding caused WG_ILLEGAL error.");
    }
  }

  return enc;
}

template<typename T>
void Field<T>::decode_data(void* dbh, wg_int data, int& value_out)
{
  value_out = static_cast<int>(wg_decode_int(dbh, data));
}

template<typename T>
void Field<T>::decode_data(void* dbh, wg_int data, unsigned int& value_out)
{
  value_out = static_cast<unsigned int>(wg_decode_int(dbh, data));
}

template<typename T>
void Field<T>::decode_data(void* dbh, wg_int data, TableID& value_out)
{
  value_out = static_cast<TableID>(wg_decode_int(dbh, data));
}

template<typename T>
void Field<T>::decode_data(void* dbh, wg_int data, bool& value_out)
{
  value_out = (wg_decode_char(dbh, data) == 'T');
}

template<typename T>
void Field<T>::decode_data(void* dbh, wg_int data, std::string& value_out)
{
  char* value = wg_decode_str(dbh, data);
  if (!value) {
    throw WhitedbReadException("String decoding returned a null string pointer.");
  }
  value_out = std::move(std::string(wg_decode_str(dbh, data)));
}

template<typename T>
void Field<T>::decode_data(void* dbh, wg_int data, RecordHandle& value_out)
{
  value_out = RecordHandle(wg_decode_record(dbh, data));
}

template<typename T>
wg_int Field<T>::encode_qparam(void* dbh, const int value)
{
  return wg_encode_query_param_int(dbh, static_cast<wg_int>(value));
}

template<typename T>
wg_int Field<T>::encode_qparam(void* dbh, const unsigned int value)
{
  return wg_encode_query_param_int(dbh, static_cast<wg_int>(value));
}

template<typename T>
wg_int Field<T>::encode_qparam(void* dbh, const TableID value)
{
  return wg_encode_query_param_int(dbh, static_cast<wg_int>(value));
}

template<typename T>
wg_int Field<T>::encode_qparam(void* dbh, const bool value)
{
  return wg_encode_query_param_char(dbh, (value ? 'T' : 'F'));
}

template<typename T>
wg_int Field<T>::encode_qparam(void* dbh, const std::string& value)
{
  return wg_encode_query_param_str(dbh, const_cast<char*>(value.c_str()), NULL);
}

template<typename T>
wg_int Field<T>::encode_qparam(void* dbh, const RecordHandle& value)
{
  return wg_encode_query_param_record(dbh, value.handle());
}

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_FIELD_HPP_
