/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/condition.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_CONDITION_HPP_
#define ARCHIVE_READER_WHITEDB_CONDITION_HPP_

#include <string>

#include <whitedb/dbapi.h>

namespace archive_reader {
namespace whitedb {

/**
 * Conditions for value comparisons in WhiteDB queries.
 */
enum class Condition {
  Equal = WG_COND_EQUAL,
  NotEqual = WG_COND_NOT_EQUAL,
  LessThan = WG_COND_LESSTHAN,
  GreaterThan = WG_COND_GREATER,
  EqualOrLess = WG_COND_LTEQUAL,
  EqualOrGreater = WG_COND_GTEQUAL
};

/**
 * Get the wg_int value equivalent.
 *
 * @param cond The Condition value.
 * @return A the wg_int value that can be used with the WhiteDB API.
 */
wg_int to_wgdb_condition(const Condition cond);

/**
 * Get a readable string for the enum.
 *
 * @param cond The Condition value.
 * @return A readable string identifying the enum.
 */
std::string condition_to_name(const Condition cond) noexcept;

/**
 * Stream Condition information to text suitable for logging and debugging.
 *
 * @param os The output stream to write to.
 * @param cond The Condition value for writing to the stream.
 * @return A reference to the output stream passed in the parameter.
 */
std::ostream& operator<<(std::ostream& os, const Condition& cond);

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_CONDITION_HPP_
