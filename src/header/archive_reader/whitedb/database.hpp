/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/database.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_DATABASE_HPP_
#define ARCHIVE_READER_WHITEDB_DATABASE_HPP_

#include <memory>

#include "archive_reader/whitedb/query.hpp"
#include "archive_reader/whitedb/record_handle.hpp"
#include "archive_reader/whitedb/record/base_record.hpp"

namespace archive_reader {
namespace whitedb {

/**
 * Encapsulates the handling of a WhiteDB database instance.
 */
class Database
{
public:

  /**
   * Class copy constructor.
   *
   * @param other The object containing the data to copy.
   */
  Database(const Database& other);

  /**
   * Class copy assignment operator.
   *
   * @param other The object containing the data to copy.
   */
  Database& operator=(const Database& other);

  /**
   * Get the database identifier used by the WhiteDB API.
   */
  int id() const noexcept;

  /**
   * Get the database handle.
   */
  void* handle() const noexcept;

  /**
   * Returns true if the database uses local memory; false if it uses shared memory.
   */
  bool is_local() const noexcept;

  /**
   * Get whether the database is attached.
   */
  bool is_attached() const noexcept;

  /**
   * Get whether the database has been deleted.
   */
  bool is_deleted() const noexcept;

  /**
   * Get the total memory segment size for the database, in bytes.
   */
  long size() const noexcept;

  /**
   * Get the amount of free space in the database memory segment, in bytes.
   */
  long free_size() const noexcept;

  /**
   * Detach the database. Does nothing if the database uses local memory, as local memory databases can't be detached.
   *
   * @throw DatabaseDetachException Thrown if the database fails to detach.
   */
  void detach();

  /**
   * Delete the database.
   *
   * @throw DatabaseDeleteException Thrown if the database is not deleted. Shared memory databases can't be deleted unless all
   * attached processes have been detached.
   */
  void delete_();

  /**
   * Read from the database to a record.
   */
  void read(const RecordHandle& rh, record::BaseRecord& rec);

  /**
   * Write to the database from a record.
   *
   * @param rec The record to write.
   * @return A record handle pointing to the newly created record.
   * @throw WhitedbWriteException Thrown if the record could not be created.
   */
  RecordHandle write(record::BaseRecord& rec);

  /**
   * Get a query object.
   *
   * This function returns a smart pointer to a Query object created by the database. Query objects are managed by the
   * database and should not be copied or created in any other way.
   *
   * @param qpl The query parameter list.
   * @return A smart pointer to the query object.
   */
  std::unique_ptr<Query> query(const QueryParameterList& qpl);

  /**
   * Factory function for creating a local memory database.
   *
   * The WhiteDB API does not have a function specifically for creating a local database. It provides a function that will
   * either create a local database, or attach to an existing one.
   *
   * @param size_in_bytes The database private memory size in bytes.
   * @return The local database instance.
   * @throw DatabaseCreateException Thrown if the local database could not be created.
   */
  static Database create_local(const int size_in_bytes);

  /**
   * Factory function for creating a shared memory database.
   *
   * The WhiteDB API does not have a function specifically for creating a local database. It provides a function that will
   * either create a local database, or attach to an existing one.
   *
   * @param id The database identifier value.
   * @param size_in_bytes The database shared memory size in bytes.
   * @return The shared database instance.
   * @throw DatabaseCreateException Thrown if the shared database could not be created.
   */
  static Database create_shared(const int id, const int size_in_bytes);

  /**
   * Factory function for attaching to a shared memory database.
   *
   * @param id The database identifier of an existing database.
   * @return The attached shared database instance.
   * @throw DatabaseAttachException Thrown if the database could not be attached.
   */
  static Database attach(const int id);

private:

  /**
   * Constructor used by the static factory functions.
   *
   * @param id The database identifier used by the WhiteDB API.
   * @param handle The handle used by the WhiteDB API.
   * @param is_local Whether the database is a local memory database.
   * @param is_attached Whether the database is attached.
   * @param is_deleted Whether the database is deleted.
   */
  Database(const int id, void* handle, const bool is_local, const bool is_attached, const bool is_deleted);

  int _id;
  void* _handle;
  bool _is_local;
  bool _is_attached;
  bool _is_deleted;

  Database() = delete;
  Database(Database&&) = delete;
  Database& operator=(Database&&) = delete;
};

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_DATABASE_HPP_
