/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/exception.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_EXCEPTION_HPP_
#define ARCHIVE_READER_WHITEDB_EXCEPTION_HPP_

#include "archive_reader/exception.hpp"

namespace archive_reader {
namespace whitedb {

class WhitedbException : public ArchiveReaderException
{
public:
  WhitedbException(const std::string& cause) : ArchiveReaderException(cause) {}
  WhitedbException(const std::string& cause, const std::exception& e) : ArchiveReaderException(cause, e) {}
};

class WhitedbCreateException : public WhitedbException
{
public:
  WhitedbCreateException(const std::string& cause) : WhitedbException(cause) {}
  WhitedbCreateException(const std::string& cause, const std::exception& e) : WhitedbException(cause, e) {}
};

class WhitedbAttachException : public WhitedbException
{
public:
  WhitedbAttachException(const std::string& cause) : WhitedbException(cause) {}
  WhitedbAttachException(const std::string& cause, const std::exception& e) : WhitedbException(cause, e) {}
};

class WhitedbDetachException : public WhitedbException
{
public:
  WhitedbDetachException(const std::string& cause) : WhitedbException(cause) {}
  WhitedbDetachException(const std::string& cause, const std::exception& e) : WhitedbException(cause, e) {}
};

class WhitedbDeleteException : public WhitedbException
{
public:
  WhitedbDeleteException(const std::string& cause) : WhitedbException(cause) {}
  WhitedbDeleteException(const std::string& cause, const std::exception& e) : WhitedbException(cause, e) {}
};

class WhitedbWriteException : public WhitedbException
{
public:
  WhitedbWriteException(const std::string& cause) : WhitedbException(cause) {}
  WhitedbWriteException(const std::string& cause, const std::exception& e) : WhitedbException(cause, e) {}
};

class WhitedbReadException : public WhitedbException
{
public:
  WhitedbReadException(const std::string& cause) : WhitedbException(cause) {}
  WhitedbReadException(const std::string& cause, const std::exception& e) : WhitedbException(cause, e) {}
};

class WhitedbQueryException : public WhitedbException
{
public:
  WhitedbQueryException(const std::string& cause) : WhitedbException(cause) {}
  WhitedbQueryException(const std::string& cause, const std::exception& e) : WhitedbException(cause, e) {}
};

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_EXCEPTION_HPP_
