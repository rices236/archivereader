/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/query.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_QUERY_HPP_
#define ARCHIVE_READER_WHITEDB_QUERY_HPP_

#include <vector>

#include <whitedb/dbapi.h>

#include "archive_reader/whitedb/query_parameter_list.hpp"
#include "archive_reader/whitedb/record_handle.hpp"

namespace archive_reader {
namespace whitedb {

/**
 * Search for records in the database.
 */
class Query
{
public:

  /**
   * Class destructor.
   */
  ~Query();

  /**
   * Get the next record in the query.
   *
   * @return A valid record handle if there is one, otherwise a null record handle.
   */
  RecordHandle fetch();

  /**
   * Free the resources in the query.
   *
   * When a query is run, WhiteDB allocates resources for it that must be de-allocated when no longer needed. This function is
   * called automatically on class destruction and after the last record has been read in a fetch() operation, but can be
   * called earlier if desired.
   */
  void free() noexcept;

private:

  /**
   * Class constructor.
   *
   * This constructor is private since instances should only be created by friends of this class.
   *
   * @param dbh The database handle.
   * @param qpl The query parameter list.
   */
  Query(const void* dbh, const QueryParameterList& qpl);

  /**
   * Allocate the resource for the query and prepare it to fetch records.
   *
   * This function is private since it should only be called by friends of this class.
   *
   * @throw WhitedbQueryException Thrown on failure if unable to create a WhiteDB query object.
   */
  void make();

  const void* _dbh;
  param_list _parameter_list;
  wg_query* _query;
  std::vector<wg_int> _resource_list;

  Query() = delete;
  Query(const Query& other) = delete;
  Query(Query&& other) = delete;
  Query& operator=(const Query& other) = delete;
  Query& operator=(Query&& other) = delete;

  friend class Database;

};

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_QUERY_HPP_
