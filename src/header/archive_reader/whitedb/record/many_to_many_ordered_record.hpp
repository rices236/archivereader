/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/record/many_to_many_ordered_record.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_RECORD_MANY_TO_MANY_ORDERED_RECORD_HPP_
#define ARCHIVE_READER_WHITEDB_RECORD_MANY_TO_MANY_ORDERED_RECORD_HPP_

#include <string>

#include "archive_reader/whitedb/record/base_record.hpp"
#include "archive_reader/whitedb/database.hpp"
#include "archive_reader/whitedb/exception.hpp"
#include "archive_reader/whitedb/query_parameter_list.hpp"

namespace archive_reader {
namespace whitedb {
namespace record {

/**
 * This class supports database records that link other sets of records in a many-to-many ordered relationship. Graphically, it
 * looks like this:
 *   ----------------------------------------------------------------------
 *   | table_id | record_id | record_a_pointer | order | record_b_pointer |
 *   ----------------------------------------------------------------------
 * The order field is an integer representing the order of record B records within record A. This structure can be used to
 * search both ways. In other words, if field 1 contained pointers to Release records, and field 3 contained pointers to Artist
 * records, then one could use the table to search for the releases for a particular artist as well as the artists for a
 * particular release. However, the order field is only useful in the latter case.
 *
 * @param tid The table identifier.
 */
template<TableID tid>
class ManyToManyOrderedRecord : public BaseRecord
{
public:

  /**
   * Default constructor.
   */
  ManyToManyOrderedRecord();

  /**
   * Class constructor.
   *
   * @param record_a The handle to record A.
   * @param order The order.
   * @param record_b The handle to record B.
   */
  ManyToManyOrderedRecord(const RecordHandle& record_a, const int order, const RecordHandle& record_b);

  /**
   * Class destructor.
   */
  virtual ~ManyToManyOrderedRecord() = default;

  /**
   * Get the handle to record A.
   */
  RecordHandle record_a() const noexcept;

  /**
   * Get the order.
   */
  int order() const noexcept;

  /**
   * Get the handle to record B.
   */
  RecordHandle record_b() const noexcept;

  /**
   * Set the handle to record A.
   */
  void set_record_a(const RecordHandle& record_a);

  /**
   * Set the order.
   */
  void set_order(const int order);

  /**
   * Set the handle to record B.
   */
  void set_record_b(const RecordHandle& record_b);

protected:

  /**
   * Get the database fields in order.
   *
   * This is called prior to reading from or writing to the database.
   *
   * @param fld_list Vector to which field pointers should be added.
   */
  void get_fields(std::vector<BaseField*>& fld_list);

  Field<RecordHandle> _f_record_a;
  Field<int> _f_order;
  Field<RecordHandle> _f_record_b;

};

template<TableID tid>
ManyToManyOrderedRecord<tid>::ManyToManyOrderedRecord()
  : BaseRecord {tid},
    _f_record_a {},
    _f_order {},
    _f_record_b {}
{
}

template<TableID tid>
ManyToManyOrderedRecord<tid>::ManyToManyOrderedRecord(const RecordHandle& record_a, const int order,
    const RecordHandle& record_b)
  : BaseRecord {tid},
    _f_record_a {record_a},
    _f_order {order},
    _f_record_b {record_b}
{
}

template<TableID tid>
RecordHandle ManyToManyOrderedRecord<tid>::record_a() const noexcept
{
  return _f_record_a.value();
}

template<TableID tid>
int ManyToManyOrderedRecord<tid>::order() const noexcept
{
  return _f_order.value();
}

template<TableID tid>
RecordHandle ManyToManyOrderedRecord<tid>::record_b() const noexcept
{
  return _f_record_b.value();
}

template<TableID tid>
void ManyToManyOrderedRecord<tid>::set_record_a(const RecordHandle& record_a)
{
  _f_record_a.set_value(record_a);
}

template<TableID tid>
void ManyToManyOrderedRecord<tid>::set_order(const int order)
{
  _f_order.set_value(order);
}

template<TableID tid>
void ManyToManyOrderedRecord<tid>::set_record_b(const RecordHandle& record_b)
{
  _f_record_b.set_value(record_b);
}

template<TableID tid>
void ManyToManyOrderedRecord<tid>::get_fields(std::vector<BaseField*>& fld_list)
{
  fld_list.push_back(static_cast<BaseField*>(&_f_record_a));
  fld_list.push_back(static_cast<BaseField*>(&_f_order));
  fld_list.push_back(static_cast<BaseField*>(&_f_record_b));
}

} // namespace record
} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_RECORD_MANY_TO_MANY_ORDERED_RECORD_HPP_
