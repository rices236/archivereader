/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/record/base_record.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_RECORD_BASE_RECORD_HPP_
#define ARCHIVE_READER_WHITEDB_RECORD_BASE_RECORD_HPP_

#include <vector>

#include "archive_reader/whitedb/field.hpp"
#include "archive_reader/whitedb/table_id.hpp"

namespace archive_reader {
namespace whitedb {
namespace record {

typedef unsigned int rec_id;

/**
 * This class forms the basis for all tables in this project, providing TableID and unique record identifier features.
 *
 * The WhiteDB API has no concept of tables. It creates and maintains record-like structures, which are simply collections of
 * related information. (They can be thought of as something like a SQL database record in that they contain indexed fields of
 * typed data, but the similarities end there.) For this project, in order to maintain categories of data (a concept roughly
 * similar to a SQL table) each record will have a table identifier as the first field. Records having identical TableIDs will
 * have identical field layouts.
 *
 * Tables using this base class will have a structure as follows:
 *   -----------------------------------------------
 *   | table_id | record_id | all other fields ... |
 *   -----------------------------------------------
 */
class BaseRecord
{
public:

  /**
   * Class destructor.
   */
  virtual ~BaseRecord() = default;

  /**
   * Get the table identifier.
   */
  TableID table_id() const noexcept
  {
    return _f_table_id.value();
  }

  /**
   * Get the unique record identifier.
   */
  rec_id urid() const noexcept
  {
    return _f_urid.value();
  }

protected:

  /**
   * Class constructor.
   *
   * @param tid The table identifier.
   */
  BaseRecord(const TableID tid);

  /**
   * Get the database fields in order.
   *
   * This is called prior to reading from or writing to the database.
   *
   * @param fld_list Vector to which field pointers should be added.
   */
  virtual void get_fields(std::vector<BaseField*>& fld_list) = 0;

  Field<TableID> _f_table_id;
  Field<rec_id> _f_urid;

private:

  /**
   * Add field pointers to the vector.
   *
   * This is called by the database on writes and reads so that the record object can add pointers to its fields in the
   * sequence in which they should appear in the database record. The implementation of this function adds the first two
   * fields--the table ID and record ID--which are common to all records. The get_fields() virtual member function is then
   * called, which is overridden in the subclasses, the implementations adding remaining data fields.
   *
   * @param fld_list The vector to add field pointers to.
   * @param br The record containing the fields.
   */
  friend void get_fields(std::vector<BaseField*>& fld_list, BaseRecord& br);

  /**
   * Get the next unique record identifier.
   */
  static rec_id next_urid() noexcept;

  inline static rec_id _next_urid = 1;

  BaseRecord() = delete;

};

} // namespace record
} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_RECORD_BASE_RECORD_HPP_
