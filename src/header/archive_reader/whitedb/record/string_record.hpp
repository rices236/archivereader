/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/record/string_record.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_RECORD_STRING_RECORD_HPP_
#define ARCHIVE_READER_WHITEDB_RECORD_STRING_RECORD_HPP_

#include <string>

#include "archive_reader/whitedb/record/base_record.hpp"
#include "archive_reader/whitedb/database.hpp"
#include "archive_reader/whitedb/exception.hpp"
#include "archive_reader/whitedb/query_parameter_list.hpp"

namespace archive_reader {
namespace whitedb {
namespace record {

/**
 * This class supports database records that contain three fields: a table ID, a record ID and a string. Composer, Artist,
 * Genre and Label are examples. Graphically, it looks like this:
 *   ---------------------------------------
 *   | table_id | record_id | string_value |
 *   ---------------------------------------
 */
template<TableID tid>
class StringRecord : public BaseRecord
{
public:

  /**
   * Class constructor.
   *
   * @param tid The table identifier.
   */
  StringRecord();

  /**
   * Class constructor.
   *
   * @param tid The table identifier.
   * @param value The value to set.
   */
  StringRecord(const std::string& value);

  /**
   * Class destructor.
   */
  virtual ~StringRecord() = default;

  /**
   * Get the value.
   */
  std::string value() const noexcept;

  /**
   * Set the value.
   */
  void set_value(const std::string& value);

  /**
   * Search for a unique record using the table identifier and value.
   *
   * This function assumes that the table contains only unique values. WhiteDB has no native means of enforcing such
   * constraints, so checking must be part of the input operations. This function will return the first record handle for a
   * match if one is found, but it will not be able to detect if there are other records with the same value.
   *
   * @param db The database.
   * @param value The value to search for.
   * @return A handle which will point to the located record, if found; otherwise, a handle pointing to null.
   */
  static RecordHandle fetch_unique(Database& db, const std::string& value);

protected:

  /**
   * Get the database fields in order.
   *
   * This is called prior to reading from or writing to the database.
   *
   * @param fld_list Vector to which field pointers should be added.
   */
  void get_fields(std::vector<BaseField*>& fld_list) override;

  Field<std::string> _value;

};

template<TableID tid>
StringRecord<tid>::StringRecord()
  : BaseRecord {tid},
    _value {}
{
}

template<TableID tid>
StringRecord<tid>::StringRecord(const std::string& value)
  : BaseRecord {tid},
    _value {value}
{
}

template<TableID tid>
std::string StringRecord<tid>::value() const noexcept
{
  return _value.value();
}

template<TableID tid>
void StringRecord<tid>::set_value(const std::string& value)
{
  _value.set_value(value);
}

template<TableID tid>
RecordHandle StringRecord<tid>::fetch_unique(Database& db, const std::string& value)
{
  QueryParameterList qpl;
  qpl.add<TableID>(0, Condition::Equal, tid);
  qpl.add<std::string>(2, Condition::Equal, value);
  auto qry = db.query(qpl);
  RecordHandle rh = qry->fetch();

  return rh;
}

template<TableID tid>
void StringRecord<tid>::get_fields(std::vector<BaseField*>& fld_list)
{
  fld_list.push_back(static_cast<BaseField*>(&_value));
}

} // namespace record
} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_RECORD_STRING_RECORD_HPP_
