/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/record/one_to_many_ordered_string_record.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_RECORD_ONE_TO_MANY_ORDERED_STRING_RECORD_HPP_
#define ARCHIVE_READER_WHITEDB_RECORD_ONE_TO_MANY_ORDERED_STRING_RECORD_HPP_

#include <string>

#include "archive_reader/whitedb/record/base_record.hpp"
#include "archive_reader/whitedb/exception.hpp"

namespace archive_reader {
namespace whitedb {
namespace record {

/**
 * This class supports database records that link a record to a string a one-to-many ordered relationship. Graphically, it
 * looks like this:
 *   ---------------------------------------------------------------
 *   | table_id | record_id | record_pointer | order | string_data |
 *   ---------------------------------------------------------------
 * The order field is an integer representing the order of string data records in the linked record.
 *
 * @param tid The table identifier.
 */
template<TableID tid>
class OneToManyOrderedStringRecord : public BaseRecord
{
public:

  /**
   * Default constructor.
   */
  OneToManyOrderedStringRecord();

  /**
   * Class constructor.
   *
   * @param linked_record The handle to the linked record.
   * @param order The order.
   * @param data The string data.
   */
  OneToManyOrderedStringRecord(const RecordHandle& linked_record, const int order, const std::string& data);

  /**
   * Class destructor.
   */
  virtual ~OneToManyOrderedStringRecord() = default;

  /**
   * Get the handle to the linked record.
   */
  RecordHandle linked_record() const noexcept;

  /**
   * Get the order.
   */
  int order() const noexcept;

  /**
   * Get the data.
   */
  std::string data() const noexcept;

  /**
   * Set the handle to the linked record.
   */
  void set_linked_record(const RecordHandle& linked_record);

  /**
   * Set the order.
   */
  void set_order(const int order);

  /**
   * Set the data.
   */
  void set_data(const std::string& data);

protected:

  /**
   * Get the database fields in order.
   *
   * This is called prior to reading from or writing to the database.
   *
   * @param fld_list Vector to which field pointers should be added.
   */
  void get_fields(std::vector<BaseField*>& fld_list);

  Field<RecordHandle> _f_linked_record;
  Field<int> _f_order;
  Field<std::string> _f_data;

};

template<TableID tid>
OneToManyOrderedStringRecord<tid>::OneToManyOrderedStringRecord()
  : BaseRecord {tid},
    _f_linked_record {},
    _f_order {},
    _f_data {}
{
}

template<TableID tid>
OneToManyOrderedStringRecord<tid>::OneToManyOrderedStringRecord(const RecordHandle& linked_record, const int order,
    const std::string& data)
  : BaseRecord {tid},
    _f_linked_record {linked_record},
    _f_order {order},
    _f_data {data}
{
}

template<TableID tid>
RecordHandle OneToManyOrderedStringRecord<tid>::linked_record() const noexcept
{
  return _f_linked_record.value();
}

template<TableID tid>
int OneToManyOrderedStringRecord<tid>::order() const noexcept
{
  return _f_order.value();
}

template<TableID tid>
std::string OneToManyOrderedStringRecord<tid>::data() const noexcept
{
  return _f_data.value();
}

template<TableID tid>
void OneToManyOrderedStringRecord<tid>::set_linked_record(const RecordHandle& linked_record)
{
  _f_linked_record.set_value(linked_record);
}

template<TableID tid>
void OneToManyOrderedStringRecord<tid>::set_order(const int order)
{
  _f_order.set_value(order);
}

template<TableID tid>
void OneToManyOrderedStringRecord<tid>::set_data(const std::string& data)
{
  _f_data.set_value(data);
}

template<TableID tid>
void OneToManyOrderedStringRecord<tid>::get_fields(std::vector<BaseField*>& fld_list)
{
  fld_list.push_back(static_cast<BaseField*>(&_f_linked_record));
  fld_list.push_back(static_cast<BaseField*>(&_f_order));
  fld_list.push_back(static_cast<BaseField*>(&_f_data));
}

} // namespace record
} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_RECORD_ONE_TO_MANY_ORDERED_STRING_RECORD_HPP_
