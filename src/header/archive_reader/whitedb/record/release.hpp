/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/record/release.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_RECORD_RELEASE_HPP_
#define ARCHIVE_READER_WHITEDB_RECORD_RELEASE_HPP_

#include "archive_reader/whitedb/record/base_record.hpp"
#include "archive_reader/whitedb/record_handle.hpp"
#include "archive_reader/release.hpp"

namespace archive_reader {
namespace whitedb {
namespace record {

/**
 * Write and read release information to and from the WhiteDB database.
 */
class Release : public BaseRecord
{
public:

  /**
   * Default constructor.
   */
  Release();

  /**
   * Class constructor.
   *
   * @param ar_rel The release data read from JSON.
   */
  Release(const archive_reader::Release& ar_rel);

  /**
   * Get the title.
   */
  std::string title() const noexcept;

  /**
   * Get the catalog number.
   */
  std::string catalog_no() const noexcept;

  /**
   * Get whether this is a compilation.
   */
  bool is_compilation() const noexcept;

  /**
   * Get whether this is a reissue.
   */
  bool is_reissue() const noexcept;

  /**
   * Get the source.
   */
  std::string source() const noexcept;

  /**
   * Get the year.
   */
  int year() const noexcept;

  /**
   * Get the original year of issue.
   */
  int original_year() const noexcept;

  /**
   * Get the year of the performance.
   */
  int performance_year() const noexcept;

  /**
   * Get the full path of the release file.
   */
  std::string release_file_path() const noexcept;

  /**
   * Get the full path of the cover art file.
   */
  std::string cover_art_path() const noexcept;

  /**
   * Get the record handle of the composer.
   */
  RecordHandle composer() const noexcept;

  /**
   * Get the record handle of the genre.
   */
  RecordHandle genre() const noexcept;

  /**
   * Get the record handle of the label.
   */
  RecordHandle label() const noexcept;

  /**
   * Set the title.
   */
  void set_title(const std::string& title);

  /**
   * Set the catalog number.
   */
  void set_catalog_no(const std::string& catalog_no);

  /**
   * Set whether this is a compilation.
   */
  void set_is_compilation(const bool is_compilation);

  /**
   * Set whether this is a reissue.
   */
  void set_is_reissue(const bool is_reissue);

  /**
   * Set the source.
   */
  void set_source(const std::string& source);

  /**
   * Set the year.
   */
  void set_year(const int year);

  /**
   * Set the original year of issue.
   */
  void set_original_year(const int original_year);

  /**
   * Set the year of the performance.
   */
  void set_performance_year(const int performance_year);

  /**
   * Set the full path of the release file.
   */
  void set_release_file_path(const std::string& release_file_path);

  /**
   * Set the full path of the cover art file.
   */
  void set_cover_art_path(const std::string& cover_art_path);

  /**
   * Set the record handle of the composer.
   */
  void set_composer(const RecordHandle& composer);

  /**
   * Set the record handle of the genre.
   */
  void set_genre(const RecordHandle& genre);

  /**
   * Set the record handle of the label.
   */
  void set_label(const RecordHandle& label);


protected:

  /**
   * Get the database fields in order.
   *
   * This is called prior to reading from or writing to the database.
   *
   * @param fld_list Vector to which field pointers should be added.
   */
  void get_fields(std::vector<BaseField*>& fld_list);

  Field<std::string> _f_title;
  Field<std::string> _f_catalog_no;
  Field<bool> _f_is_compilation;
  Field<bool> _f_is_reissue;
  Field<std::string> _f_source;
  Field<int> _f_year;
  Field<int> _f_original_year;
  Field<int> _f_performance_year;
  Field<std::string> _f_release_file_path;
  Field<std::string> _f_cover_art_path;
  Field<RecordHandle> _f_composer_rec;
  Field<RecordHandle> _f_genre_rec;
  Field<RecordHandle> _f_label_rec;

};

} // namespace record
} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_RECORD_RELEASE_HPP_
