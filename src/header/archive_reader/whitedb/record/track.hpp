/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/record/track.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_RECORD_TRACK_HPP_
#define ARCHIVE_READER_WHITEDB_RECORD_TRACK_HPP_

#include "archive_reader/whitedb/record/base_record.hpp"
#include "archive_reader/whitedb/record_handle.hpp"
#include "archive_reader/track.hpp"

namespace archive_reader {
namespace whitedb {
namespace record {

/**
 * Write and read track information to and from the WhiteDB database.
 */
class Track : public BaseRecord
{
public:

  /**
   * Default constructor.
   */
  Track();

  /**
   * Class constructor.
   *
   * @param ar_rel The track data read from JSON.
   */
  Track(const archive_reader::Track& ar_track);

  /**
   * Get the record handle of the release.
   */
  RecordHandle release() const noexcept;

  /**
   * Get the track order.
   */
  int order() const noexcept;

  /**
   * Get the title.
   */
  std::string title() const noexcept;

  /**
   * Get the original year of issue.
   */
  int original_year() const noexcept;

  /**
   * Get the year of the performance.
   */
  int performance_year() const noexcept;

  /**
   * Get the record handle of the composer.
   */
  RecordHandle composer() const noexcept;

  /**
   * Get the record handle of the genre.
   */
  RecordHandle genre() const noexcept;

  /**
   * Get the record handle of the label.
   */
  RecordHandle label() const noexcept;

  /**
   * Get the track file path.
   */
  std::string file_path() const noexcept;

  /**
   * Set the record handle of the release.
   */
  void set_release(const RecordHandle& release);

  /**
   * Set the track order.
   */
  void set_order(const int order);

  /**
   * Set the title.
   */
  void set_title(const std::string& title);

  /**
   * Set the original year of issue.
   */
  void set_original_year(const int original_year);

  /**
   * Set the year of the performance.
   */
  void set_performance_year(const int performance_year);

  /**
   * Set the record handle of the composer.
   */
  void set_composer(const RecordHandle& composer);

  /**
   * Set the record handle of the genre.
   */
  void set_genre(const RecordHandle& genre);

  /**
   * Set the record handle of the label.
   */
  void set_label(const RecordHandle& label);

  /**
   * Set the track file path.
   */
  void set_file_path(const std::string& file_path);


protected:

  /**
   * Get the database fields in order.
   *
   * This is called prior to reading from or writing to the database.
   *
   * @param fld_list Vector to which field pointers should be added.
   */
  void get_fields(std::vector<BaseField*>& fld_list);

  Field<RecordHandle> _f_release_rec;
  Field<int> _f_order;
  Field<std::string> _f_title;
  Field<int> _f_original_year;
  Field<int> _f_performance_year;
  Field<RecordHandle> _f_composer_rec;
  Field<RecordHandle> _f_genre_rec;
  Field<RecordHandle> _f_label_rec;
  Field<std::string> _f_file_path;

};

} // namespace record
} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_RECORD_TRACK_HPP_
