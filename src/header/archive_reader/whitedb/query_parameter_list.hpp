/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/query_parameter_list.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_QUERY_PARAMETER_LIST_HPP_
#define ARCHIVE_READER_WHITEDB_QUERY_PARAMETER_LIST_HPP_

#include <memory>
#include <tuple>
#include <vector>

#include "archive_reader/whitedb/condition.hpp"
#include "archive_reader/whitedb/field.hpp"

namespace archive_reader {
namespace whitedb {

typedef std::vector<std::tuple<int, Condition, std::shared_ptr<BaseField>>> param_list;

/**
 * Build sets of query parameters for use in database queries.
 */
class QueryParameterList
{
public:

  /**
   * Default constructor.
   */
  QueryParameterList();

  /**
   * Class copy constructor.
   *
   * @param other The object containing the data to copy.
   */
  QueryParameterList(const QueryParameterList& other);

  /**
   * Class move constructor.
   *
   * @param other The object containing the data to move.
   */
  QueryParameterList(QueryParameterList&& other);

  /**
   * Class destructor.
   */
  ~QueryParameterList();

  /**
   * Class copy assignment operator.
   *
   * @param other The object containing the data to copy.
   * @return A reference to this object.
   */
  QueryParameterList& operator=(const QueryParameterList& other);

  /**
   * Class move assignment operator.
   *
   * @param other The object containing the data to move.
   * @return A reference to this object.
   */
  QueryParameterList& operator=(QueryParameterList&& other);

  /**
   * Add a query parameter.
   *
   * @param column The zero-based record column index.
   * @param cond The parameter condition to evaluate.
   * @param value The value to compare with database values.
   */
  template<typename T>
  void add(const int column, const Condition cond, const T& value);

private:
  param_list _params;

  friend class Query;

};

template<typename T>
void QueryParameterList::add(const int column, const Condition cond, const T& value)
{
  _params.push_back(std::make_tuple(column, cond, std::shared_ptr<BaseField>(new Field<T>(value))));
}

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_QUERY_PARAMETER_LIST_HPP_
