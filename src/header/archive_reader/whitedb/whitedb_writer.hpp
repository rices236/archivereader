/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/whitedb_writer.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_WHITEDB_WRITER_HPP_
#define ARCHIVE_READER_WHITEDB_WHITEDB_WRITER_HPP_

#include "archive_reader/db_writer.hpp"
#include "archive_reader/whitedb/database.hpp"
#include "archive_reader/whitedb/record_handle.hpp"
#include "archive_reader/whitedb/record/many_to_many_ordered_record.hpp"
#include "archive_reader/whitedb/record/one_to_many_ordered_string_record.hpp"
#include "archive_reader/whitedb/record/string_record.hpp"
#include "archive_reader/whitedb/table_id.hpp"

namespace archive_reader {
namespace whitedb {

/**
 * Write release information to a WhiteDB database.
 */
class WhitedbWriter : public DBWriter
{
public:

  /**
   * Class constructor.
   *
   * @param db The database to write information to.
   */
  WhitedbWriter(Database& db);

  /**
   * Class destructor.
   */
  ~WhitedbWriter();

  /**
   * Write the release data to the database.
   *
   * @param rel The release data to be written.
   */
  void write(const Release& rel) override;

protected:

  /**
   * Write the release record and return the handle of the newly created record.
   *
   * @param rel The release data to be written.
   * @return The handle to the newly written record.
   */
  RecordHandle write_release(const Release& rel);

  /**
   * Write track and related records with a link back to the release.
   *
   * @param track The track data to write.
   * @param h_release_rec The handle of the existing release record to link the track to.
   * @param order The order of the track record within the release.
   */
  void write_track(const Track& track, const RecordHandle& h_release_rec, const int order);

  /**
   * Write a record in a table that consists of a table ID and a single unique string.
   *
   * @param tid The table ID.
   * @param value The string value.
   * @return The handle to the newly written record if the value is unique in the table; if the value already exists, then the
   * record handle of the existing matching record.
   */
  template<TableID tid>
  RecordHandle write_unique_str_record(const std::string& value);

  /**
   * Create a record that is linked to another record by a many-to-many linking record. Graphically, it looks like this:
   *   ------------------    -------------------------------------------------    ----------------
   *   | release_record | <- | release_record_pointer | other_record_pointer | -> | other_record |
   *   ------------------    -------------------------------------------------    ----------------
   * This is used for items such as Performers, where one performer may be linked to many releases, and one release may be
   * linked to many performers.
   *
   * @param tid_a The table identifier of the record with the value.
   * @param tid_b The table identifier of the table that links the value record back to the primary record.
   * @param values A list of values to create (or link existing) value records from.
   * @param h_primary_rec The handle of the existing primary record to link the values to.
   */
  template<TableID tid_a, TableID tid_b>
  void write_linked_records(const str_list& values, const RecordHandle& h_primary_rec);

  /**
   * Write comments records with a link back to the release.
   *
   * @param tid The table identifier of the linking record.
   * @param comments The comments to write.
   * @param h_primary_rec The handle of the existing primary record to link the comments to.
   */
  template<TableID tid>
  void write_comments(const str_list& comments, const RecordHandle& h_primary_rec);

  Database _db;

  WhitedbWriter() = delete;

};

template<TableID tid>
RecordHandle WhitedbWriter::write_unique_str_record(const std::string& value)
{
  RecordHandle rh {};
  if (!value.empty()) {
    rh = record::StringRecord<tid>::fetch_unique(_db, value);
    if (!rh) {
      record::StringRecord<tid> sr {value};
      rh = _db.write(sr);
    }
  }

  return rh;
}

template<TableID tid_a, TableID tid_b>
void WhitedbWriter::write_linked_records(const str_list& values, const RecordHandle& h_release_rec)
{
  int order = 0;
  for (const std::string& val : values) {
    RecordHandle h_linked_rec = write_unique_str_record<tid_a>(val);
    record::ManyToManyOrderedRecord<tid_b> mtom_link(h_release_rec, ++order, h_linked_rec);
    _db.write(mtom_link);
  }
}

template<TableID tid>
void WhitedbWriter::write_comments(const str_list& comments, const RecordHandle& h_primary_rec)
{
  int order {0};
  for (const std::string& val : comments) {
    record::OneToManyOrderedStringRecord<tid> comment_link(h_primary_rec, ++order, val);
    _db.write(comment_link);
  }
}

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_WHITEDB_WRITER_HPP_
