/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/table_id.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_TABLE_ID_HPP_
#define ARCHIVE_READER_WHITEDB_TABLE_ID_HPP_

#include <string>

namespace archive_reader {
namespace whitedb {

/**
 * Table identifiers used in this database schema.
 */
enum class TableID {
  Release = 1,
  Track,
  Composer,
  Genre,
  Label,
  Artist,
  ReleaseArtist,
  TrackArtist,
  Performer,
  ReleasePerformer,
  TrackPerformer,
  Style,
  ReleaseStyle,
  TrackStyle,
  ReleaseComment,
  TrackComment
};

/**
 * Get a readable string for the enum.
 *
 * @param tid The table ID.
 * @return A readable string identifying the enum.
 */
std::string table_id_to_name(const TableID tid) noexcept;

/**
 * Stream TableID information to text suitable for logging and debugging.
 *
 * @param os The output stream to write to.
 * @param tid The TableID value for writing to the stream.
 * @return A reference to the output stream passed in the parameter.
 */
std::ostream& operator<<(std::ostream& os, const TableID& tid);

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_TABLE_ID_HPP_
