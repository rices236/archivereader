/*******************************************************************************************************************************
 * src/header/archive_reader/whitedb/record_handle.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_RECORD_HANDLE_HPP_
#define ARCHIVE_READER_WHITEDB_RECORD_HANDLE_HPP_

namespace archive_reader {
namespace whitedb {

/**
 * This class provides a type-safe way of passing WhiteDB record handles (which are pointer to void in the WhiteDB API) to
 * other components used by this project.
 */
class RecordHandle {
public:

  /**
   * Default constructor.
   */
  RecordHandle();

  /**
   * Class constructor.
   *
   * @param handle The handle to a WhiteDB record.
   */
  RecordHandle(void* handle);

  /**
   * Class copy constructor.
   *
   * @param other The object containing the data to copy.
   */
  RecordHandle(const RecordHandle& other);

  /**
   * Class move constructor.
   *
   * @param other The object containing the data to move.
   */
  RecordHandle(RecordHandle&& other);

  /**
   * Class destructor.
   */
  ~RecordHandle() = default;

  /**
   * Class copy assignment operator.
   *
   * @param other The object containing the data to copy.
   * @return A reference to this object.
   */
  RecordHandle& operator=(const RecordHandle& other);

  /**
   * Class move assignment operator.
   *
   * @param other The object containing the data to move.
   * @return A reference to this object.
   */
  RecordHandle& operator=(RecordHandle&& other);

  /**
   * Indicate whether the handle is null.
   *
   * @return True if the handle is not null.
   */
  operator bool() const noexcept;

  /**
   * Get the record handle.
   */
  void* handle() const noexcept;

  /**
   * Indicate whether objects are equal.
   *
   * @return True if the objects contain equivalent handles.
   */
  friend bool operator==(const RecordHandle& lhs, const RecordHandle& rhs) noexcept;

  /**
   * Indicate whether objects are different.
   *
   * @return True if the objects contain different handles.
   */
  friend bool operator!=(const RecordHandle& lhs, const RecordHandle& rhs) noexcept;

private:
  void* _handle;

};

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_RECORD_HANDLE_HPP_
