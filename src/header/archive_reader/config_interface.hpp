/*******************************************************************************************************************************
 * src/header/archive_reader/config_interface.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_CONFIG_INTERFACE_HPP_
#define ARCHIVE_READER_CONFIG_INTERFACE_HPP_

#include <string>

namespace archive_reader {

/**
 * Interface for configuration options.
 */
class ConfigInterface
{
public:

  /**
   * Class destructor.
   */
  virtual ~ConfigInterface() = default;

  /**
   * Get the library root value.
   */
  virtual std::string library_root() const noexcept = 0;

  /**
   * Get the release file value.
   */
  virtual std::string release_file() const noexcept = 0;

  /**
   * Get the WhiteDB ID value.
   */
  virtual int db_id() const noexcept = 0;

  /**
   * Get the WhiteDB database size in bytes.
   */
  virtual int db_size() const noexcept = 0;

  /**
   * Get the service IP value.
   */
  virtual std::string service_ip() const noexcept = 0;

  /**
   * Get the service port value.
   */
  virtual int service_port() const noexcept = 0;

};

} // namespace archive_reader

#endif // ARCHIVE_READER_CONFIG_INTERFACE_HPP_
