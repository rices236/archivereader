/*******************************************************************************************************************************
 * src/header/archive_reader/release.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_RELEASE_HPP_
#define ARCHIVE_READER_RELEASE_HPP_

#include <ostream>
#include <string>
#include <vector>

#include <boost/filesystem/path.hpp>
#include <nlohmann/json.hpp>

#include "archive_reader/track.hpp"

namespace archive_reader {

typedef std::vector<std::string> str_list;
typedef std::vector<Track> track_list;

/**
 * Read from JSON object and store release information.
 */
class Release
{
public:

  /**
   * Default constructor.
   */
  Release();

  /**
  * Get the title.
  */
  std::string title() const noexcept;

  /**
   * Get the list of artists.
   */
  str_list artists() const noexcept;

  /**
   * Get the composer.
   */
  std::string composer() const noexcept;

  /**
   * Get the list of performers.
   */
  str_list performers() const noexcept;

  /**
   * Get the catalog number.
   */
  std::string catalog_no() const noexcept;

  /**
   * Get the path to the cover art, relative to the release file.
   */
  std::string cover_art() const noexcept;

  /**
   * Get the musical genre.
   */
  std::string genre() const noexcept;

  /**
   * Get the list of musical styles.
   */
  str_list styles() const noexcept;

  /**
   * Returns true if the release is a compilation.
   */
  bool is_compilation() const noexcept;

  /**
   * Returns true if the release is a reissue.
   */
  bool is_reissue() const noexcept;

  /**
   * Get the record label.
   */
  std::string label() const noexcept;

  /**
   * Get the source identifier of the audio file.
   */
  std::string source() const noexcept;

  /**
   * Get the release year.
   */
  int year() const noexcept;

  /**
   * Get the original year of issue. Used mostly for reissued works. For instance, an original Ventures album may have been
   * released in 1962, and have been reissued many times since then by different labels. Since the Release::year() value is
   * the year of the particular release, the originalYear can be used to indicate the work's history.
   */
  int original_year() const noexcept;

  /**
   * Get the year of the performance.
   */
  int performance_year() const noexcept;

  /**
   * Get the comments.
   */
  str_list comments() const noexcept;

  /**
   * Get the list of tracks.
   */
  track_list tracks() const noexcept;

  /**
   * Get the full path of the release file.
   */
  std::string release_file_path() const noexcept;

  /**
   * Get the full path of the cover art file.
   */
  std::string cover_art_path() const noexcept;

  /**
   * Supports syntax such as: Release my_rel = j.get<Release>(); where 'j' is an nlohmann::json object.
   *
   * @param j A json object populated with Release information.
   * @param r A reference to a Release object to be written to.
   */
  friend void from_json(const nlohmann::json& j, Release& r);

  /**
   * Supports adding additional information from the path to the Release object.
   *
   * @param path The current path of the release data file.
   * @param r A reference to a Release object to be written to.
   */
  friend void from_path(const boost::filesystem::path& path, Release& r);

  /**
   * Supports streaming Release information to text suitable for logging and debugging.
   *
   * @param os The output stream to write to.
   * @param r The Release object for writing to the stream.
   * @return A reference to the output stream passed in the parameter.
   */
  friend std::ostream& operator<<(std::ostream& os, const Release& r);

private:
  std::string _title;
  str_list _artists;
  std::string _composer;
  str_list _performers;
  std::string _catalog_no;
  std::string _cover_art;
  std::string _genre;
  str_list _styles;
  bool _is_compilation;
  bool _is_reissue;
  std::string _label;
  std::string _source;
  int _year;
  int _original_year;
  int _performance_year;
  str_list _comments;
  track_list _tracks;
  std::string _release_file_path;
  std::string _cover_art_path;

};

} // namespace archive_reader

#endif // ARCHIVE_READER_RELEASE_HPP_
