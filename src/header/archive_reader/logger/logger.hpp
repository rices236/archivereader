/*******************************************************************************************************************************
 * src/header/archive_reader/logger/logger.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_LOGGER_LOGGER_HPP_
#define ARCHIVE_READER_LOGGER_LOGGER_HPP_

#include <spdlog/spdlog.h>

#ifdef DEBUG
#include "archive_reader/exception.hpp"
#endif

namespace archive_reader {
namespace logger {

/**
 * Log format options.
 */
enum class LogFormat { Default, Debug, Json };

/**
 * Log level options.
 */
enum class LogLevel { Off = 0, Critical, Error, Warn, Info, Debug, Trace };

} // namespace logger

// Declaring this namespace in the middle of the other so that we get the enum definitions.
namespace logger_impl {

inline bool _initialized;
inline const char* const _std_logger {"_std_logger"};
inline const char* const _err_logger {"_err_logger"};
inline const logger::LogFormat _default_format = logger::LogFormat::Default;
inline const logger::LogLevel _default_level = logger::LogLevel::Info;
inline logger::LogLevel _level;

} // namespace logger_impl

namespace logger {

/**
 * Initialize the logging service and set log options.
 *
 * @param format The log format to use.
 * @param level The log level to use.
 * @throw LogInitializationException Thrown on initialization errors.
 */
void init(const LogFormat format = logger_impl::_default_format, const LogLevel level = logger_impl::_default_level);

/**
 * Get the logging level set at initialization.
 *
 * @return The logging level.
 */
LogLevel level() noexcept;

/**
 * Whether the logging level was set to Off at initialization.
 *
 * @return True if the logging level was set to Off.
 */
bool is_off() noexcept;

/**
 * Whether the logging is at a specified level or higher.
 *
 * For example: Trace is considered a higher level than Debug since it will include Debug logging as well. Info is considered
 * a lower level than Debug since it will not include Debug logging.
 *
 * From lowest to highest, levels are: Off, Critical, Error, Warn, Info, Debug, Trace.
 *
 * @param level The specified level.
 * @return True if the logging level is at the specified level or higher.
 */
bool is_at_least(const LogLevel level) noexcept;

#ifdef DEBUG
/**
 * Get the standard logger.
 * @return The standard logger.
 * @throw ArchiveReaderException Thrown if logging has not been initialized.
 */
inline std::shared_ptr<spdlog::logger> get()
{
  if (!logger_impl::_initialized) {
    throw ArchiveReaderException("Logging has not been initialized.");
  }

  return spdlog::get(logger_impl::_std_logger);
}

/**
 * Get the error logger.
 * @return The error logger.
 * @throw ArchiveReaderException Thrown if logging has not been initialized.
 */
inline std::shared_ptr<spdlog::logger> err()
{
  if (!logger_impl::_initialized) {
    throw ArchiveReaderException("Logging has not been initialized.");
  }

  return spdlog::get(logger_impl::_err_logger);
}
#else
/**
 * Get the standard logger.
 * @return The standard logger.
 */
inline std::shared_ptr<spdlog::logger> get() noexcept
{
  return spdlog::get(logger_impl::_std_logger);
}

/**
 * Get the error logger.
 * @return The error logger.
 */
inline std::shared_ptr<spdlog::logger> err() noexcept
{
  return spdlog::get(logger_impl::_err_logger);
}
#endif // DEBUG

} // namespace logger
} // namespace archive_reader

#endif // ARCHIVE_READER_LOGGER_LOGGER_HPP_
