/*******************************************************************************************************************************
 * src/header/archive_reader/release_scan_handler.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_RELEASE_SCAN_HANDLER_HPP_
#define ARCHIVE_READER_RELEASE_SCAN_HANDLER_HPP_

#include <string>

#include <boost/filesystem/operations.hpp>

#include "archive_reader/db_writer.hpp"

namespace archive_reader {

/**
 * Functor for handling the scanning of release information from JSON files.
 */
class ReleaseScanHandler
{
public:

  /**
   * Class constructor.
   *
   * @param library_root The root path of the audio library.
   * @param release_file The name of release files to scan for.
   */
  ReleaseScanHandler(const std::string& library_root, const std::string& release_file, DBWriter* dbwriter);

  /**
   * Process the directory entry.
   *
   * @param dir_entry The directory entry to process.
   * @return True indicates scanning should continue; false indicates it should stop.
   */
  bool operator()(const boost::filesystem::directory_entry& dir_entry);

  /**
   * Get the number of release scans since last reset.
   */
  int scan_count() const noexcept;

  /**
   * Get the number of errors since last reset.
   */
  int err_count() const noexcept;

  /**
   * Reset counts to zero.
   */
  void reset() noexcept;

private:
  std::string _library_root;
  std::string _release_file;
  DBWriter* _dbwriter;
  int _scan_count;
  int _err_count;

  ReleaseScanHandler(const ReleaseScanHandler&) = delete;
  ReleaseScanHandler& operator=(const ReleaseScanHandler&) = delete;
};

} // namespace archive_reader

#endif // ARCHIVE_READER_RELEASE_SCAN_HANDLER_HPP_
