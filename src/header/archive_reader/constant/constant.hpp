/*******************************************************************************************************************************
 * src/header/archive_reader/constant/constant.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_CONSTANT_CONSTANT_HPP_
#define ARCHIVE_READER_CONSTANT_CONSTANT_HPP_

namespace archive_reader {
namespace constant {

inline const char ProjectName[] = {"Audimation"};
inline const char ServiceName[] = {"ArchiveReader"};

} // namespace constant
} // namespace archive_reader

#endif // ARCHIVE_READER_CONSTANT_CONSTANT_HPP_
