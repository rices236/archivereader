/*******************************************************************************************************************************
 * src/header/archive_reader/file_type.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_FILE_TYPE_HPP_
#define ARCHIVE_READER_FILE_TYPE_HPP_

#include <string>

#include <boost/filesystem/path.hpp>

namespace archive_reader {

/**
 * File types commonly used by this service.
 */
enum class FileType {
  Flac,
  Jpeg,
  Json,
  Wave,
  Mp3,
  Other
};

/**
 * Get the file type from the path.
 *
 * @param path The path of the file to identify.
 * @return The type of file, or 'Other' if not found.
 */
FileType file_type_from_path(const boost::filesystem::path& path) noexcept;

/**
 * Get the file type from the extension.
 *
 * @param file_extension The extension of the file to identify, including the dot: as in ".wav" but not "wav".
 * @return The type of file, or 'Other' if not found.
 */
FileType file_type_from_extension(const std::string& file_extension) noexcept;

/**
 * Get the string for the FileType enumeration value.
 *
 * @param file_type The file type.
 * @return The string for the file type.
 */
std::string file_type_to_string(const FileType file_type) noexcept;

} // namespace archive_reader

#endif // ARCHIVE_READER_FILE_TYPE_HPP_
