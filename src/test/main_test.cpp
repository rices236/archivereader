/*******************************************************************************************************************************
 * src/test/main_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "archive_reader/logger/logger.hpp"

namespace ar = archive_reader;

int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);

  ar::logger::init(ar::logger::LogFormat::Debug, ar::logger::LogLevel::Off);

  return RUN_ALL_TESTS();
}
