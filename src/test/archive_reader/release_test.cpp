/*******************************************************************************************************************************
 * src/test/archive_reader/release_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include <nlohmann/json.hpp>

#include "archive_reader/exception.hpp"
#include "archive_reader/release.hpp"

using nlohmann::json;

namespace archive_reader {

class ReleaseTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(ReleaseTest, default_ctor_creates_empty_values)
{
  Release tgt;
  EXPECT_TRUE(tgt.title().empty());
  EXPECT_TRUE(tgt.artists().empty());
  EXPECT_TRUE(tgt.composer().empty());
  EXPECT_TRUE(tgt.performers().empty());
  EXPECT_TRUE(tgt.catalog_no().empty());
  EXPECT_TRUE(tgt.cover_art().empty());
  EXPECT_TRUE(tgt.genre().empty());
  EXPECT_TRUE(tgt.styles().empty());
  EXPECT_FALSE(tgt.is_compilation());
  EXPECT_FALSE(tgt.is_reissue());
  EXPECT_TRUE(tgt.label().empty());
  EXPECT_TRUE(tgt.source().empty());
  EXPECT_EQ(0, tgt.year());
  EXPECT_EQ(0, tgt.original_year());
  EXPECT_EQ(0, tgt.performance_year());
  EXPECT_TRUE(tgt.comments().empty());
  EXPECT_TRUE(tgt.tracks().empty());
  EXPECT_TRUE(tgt.release_file_path().empty());
}

TEST_F(ReleaseTest, requires_title_only)
{
  json j = R"({"title":"Cool Water"})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  // throws if title missing
  j = R"({})"_json;
  EXPECT_THROW(j.get<Release>(), JsonSchemaException);
}

TEST_F(ReleaseTest, json_to_title)
{
  json j = R"({"title":"Truth Decay"})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ("Truth Decay", tgt.title());
}

TEST_F(ReleaseTest, json_to_artists)
{
  json j = R"({"title":"t", "artists":["Coldplay"]})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ(1, tgt.artists().size());
  EXPECT_EQ("Coldplay", tgt.artists()[0]);

  j = R"({"title":"t", "artists":["Lata Mangeshkar","Kishore Kumar","Amitabh Bachchan","Pamela Chopra"]})"_json;
  tgt = j.get<Release>();
  EXPECT_EQ(4, tgt.artists().size());
  EXPECT_EQ("Lata Mangeshkar", tgt.artists()[0]);
  EXPECT_EQ("Kishore Kumar", tgt.artists()[1]);
  EXPECT_EQ("Amitabh Bachchan", tgt.artists()[2]);
  EXPECT_EQ("Pamela Chopra", tgt.artists()[3]);

  j = R"({"title":"t", "artist":"Buell Kazee"})"_json;
  tgt = j.get<Release>();
  EXPECT_EQ(1, tgt.artists().size());
  EXPECT_EQ("Buell Kazee", tgt.artists()[0]);
}

TEST_F(ReleaseTest, throws_if_artists_not_an_array)
{
  json j = R"({"title":"t", "artists":"CBS Radio Mystery Theater"})"_json;
  EXPECT_THROW(j.get<Release>(), JsonSchemaException);
}

TEST_F(ReleaseTest, throws_if_artist_not_a_string)
{
  json j = R"({"title":"t", "artist":["E.G. Marshall","Hyman Brown"]})"_json;
  EXPECT_THROW(j.get<Release>(), JsonSchemaException);
}

TEST_F(ReleaseTest, json_to_composer)
{
  json j = R"({"title":"t", "composer":null})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  j = R"({"title":"t", "composer":"Palestrina, Giovanni Pierluigi da"})"_json;
  Release tgt = j.get<Release>();
}

TEST_F(ReleaseTest, json_to_performers)
{
  json j = R"({"title":"t", "performers":["La Petite Bande de Montréal"]})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ(1, tgt.performers().size());
  EXPECT_EQ("La Petite Bande de Montréal", tgt.performers()[0]);

  j = R"({"title":"t", "path":"p", "performers":["The Sixteen","Harry Christophers"]})"_json;
  tgt = j.get<Release>();
  EXPECT_EQ(2, tgt.performers().size());
  EXPECT_EQ("The Sixteen", tgt.performers()[0]);
  EXPECT_EQ("Harry Christophers", tgt.performers()[1]);

  j = R"({"title":"t", "performer":"Academy of St. Martin-in-the-Fields"})"_json;
  tgt = j.get<Release>();
  EXPECT_EQ(1, tgt.performers().size());
  EXPECT_EQ("Academy of St. Martin-in-the-Fields", tgt.performers()[0]);
}

TEST_F(ReleaseTest, throws_if_performers_not_an_array)
{
  json j = R"({"title":"t", "performers":"Boston Symphony Orchestra"})"_json;
  EXPECT_THROW(j.get<Release>(), JsonSchemaException);
}

TEST_F(ReleaseTest, throws_if_performer_not_a_string)
{
  json j = R"({"title":"t", "performer":["Lata Mangeshkar","Usha Mangeshkar"]})"_json;
  EXPECT_THROW(j.get<Release>(), JsonSchemaException);
}

TEST_F(ReleaseTest, json_to_catalog_no)
{
  json j = R"({"title":"t", "catalogNo":null})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  j = R"({"title":"t", "catalogNo":"CDP-72780"})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ("CDP-72780", tgt.catalog_no());
}

TEST_F(ReleaseTest, json_to_cover_art)
{
  json j = R"({"title":"t", "coverArt":null})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  j = R"({"title":"t", "coverArt":"./cover-cdp_72780.jpg"})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ("./cover-cdp_72780.jpg", tgt.cover_art());
}

TEST_F(ReleaseTest, json_to_genre)
{
  json j = R"({"title":"t", "genre":null})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  j = R"({"title":"t", "genre":"Rock&Roll"})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ("Rock&Roll", tgt.genre());
}

TEST_F(ReleaseTest, json_to_styles)
{
  json j = R"({"title":"t", "styles":["Alternative"]})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ(1, tgt.styles().size());
  EXPECT_EQ("Alternative", tgt.styles()[0]);

  j = R"({"title":"t", "path":"p", "styles":["Alternative","Goth"]})"_json;
  tgt = j.get<Release>();
  EXPECT_EQ(2, tgt.styles().size());
  EXPECT_EQ("Alternative", tgt.styles()[0]);
  EXPECT_EQ("Goth", tgt.styles()[1]);

  j = R"({"title":"t", "style":"Dance"})"_json;
  tgt = j.get<Release>();
  EXPECT_EQ(1, tgt.styles().size());
  EXPECT_EQ("Dance", tgt.styles()[0]);
}

TEST_F(ReleaseTest, throws_if_styles_not_an_array)
{
  json j = R"({"title":"t", "styles":"Opera"})"_json;
  EXPECT_THROW(j.get<Release>(), JsonSchemaException);
}

TEST_F(ReleaseTest, throws_if_style_not_a_string)
{
  json j = R"({"title":"t", "style":["Electronic","Europop"]})"_json;
  EXPECT_THROW(j.get<Release>(), JsonSchemaException);
}

TEST_F(ReleaseTest, json_to_is_compilation)
{
  json j = R"({"title":"t", "isCompilation":null})"_json;
  Release tgt;
  EXPECT_NO_THROW(tgt = j.get<Release>());
  EXPECT_FALSE(tgt.is_compilation());

  j = R"({"title":"t", "isCompilation":true})"_json;
  tgt = j.get<Release>();
  EXPECT_TRUE(tgt.is_compilation());
}

TEST_F(ReleaseTest, json_to_is_reissue)
{
  json j = R"({"title":"t", "isReissue":null})"_json;
  Release tgt;
  EXPECT_NO_THROW(tgt = j.get<Release>());
  EXPECT_FALSE(tgt.is_reissue());

  j = R"({"title":"t", "isReissue":true})"_json;
  tgt = j.get<Release>();
  EXPECT_TRUE(tgt.is_reissue());
}

TEST_F(ReleaseTest, json_to_label)
{
  json j = R"({"title":"t", "label":null})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  j = R"({"title":"t", "label":"415 Records"})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ("415 Records", tgt.label());
}

TEST_F(ReleaseTest, json_to_source)
{
  json j = R"({"title":"t", "source":null})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  j = R"({"title":"t", "source":"FLAC Download"})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ("FLAC Download", tgt.source());
}

TEST_F(ReleaseTest, json_to_year)
{
  json j = R"({"title":"t", "year":null})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  j = R"({"title":"t", "year":2008})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ(2008, tgt.year());
}

TEST_F(ReleaseTest, json_to_original_year)
{
  json j = R"({"title":"t", "originalYear":null})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  j = R"({"title":"t", "originalYear":1962})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ(1962, tgt.original_year());
}

TEST_F(ReleaseTest, json_to_performance_year)
{
  json j = R"({"title":"t", "performanceYear":null})"_json;
  EXPECT_NO_THROW(j.get<Release>());

  j = R"({"title":"t", "performanceYear":2001})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ(2001, tgt.performance_year());
}

TEST_F(ReleaseTest, json_to_comments)
{
  json j = R"({"title":"t", "comments":["This is a compilation of 2 albums"]})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ(1, tgt.comments().size());
  EXPECT_EQ("This is a compilation of 2 albums", tgt.comments()[0]);

  j = R"({"title":"t", "comments":["Recorded from vinyl","Track 7 skips at 1:34"]})"_json;
  tgt = j.get<Release>();
  EXPECT_EQ(2, tgt.comments().size());
  EXPECT_EQ("Recorded from vinyl", tgt.comments()[0]);
  EXPECT_EQ("Track 7 skips at 1:34", tgt.comments()[1]);

  j = R"({"title":"t", "comment":"Brian Wilson coughs"})"_json;
  tgt = j.get<Release>();
  EXPECT_EQ(1, tgt.comments().size());
  EXPECT_EQ("Brian Wilson coughs", tgt.comments()[0]);
}

TEST_F(ReleaseTest, throws_if_comments_not_an_array)
{
  json j = R"({"title":"t", "comments":"I'm running out of interesting comments"})"_json;
  EXPECT_THROW(j.get<Release>(), JsonSchemaException);
}

TEST_F(ReleaseTest, throws_if_comment_not_a_string)
{
  json j = R"({"title":"t", "path":"p", "style":["Recorded by the Wrecking! Crew"]})"_json;
  EXPECT_THROW(j.get<Release>(), JsonSchemaException);
}

TEST_F(ReleaseTest, json_to_tracks)
{
  json j = R"({"title":"The Tallis Scholars Sing Palestrina", "tracks":[
    {"title":"Assumpta est Maria in caelum","path":"./cd01/track01.wav"}
  ]})"_json;
  Release tgt = j.get<Release>();
  EXPECT_EQ("The Tallis Scholars Sing Palestrina", tgt.title());
  EXPECT_EQ(1, tgt.tracks().size());
  EXPECT_EQ("Assumpta est Maria in caelum", tgt.tracks()[0].title());
  EXPECT_EQ("./cd01/track01.wav", tgt.tracks()[0].path());

  j = R"!({"title":"DIY: Starry Eyes - UK Pop II (1978-79)", "isCompilation":true, "artist":"Various", "tracks":[
    {"title":"Girl of My Dreams","artist":"Bram Tchaikovsky","label":"Radarscope Records","path":"./track06.wav",
      "originalYear":1979},
    {"title":"Let's Talk About the Weather","artist":"The Radiators","label":"Chiswick Records","path":"./track11.wav",
      "originalYear":1979},
    {"title":"Starry Eyes","artist":"The Records","label":"Virgin Records","path":"./track12.wav","originalYear":1978}
  ]})!"_json;
  tgt = j.get<Release>();
  EXPECT_EQ("DIY: Starry Eyes - UK Pop II (1978-79)", tgt.title());
  EXPECT_TRUE(tgt.is_compilation());
  EXPECT_EQ("Various", tgt.artists()[0]);
  EXPECT_EQ(3, tgt.tracks().size());
  // track 6
  EXPECT_EQ("Girl of My Dreams", tgt.tracks()[0].title());
  EXPECT_EQ("Bram Tchaikovsky", tgt.tracks()[0].artists()[0]);
  EXPECT_EQ("Radarscope Records", tgt.tracks()[0].label());
  EXPECT_EQ("./track06.wav", tgt.tracks()[0].path());
  EXPECT_EQ(1979, tgt.tracks()[0].original_year());
  // track 11
  EXPECT_EQ("Let's Talk About the Weather", tgt.tracks()[1].title());
  EXPECT_EQ("The Radiators", tgt.tracks()[1].artists()[0]);
  EXPECT_EQ("Chiswick Records", tgt.tracks()[1].label());
  EXPECT_EQ("./track11.wav", tgt.tracks()[1].path());
  EXPECT_EQ(1979, tgt.tracks()[1].original_year());
  // track 12
  EXPECT_EQ("Starry Eyes", tgt.tracks()[2].title());
  EXPECT_EQ("The Records", tgt.tracks()[2].artists()[0]);
  EXPECT_EQ("Virgin Records", tgt.tracks()[2].label());
  EXPECT_EQ("./track12.wav", tgt.tracks()[2].path());
  EXPECT_EQ(1978, tgt.tracks()[2].original_year());
}

} // namespace archive_reader
