/*******************************************************************************************************************************
 * src/test/archive_reader/config_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "archive_reader/config.hpp"

namespace archive_reader {

class ConfigTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(ConfigTest, all_long_options)
{
  const char* argv[] {"progname", "--library-root=/library/path/here", "--release-file=xfile.json", "--db-id=1099",
                      "--db-size=1230000", "--service-ip=16.132.99.44", "--service-port=2134"
                     };
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ("/library/path/here", tgt.library_root());
  EXPECT_EQ("xfile.json", tgt.release_file());
  EXPECT_EQ(1099, tgt.db_id());
  EXPECT_EQ(1230000, tgt.db_size());
  EXPECT_EQ("16.132.99.44", tgt.service_ip());
  EXPECT_EQ(2134, tgt.service_port());
}

TEST_F(ConfigTest, all_short_options)
{
  const char* argv[] {"progname", "-s90109010", "-d505501", "-rsuccess.json", "-l/your/path/to/success", "-i162.18.1.28",
                      "-p6177"
                     };
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ("/your/path/to/success", tgt.library_root());
  EXPECT_EQ("success.json", tgt.release_file());
  EXPECT_EQ(505501, tgt.db_id());
  EXPECT_EQ(90109010, tgt.db_size());
  EXPECT_EQ("162.18.1.28", tgt.service_ip());
  EXPECT_EQ(6177, tgt.service_port());
}

TEST_F(ConfigTest, copy_ctor__copies_members)
{
  const char* argv[] {"progname", "-s16000000", "-d1110", "-ra-file.json", "-l/a/path"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config cfg(argc, const_cast<char**>(argv));
  Config tgt {cfg};

  // Copied from should still have the same values
  EXPECT_EQ("/a/path", cfg.library_root());
  EXPECT_EQ("a-file.json", cfg.release_file());
  EXPECT_EQ(1110, cfg.db_id());
  EXPECT_EQ(16000000, cfg.db_size());

  // Copied to should have the same values
  EXPECT_EQ("/a/path", tgt.library_root());
  EXPECT_EQ("a-file.json", tgt.release_file());
  EXPECT_EQ(1110, tgt.db_id());
  EXPECT_EQ(16000000, tgt.db_size());
}

TEST_F(ConfigTest, move_ctor__moves_members)
{
  const char* argv[] {"progname", "-s16000000", "-d1110", "-ra-file.json", "-l/a/path"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config cfg(argc, const_cast<char**>(argv));
  Config tgt(std::move(cfg));

  // Moved from string data should now be empty
  EXPECT_EQ("", cfg.library_root());
  EXPECT_EQ("", cfg.release_file());

  // Moved to should have the same values
  EXPECT_EQ("/a/path", tgt.library_root());
  EXPECT_EQ("a-file.json", tgt.release_file());
  EXPECT_EQ(1110, tgt.db_id());
  EXPECT_EQ(16000000, tgt.db_size());
}

TEST_F(ConfigTest, copy_assignment__copies_members)
{
  const char* argv1[] {"progname", "-s16000000", "-d1110", "-ra-file.json", "-l/a/path"};
  const int argc1 = sizeof(argv1) / sizeof(argv1[0]);
  Config cfg(argc1, const_cast<char**>(argv1));
  const char* argv2[] {"progname"};
  const int argc2 = sizeof(argv2) / sizeof(argv2[0]);
  Config tgt(argc2, const_cast<char**>(argv2));

  // Target should have default values
  EXPECT_EQ("", tgt.library_root());
  EXPECT_EQ(Config::release_file_default(), tgt.release_file());
  EXPECT_EQ(Config::db_id_default(), tgt.db_id());
  EXPECT_EQ(Config::db_size_default(), tgt.db_size());

  // Do the copy assignment operation
  tgt = cfg;

  // Copied from should still have the same values
  EXPECT_EQ("/a/path", cfg.library_root());
  EXPECT_EQ("a-file.json", cfg.release_file());
  EXPECT_EQ(1110, cfg.db_id());
  EXPECT_EQ(16000000, cfg.db_size());

  // Copied to should now have the same values
  EXPECT_EQ("/a/path", tgt.library_root());
  EXPECT_EQ("a-file.json", tgt.release_file());
  EXPECT_EQ(1110, tgt.db_id());
  EXPECT_EQ(16000000, tgt.db_size());
}

TEST_F(ConfigTest, move_assignment__moves_members)
{
  const char* argv1[] {"progname", "-s16000000", "-d1110", "-ra-file.json", "-l/a/path"};
  const int argc1 = sizeof(argv1) / sizeof(argv1[0]);
  Config cfg(argc1, const_cast<char**>(argv1));
  const char* argv2[] {"progname"};
  const int argc2 = sizeof(argv2) / sizeof(argv2[0]);
  Config tgt(argc2, const_cast<char**>(argv2));

  // Target should have default values
  EXPECT_EQ("", tgt.library_root());
  EXPECT_EQ(Config::release_file_default(), tgt.release_file());
  EXPECT_EQ(Config::db_id_default(), tgt.db_id());
  EXPECT_EQ(Config::db_size_default(), tgt.db_size());

  // Do the move assignment operation
  tgt = std::move(cfg);

  // Moved from string objects should now be empty
  EXPECT_EQ("", cfg.library_root());
  EXPECT_EQ("", cfg.release_file());

  // Moved to should now have the moved values
  EXPECT_EQ("/a/path", tgt.library_root());
  EXPECT_EQ("a-file.json", tgt.release_file());
  EXPECT_EQ(1110, tgt.db_id());
  EXPECT_EQ(16000000, tgt.db_size());
}

TEST_F(ConfigTest, library_root__option_long)
{
  const char* argv[] {"progname", "--library-root=/path/to/library"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ("/path/to/library", tgt.library_root());
}

TEST_F(ConfigTest, library_root__option_short)
{
  const char* argv[] {"progname", "-l/your/library/root/here"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ("/your/library/root/here", tgt.library_root());
}

TEST_F(ConfigTest, library_root__default)
{
  const char* argv[] {"progname"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ("", tgt.library_root());
}

TEST_F(ConfigTest, release_file__option_long)
{
  const char* argv[] {"progname", "--release-file=my_rel"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ("my_rel", tgt.release_file());
}

TEST_F(ConfigTest, release_file__option_short)
{
  const char* argv[] {"progname", "-rtheir_rel"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ("their_rel", tgt.release_file());
}

TEST_F(ConfigTest, release_file__default)
{
  const char* argv[] {"progname"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(Config::release_file_default(), tgt.release_file());
}

TEST_F(ConfigTest, db_id__option_long)
{
  const char* argv[] {"progname", "--db-id=1021"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(1021, tgt.db_id());
}

TEST_F(ConfigTest, db_id__option_short)
{
  const char* argv[] {"progname", "-d7891"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(7891, tgt.db_id());
}

TEST_F(ConfigTest, db_id__default)
{
  const char* argv[] {"progname"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(Config::db_id_default(), tgt.db_id());
}

TEST_F(ConfigTest, db_size__option_long)
{
  const char* argv[] {"progname", "--db-size=20000000"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(20000000, tgt.db_size());
}

TEST_F(ConfigTest, db_size__option_short)
{
  const char* argv[] {"progname", "-s33000000"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(33000000, tgt.db_size());
}

TEST_F(ConfigTest, db_size__default)
{
  const char* argv[] {"progname"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(Config::db_size_default(), tgt.db_size());
}

TEST_F(ConfigTest, service_ip__option_long)
{
  const char* argv[] {"progname", "--service-ip=66.12.6.47"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ("66.12.6.47", tgt.service_ip());
}

TEST_F(ConfigTest, service_ip__option_short)
{
  const char* argv[] {"progname", "-iremotehost"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ("remotehost", tgt.service_ip());
}

TEST_F(ConfigTest, service_ip__default)
{
  const char* argv[] {"progname"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(Config::service_ip_default(), tgt.service_ip());
}

TEST_F(ConfigTest, service_port__option_long)
{
  const char* argv[] {"progname", "--service-port=18788"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(18788, tgt.service_port());
}

TEST_F(ConfigTest, service_port__option_short)
{
  const char* argv[] {"progname", "-p38104"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(38104, tgt.service_port());
}

TEST_F(ConfigTest, service_port__default)
{
  const char* argv[] {"progname"};
  const int argc = sizeof(argv) / sizeof(argv[0]);
  Config tgt(argc, const_cast<char**>(argv));
  EXPECT_EQ(Config::service_port_default(), tgt.service_port());
}

} // namespace archive_reader
