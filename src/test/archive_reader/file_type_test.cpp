/*******************************************************************************************************************************
 * src/test/archive_reader/file_type_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "archive_reader/file_type.hpp"

namespace bfs = boost::filesystem;

namespace archive_reader {

class FileTypeTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(FileTypeTest, file_type_to_string)
{
  EXPECT_EQ("Flac", file_type_to_string(FileType::Flac));
  EXPECT_EQ("Jpeg", file_type_to_string(FileType::Jpeg));
  EXPECT_EQ("Json", file_type_to_string(FileType::Json));
  EXPECT_EQ("Wave", file_type_to_string(FileType::Wave));
  EXPECT_EQ("Mp3", file_type_to_string(FileType::Mp3));
  EXPECT_EQ("Other", file_type_to_string(FileType::Other));
}

TEST_F(FileTypeTest, flac_file_type_from_extension)
{
  EXPECT_EQ(FileType::Flac, file_type_from_extension(".flac"));
  EXPECT_EQ(FileType::Flac, file_type_from_extension(".FLAC"));
  EXPECT_NE(FileType::Flac, file_type_from_extension("flac"));
  EXPECT_NE(FileType::Flac, file_type_from_extension("FLAC"));
  EXPECT_NE(FileType::Flac, file_type_from_extension(".fla"));
  EXPECT_NE(FileType::Flac, file_type_from_extension(".flc"));
  EXPECT_NE(FileType::Flac, file_type_from_extension(".Flac"));
}

TEST_F(FileTypeTest, flac_file_type_from_path)
{
  EXPECT_EQ(FileType::Flac, file_type_from_path(bfs::path("/home/somefile.flac")));
  EXPECT_EQ(FileType::Flac, file_type_from_path(bfs::path("/home/somefile.FLAC")));
  EXPECT_NE(FileType::Flac, file_type_from_path(bfs::path("/home/somefile.Flac")));
}

TEST_F(FileTypeTest, jpeg_file_type_from_extension)
{
  EXPECT_EQ(FileType::Jpeg, file_type_from_extension(".jpeg"));
  EXPECT_EQ(FileType::Jpeg, file_type_from_extension(".JPEG"));
  EXPECT_EQ(FileType::Jpeg, file_type_from_extension(".jpg"));
  EXPECT_EQ(FileType::Jpeg, file_type_from_extension(".JPG"));
  EXPECT_NE(FileType::Jpeg, file_type_from_extension("jpeg"));
  EXPECT_NE(FileType::Jpeg, file_type_from_extension("JPEG"));
  EXPECT_NE(FileType::Jpeg, file_type_from_extension("jpg"));
  EXPECT_NE(FileType::Jpeg, file_type_from_extension("JPG"));
  EXPECT_NE(FileType::Jpeg, file_type_from_extension(".Jpeg"));
  EXPECT_NE(FileType::Jpeg, file_type_from_extension(".Jpg"));
}

TEST_F(FileTypeTest, jpeg_file_type_from_path)
{
  EXPECT_EQ(FileType::Jpeg, file_type_from_path(bfs::path("/home/somefile.jpeg")));
  EXPECT_EQ(FileType::Jpeg, file_type_from_path(bfs::path("/home/somefile.JPEG")));
  EXPECT_EQ(FileType::Jpeg, file_type_from_path(bfs::path("/home/somefile.jpg")));
  EXPECT_EQ(FileType::Jpeg, file_type_from_path(bfs::path("/home/somefile.JPG")));
  EXPECT_NE(FileType::Jpeg, file_type_from_path(bfs::path("/home/somefile.Jpeg")));
}

TEST_F(FileTypeTest, json_file_type_from_extension)
{
  EXPECT_EQ(FileType::Json, file_type_from_extension(".json"));
  EXPECT_EQ(FileType::Json, file_type_from_extension(".JSON"));
  EXPECT_NE(FileType::Json, file_type_from_extension("json"));
  EXPECT_NE(FileType::Json, file_type_from_extension("JSON"));
  EXPECT_NE(FileType::Json, file_type_from_extension(".jsn"));
  EXPECT_NE(FileType::Json, file_type_from_extension(".JSN"));
  EXPECT_NE(FileType::Json, file_type_from_extension(".Json"));
  EXPECT_NE(FileType::Json, file_type_from_extension(".jso"));
}

TEST_F(FileTypeTest, json_file_type_from_path)
{
  EXPECT_EQ(FileType::Json, file_type_from_path(bfs::path("/home/somefile.json")));
  EXPECT_EQ(FileType::Json, file_type_from_path(bfs::path("/home/somefile.JSON")));
  EXPECT_NE(FileType::Json, file_type_from_path(bfs::path("/home/somefile.Json")));
}

TEST_F(FileTypeTest, wave_file_type_from_extension)
{
  EXPECT_EQ(FileType::Wave, file_type_from_extension(".wav"));
  EXPECT_EQ(FileType::Wave, file_type_from_extension(".WAV"));
  EXPECT_EQ(FileType::Wave, file_type_from_extension(".wave"));
  EXPECT_EQ(FileType::Wave, file_type_from_extension(".WAVE"));
  EXPECT_NE(FileType::Wave, file_type_from_extension("wav"));
  EXPECT_NE(FileType::Wave, file_type_from_extension("WAV"));
  EXPECT_NE(FileType::Wave, file_type_from_extension("wave"));
  EXPECT_NE(FileType::Wave, file_type_from_extension("WAVE"));
  EXPECT_NE(FileType::Wave, file_type_from_extension(".Wave"));
}

TEST_F(FileTypeTest, wave_file_type_from_path)
{
  EXPECT_EQ(FileType::Wave, file_type_from_path(".wave"));
  EXPECT_EQ(FileType::Wave, file_type_from_path(".WAVE"));
  EXPECT_EQ(FileType::Wave, file_type_from_path(".wav"));
  EXPECT_EQ(FileType::Wave, file_type_from_path(".WAV"));
  EXPECT_NE(FileType::Wave, file_type_from_path(".Wav"));
}

TEST_F(FileTypeTest, mp3_file_type_from_extension)
{
  EXPECT_EQ(FileType::Mp3, file_type_from_extension(".mp3"));
  EXPECT_EQ(FileType::Mp3, file_type_from_extension(".MP3"));
  EXPECT_NE(FileType::Mp3, file_type_from_extension("mp3"));
  EXPECT_NE(FileType::Mp3, file_type_from_extension("MP3"));
  EXPECT_NE(FileType::Mp3, file_type_from_extension(".Mp3"));
}

TEST_F(FileTypeTest, mp3_file_type_from_path)
{
  EXPECT_EQ(FileType::Mp3, file_type_from_path(bfs::path("/home/somefile.mp3")));
  EXPECT_EQ(FileType::Mp3, file_type_from_path(bfs::path("/home/somefile.MP3")));
  EXPECT_NE(FileType::Mp3, file_type_from_path(bfs::path("/home/somefile.Mp3")));
}

} // namespace archive_reader
