/*******************************************************************************************************************************
 * src/test/archive_reader/whitedb/field_test.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/test_helper.hpp"

#include <string>

#include "archive_reader/whitedb/record_handle.hpp"

namespace archive_reader {
namespace whitedb {

TestHelper::TestHelper()
  : _db_handle {}
{
  void* dbh = wg_attach_database(const_cast<char*>(std::to_string(db_id).c_str()), db_size);
  if (dbh == nullptr) {
    throw "No database attached.";
  }

  _db_handle = dbh;
}

TestHelper::~TestHelper()
{
  wg_detach_database(_db_handle);
}

void* TestHelper::db_handle() const noexcept
{
  return _db_handle;
}

void* TestHelper::create_record(const int length)
{
  void* rch = wg_create_record(_db_handle, length);
  if (rch == nullptr) {
    throw "No record created.";
  }

  return rch;
}

void TestHelper::write_field(void* rec, const wg_int field_no, const int data)
{
  wg_int enc = wg_encode_int(_db_handle, data);
  if (enc == WG_ILLEGAL) {
    throw "Failed to encode the integer data.";
  }
  set_field_enc(rec, field_no, enc);
}

void TestHelper::write_field(void* rec, const wg_int field_no, const std::string& data)
{
  wg_int enc = wg_encode_str(_db_handle, const_cast<char*>(data.c_str()), NULL);
  if (enc == WG_ILLEGAL) {
    throw "Failed to encode the string data.";
  }
  set_field_enc(rec, field_no, enc);
}

void TestHelper::write_field(void* rec, const wg_int field_no, const char data)
{
  wg_int enc = wg_encode_char(_db_handle, data);
  if (enc == WG_ILLEGAL) {
    throw "Failed to encode the string data.";
  }
  set_field_enc(rec, field_no, enc);
}

void TestHelper::write_field(void* rec, const wg_int field_no, void* data)
{
  wg_int enc = wg_encode_record(_db_handle, data);
  if (enc == WG_ILLEGAL) {
    throw "Failed to encode the integer data.";
  }
  set_field_enc(rec, field_no, enc);
}

int& TestHelper::read_field(void* rec, const wg_int field_no, int& data)
{
  wg_int enc = wg_get_field(_db_handle, rec, field_no);
  data = wg_decode_int(_db_handle, enc);

  return data;
}

std::string& TestHelper::read_field(void* rec, const wg_int field_no, std::string& data)
{
  wg_int enc = wg_get_field(_db_handle, rec, field_no);
  char* str = wg_decode_str(_db_handle, enc);
  if (str == nullptr) {
    throw "Failed to decode a string field.";
  }
  data = str;

  return data;
}

char& TestHelper::read_field(void* rec, const wg_int field_no, char& data)
{
  wg_int enc = wg_get_field(_db_handle, rec, field_no);
  data = wg_decode_char(_db_handle, enc);

  return data;
}

void*& TestHelper::read_field(void* rec, const wg_int field_no, void*& data)
{
  wg_int enc = wg_get_field(_db_handle, rec, field_no);
  data = wg_decode_record(_db_handle, enc);

  return data;
}

void TestHelper::set_field_enc(void* rec, const wg_int field_no, const wg_int enc)
{
  int res = wg_set_field(_db_handle, rec, field_no, enc);
  if (res != 0) {
    throw "Failed to set the field.";
  }
}

} // namespace whitedb
} // namespace archive_reader
