/*******************************************************************************************************************************
 * src/test/archive_reader/whitedb/test_helper.hpp
 ******************************************************************************************************************************/

#ifndef ARCHIVE_READER_WHITEDB_TEST_HELPER_HPP_
#define ARCHIVE_READER_WHITEDB_TEST_HELPER_HPP_

#include <string>

#include <whitedb/dbapi.h>

namespace archive_reader {
namespace whitedb {

inline const int db_id = 7123;
inline const int db_size = 0x100000;
inline const int larger_db_size = 0x200000;

class TestHelper
{
public:

  /**
   * Default constructor.
   *
   * Initializes a local memory database that can be used for testing.
   *
   * @throw std::exception Thrown if the database could not be created or attached.
   */
  TestHelper();

  /**
   * Class destructor.
   */
  ~TestHelper();

  /**
   * Get the database handle used by the WhiteDB API.
   */
  void* db_handle() const noexcept;

  /**
   * Create a record.
   *
   * @param length The length (number of fields) of the created record.
   * @return A handle to the created record.
   * @throw std::exception Thrown if the operation fails.
   */
  void* create_record(int length);

  /**
   * Set an integer field in a record.
   *
   * @param rec The record to write to.
   * @param field_no The index of the field within the record.
   * @param data The data to write to the field.
   * @throw std::exception Thrown if the operation fails.
   */
  void write_field(void* rec, const wg_int field_no, const int data);

  /**
   * Set a std::string field in a record.
   *
   * @param rec The record to write to.
   * @param field_no The index of the field within the record.
   * @param data The data to write to the field.
   * @throw std::exception Thrown if the operation fails.
   */
  void write_field(void* rec, const wg_int field_no, const std::string& data);

  /**
   * Set a char field in a record.
   *
   * @param rec The record to write to.
   * @param field_no The index of the field within the record.
   * @param data The data to write to the field.
   * @throw std::exception Thrown if the operation fails.
   */
  void write_field(void* rec, const wg_int field_no, const char data);

  /**
   * Set a record pointer field in a record.
   *
   * @param rec The record to write to.
   * @param field_no The index of the field within the record.
   * @param data The data to write to the field.
   * @throw std::exception Thrown if the operation fails.
   */
  void write_field(void* rec, const wg_int field_no, void* data);

  /**
   * Read an integer field from a record.
   *
   * @param rec The record to read from.
   * @param field_no The index of the record field.
   * @param data Reference to the output data.
   * @return A reference to the output data.
   */
  int& read_field(void* rec, const wg_int field_no, int& data);

  /**
   * Read a std::string field from a record.
   *
   * @param rec The record to read from.
   * @param field_no The index of the record field.
   * @param data Reference to the output data.
   * @return A reference to the output data.
   * @throw std::exception Thrown if the operation fails.
   */
  std::string& read_field(void* rec, const wg_int field_no, std::string& data);

  /**
   * Read a char field from a record.
   *
   * @param rec The record to read from.
   * @param field_no The index of the record field.
   * @param data Reference to the output data.
   * @return A reference to the output data.
   */
  char& read_field(void* rec, const wg_int field_no, char& data);

  /**
   * Read a record pointer field from a record.
   *
   * @param rec The record to read from.
   * @param field_no The index of the record field.
   * @param data Reference to the output data.
   * @return A reference to the output data.
   */
  void*& read_field(void* rec, const wg_int field_no, void*& data);

private:

  /**
   * Set a record field to an encoded value.
   *
   * @param rec The record to write to.
   * @param field_no The index of the field within the record.
   * @param data The encoded data to write to the field.
   * @throw std::exception Thrown if the operation fails.
   */
  void set_field_enc(void* rec, const wg_int field_no, const wg_int enc);

  void* _db_handle;

  TestHelper(const TestHelper& other) = delete;
  TestHelper(TestHelper&& other) = delete;
  TestHelper& operator=(const TestHelper& other) = delete;
  TestHelper& operator=(TestHelper&& other) = delete;
};

} // namespace whitedb
} // namespace archive_reader

#endif // ARCHIVE_READER_WHITEDB_TEST_HELPER_HPP_
