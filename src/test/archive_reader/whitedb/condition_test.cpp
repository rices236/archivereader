/*******************************************************************************************************************************
 * src/test/archive_reader/whitedb/condition_test.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/condition.hpp"

#include <sstream>

#include <gtest/gtest.h>

namespace archive_reader {
namespace whitedb {

class ConditionTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(ConditionTest, condition_to_name__returns_name)
{
  ASSERT_EQ("Equal", condition_to_name(Condition::Equal));
  ASSERT_EQ("NotEqual", condition_to_name(Condition::NotEqual));
  ASSERT_EQ("LessThan", condition_to_name(Condition::LessThan));
  ASSERT_EQ("GreaterThan", condition_to_name(Condition::GreaterThan));
  ASSERT_EQ("EqualOrLess", condition_to_name(Condition::EqualOrLess));
  ASSERT_EQ("EqualOrGreater", condition_to_name(Condition::EqualOrGreater));
}

TEST_F(ConditionTest, to_wgdb_condition__returns_condition)
{
  ASSERT_EQ(WG_COND_EQUAL, to_wgdb_condition(Condition::Equal));
  ASSERT_EQ(WG_COND_NOT_EQUAL, to_wgdb_condition(Condition::NotEqual));
  ASSERT_EQ(WG_COND_LESSTHAN, to_wgdb_condition(Condition::LessThan));
  ASSERT_EQ(WG_COND_GREATER, to_wgdb_condition(Condition::GreaterThan));
  ASSERT_EQ(WG_COND_LTEQUAL, to_wgdb_condition(Condition::EqualOrLess));
  ASSERT_EQ(WG_COND_GTEQUAL, to_wgdb_condition(Condition::EqualOrGreater));
}

TEST_F(ConditionTest, streaming_operator_returns_condition_string)
{
  std::stringstream ss;
  ss << Condition::EqualOrLess << Condition::EqualOrGreater <<
     Condition::LessThan << Condition::GreaterThan <<
     Condition::Equal << Condition::NotEqual;
  ASSERT_EQ(ss.str(),
            condition_to_name(Condition::EqualOrLess) + condition_to_name(Condition::EqualOrGreater) +
            condition_to_name(Condition::LessThan) + condition_to_name(Condition::GreaterThan) +
            condition_to_name(Condition::Equal) + condition_to_name(Condition::NotEqual));
}

} // namespace whitedb
} // namespace archive_reader
