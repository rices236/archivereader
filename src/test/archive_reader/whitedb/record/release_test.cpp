/*******************************************************************************************************************************
 * src/test/archive_reader/whitedb/record/release_test.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/record/release.hpp"

#include <gtest/gtest.h>

#include <nlohmann/json.hpp>

namespace archive_reader {
namespace whitedb {
namespace record {

using record::Release;
using nlohmann::json;

class Record_ReleaseTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

const json jrel = R"#!({
  "artist": "The Ventures",
  "catalogNo": "72438 18928 24",
  "comments": [
    "Compilation of 2 albums: \"Surfing,\" and \"The Colorful Ventures\"",
    "Tracks 25 - 27 are bonus tracks"
  ],
  "coverArt": "./cover-72438_18928_24.jpg",
  "genre": "Rock",
  "isCompilation": true,
  "isReissue": true,
  "label": "One Way Records",
  "source": "CD (18928)",
  "styles": [
    "Instrumental",
    "Surf"
  ],
  "title": "The Ventures - Surfing / The Colorful Ventures",
  "tracks": [
    {
      "originalYear": 1963,
      "path": "./track01.wav",
      "title": "Pipeline"
    },
    {
      "originalYear": 1963,
      "path": "./track02.wav",
      "title": "Diamonds"
    },
    {
      "originalYear": 1963,
      "path": "./track03.wav",
      "title": "Windy and Warm"
    },
    {
      "originalYear": 1963,
      "path": "./track04.wav",
      "title": "Ten Over"
    },
    {
      "originalYear": 1963,
      "path": "./track05.wav",
      "title": "Surf Rider"
    },
    {
      "originalYear": 1963,
      "path": "./track06.wav",
      "title": "Changing Tides"
    },
    {
      "originalYear": 1963,
      "path": "./track07.wav",
      "title": "The Ninth Wave"
    },
    {
      "originalYear": 1963,
      "path": "./track08.wav",
      "title": "Party in Laguna"
    },
    {
      "originalYear": 1963,
      "path": "./track09.wav",
      "title": "Barefoot Venture"
    },
    {
      "originalYear": 1963,
      "path": "./track10.wav",
      "title": "The Heavies"
    },
    {
      "originalYear": 1963,
      "path": "./track11.wav",
      "title": "Cruncher"
    },
    {
      "originalYear": 1963,
      "path": "./track12.wav",
      "title": "The Lonely Sea"
    },
    {
      "originalYear": 1961,
      "path": "./track13.wav",
      "title": "Blue Moon"
    },
    {
      "originalYear": 1961,
      "path": "./track14.wav",
      "title": "Yellow Jacket"
    },
    {
      "originalYear": 1961,
      "path": "./track15.wav",
      "title": "Bluer than Blue"
    },
    {
      "originalYear": 1961,
      "path": "./track16.wav",
      "title": "Cherry Pink and Apple Blossom White"
    },
    {
      "originalYear": 1961,
      "path": "./track17.wav",
      "title": "Green Leaves of Summer"
    },
    {
      "originalYear": 1961,
      "path": "./track18.wav",
      "title": "Blue Skies"
    },
    {
      "originalYear": 1961,
      "path": "./track19.wav",
      "title": "Greenfields"
    },
    {
      "originalYear": 1961,
      "path": "./track20.wav",
      "title": "Red Top"
    },
    {
      "originalYear": 1961,
      "path": "./track21.wav",
      "title": "White Silver Sands"
    },
    {
      "originalYear": 1961,
      "path": "./track22.wav",
      "title": "Yellow Bird"
    },
    {
      "originalYear": 1961,
      "path": "./track23.wav",
      "title": "Orange Fire"
    },
    {
      "originalYear": 1961,
      "path": "./track24.wav",
      "title": "Silver City"
    },
    {
      "originalYear": 1962,
      "path": "./track25.wav",
      "title": "Genesis"
    },
    {
      "originalYear": 1963,
      "path": "./track26.wav",
      "title": "Skip to M'Limbo"
    },
    {
      "originalYear": 1965,
      "path": "./track27.wav",
      "title": "Pedal Pusher (live)"
    }
  ],
  "year": 1996
})#!"_json;

TEST_F(Record_ReleaseTest, default_ctor__initializes_empty_members)
{
  Release tgt;
  ASSERT_EQ("", tgt.title());
  ASSERT_EQ("", tgt.catalog_no());
  ASSERT_FALSE(tgt.is_compilation());
  ASSERT_FALSE(tgt.is_reissue());
  ASSERT_EQ("", tgt.source());
  ASSERT_EQ(0, tgt.year());
  ASSERT_EQ(0, tgt.original_year());
  ASSERT_EQ(0, tgt.performance_year());
  ASSERT_EQ("", tgt.release_file_path());
  ASSERT_EQ("", tgt.cover_art_path());
  ASSERT_EQ({}, tgt.composer());
  ASSERT_EQ({}, tgt.genre());
  ASSERT_EQ({}, tgt.label());
}

TEST_F(Record_ReleaseTest, ctor__initializes_members_from_release)
{
  Release tgt {jrel.get<archive_reader::Release>()};
  ASSERT_EQ("The Ventures - Surfing / The Colorful Ventures", tgt.title());
  ASSERT_EQ("72438 18928 24", tgt.catalog_no());
  ASSERT_TRUE(tgt.is_compilation());
  ASSERT_TRUE(tgt.is_reissue());
  ASSERT_EQ("CD (18928)", tgt.source());
  ASSERT_EQ(1996, tgt.year());
  ASSERT_EQ(0, tgt.original_year());
  ASSERT_EQ(0, tgt.performance_year());
  ASSERT_EQ("", tgt.release_file_path());
  ASSERT_EQ("", tgt.cover_art_path());
  ASSERT_EQ({}, tgt.composer());
  ASSERT_EQ({}, tgt.genre());
  ASSERT_EQ({}, tgt.label());
}

} // namespace record
} // namespace whitedb
} // namespace archive_reader
