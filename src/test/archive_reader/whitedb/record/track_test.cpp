/*******************************************************************************************************************************
 * src/test/archive_reader/whitedb/record/track_test.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/record/track.hpp"

#include <gtest/gtest.h>

#include <nlohmann/json.hpp>

namespace archive_reader {
namespace whitedb {
namespace record {

using record::Track;
using nlohmann::json;

class Record_TrackTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

const json jrel = R"#!({
  "artist": "Starjets",
  "label": "Sony Music Entertainment",
  "originalYear": 1978,
  "path": "./track05.wav",
  "title": "Schooldays"
})#!"_json;

TEST_F(Record_TrackTest, default_ctor__initializes_empty_members)
{
  Track tgt;
  ASSERT_EQ({}, tgt.release());
  ASSERT_EQ(0, tgt.order());
  ASSERT_EQ("", tgt.title());
  ASSERT_EQ(0, tgt.original_year());
  ASSERT_EQ(0, tgt.performance_year());
  ASSERT_EQ({}, tgt.composer());
  ASSERT_EQ({}, tgt.genre());
  ASSERT_EQ({}, tgt.label());
  ASSERT_EQ("", tgt.file_path());
}

TEST_F(Record_TrackTest, ctor__initializes_members_from_track)
{
  Track tgt {jrel.get<archive_reader::Track>()};
  ASSERT_EQ({}, tgt.release());
  ASSERT_EQ(0, tgt.order());
  ASSERT_EQ("Schooldays", tgt.title());
  ASSERT_EQ(1978, tgt.original_year());
  ASSERT_EQ(0, tgt.performance_year());
  ASSERT_EQ({}, tgt.composer());
  ASSERT_EQ({}, tgt.genre());
  ASSERT_EQ({}, tgt.label());
  ASSERT_EQ("", tgt.file_path());
}

} // namespace record
} // namespace whitedb
} // namespace archive_reader
