/*******************************************************************************************************************************
 * src/test/archive_reader/whitedb/database_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "archive_reader/whitedb/database.hpp"
#include "archive_reader/whitedb/exception.hpp"

namespace archive_reader {
namespace whitedb {

const int db_id = 7123;
const int db_size = 0x100000;
const int larger_db_size = 0x200000;

class DatabaseTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(DatabaseTest, create_local__creates_a_local_db)
{
  Database db = Database::create_local(db_size);
  ASSERT_EQ(0, db.id());
  ASSERT_TRUE(db.handle());
  ASSERT_TRUE(db.is_local());
  ASSERT_TRUE(db.is_attached());
  ASSERT_FALSE(db.is_deleted());

  // clean up
  db.delete_();
}

TEST_F(DatabaseTest, local_db_cant_be_detached)
{
  Database db = Database::create_local(db_size);
  ASSERT_EQ(0, db.id());
  ASSERT_TRUE(db.handle());
  db.detach();
  ASSERT_TRUE(db.is_attached());

  // clean up
  db.delete_();
}

TEST_F(DatabaseTest, local_db_indicates_unattached_and_deleted_after_delete)
{
  Database db = Database::create_local(db_size);
  ASSERT_EQ(0, db.id());
  ASSERT_LT(static_cast<void*>(0), db.handle());
  db.delete_();

  ASSERT_TRUE(db.is_local());
  ASSERT_FALSE(db.is_attached());
  ASSERT_TRUE(db.is_deleted());
}

TEST_F(DatabaseTest, creates_shared_db)
{
  Database db = Database::create_shared(db_id, db_size);
  ASSERT_EQ(db_id, db.id());
  ASSERT_LT(static_cast<void*>(0), db.handle());
  ASSERT_FALSE(db.is_local());
  ASSERT_TRUE(db.is_attached());
  ASSERT_FALSE(db.is_deleted());

  // clean up
  db.detach();
  db.delete_();
}

TEST_F(DatabaseTest, attaches_to_existing_shared_db)
{
  Database db1 = Database::create_shared(db_id, db_size);
  Database db2 = Database::attach(db_id);
  ASSERT_EQ(db_id, db2.id());
  ASSERT_LT(static_cast<void*>(0), db2.handle());
  ASSERT_FALSE(db2.is_local());
  ASSERT_TRUE(db2.is_attached());
  ASSERT_FALSE(db2.is_deleted());

  // clean up
  db1.detach();
  db2.detach();
  db1.delete_();
}

TEST_F(DatabaseTest, attach_throws_on_non_existing_db)
{
  ASSERT_THROW(Database::attach(-1), WhitedbAttachException);
}

TEST_F(DatabaseTest, shared_db_indicates_unattached_and_not_deleted_after_detach)
{
  Database db = Database::create_shared(db_id, db_size);
  db.detach();
  ASSERT_FALSE(db.is_attached());
  ASSERT_FALSE(db.is_deleted());

  // clean up
  db.delete_();
}

TEST_F(DatabaseTest, shared_db_indicates_unattached_and_deleted_after_delete)
{
  Database db = Database::create_shared(db_id, db_size);
  db.detach();
  db.delete_();
  ASSERT_FALSE(db.is_attached());
  ASSERT_TRUE(db.is_deleted());
}

TEST_F(DatabaseTest, copy_ctor_copies_properties)
{
  Database db1 = Database::create_shared(db_id, db_size);

  Database db2 = Database(db1);
  ASSERT_NE(&db1, &db2);
  ASSERT_EQ(db1.id(), db2.id());
  ASSERT_EQ(db1.handle(), db2.handle());
  ASSERT_EQ(db1.is_local(), db2.is_local());
  ASSERT_EQ(db1.is_attached(), db2.is_attached());
  ASSERT_EQ(db1.is_deleted(), db2.is_deleted());

  // clean up
  db1.detach();
  db1.delete_();
}

} // namespace whitedb
} // namespace archive_reader
