/*******************************************************************************************************************************
 * src/test/archive_reader/whitedb/field_test.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/field.hpp"

#include <gtest/gtest.h>

#include "archive_reader/whitedb/test_helper.hpp"

namespace archive_reader {
namespace whitedb {

class FieldTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(FieldTest, default_ctor__creates_field_with_initialized_empty_value)
{
  Field<int> tgt_a;
  ASSERT_EQ(0, tgt_a.value());
  Field<std::string> tgt_b;
  ASSERT_EQ("", tgt_b.value());
  Field<bool> tgt_c;
  ASSERT_FALSE(tgt_c.value());
}

TEST_F(FieldTest, ctor__creates_field_with_initialized_value)
{
  Field<int> tgt_a {9801};
  ASSERT_EQ(9801, tgt_a.value());
  Field<std::string> tgt_b {"Noël Baroques a Versailles"};
  ASSERT_EQ("Noël Baroques a Versailles", tgt_b.value());
  Field<bool> tgt_c {true};
  ASSERT_TRUE(tgt_c.value());
}

TEST_F(FieldTest, read__reads_integer_values)
{
  TestHelper th;
  int values[] = {47, 1099, 1398674};
  void* rec = th.create_record(3);
  for (int index = 0; index < 3; ++index) {
    th.write_field(rec, index, values[index]);
  }

  Field<int> tgt;
  tgt.read(th.db_handle(), rec, 0);
  ASSERT_EQ(values[0], tgt.value());
  tgt.read(th.db_handle(), rec, 1);
  ASSERT_EQ(values[1], tgt.value());
  tgt.read(th.db_handle(), rec, 2);
  ASSERT_EQ(values[2], tgt.value());
}

TEST_F(FieldTest, read__reads_string_values)
{
  TestHelper th;
  std::string values[] = {"Dave", "2002 Sparrow Records", ""};
  void* rec = th.create_record(3);
  for (int index = 0; index < 3; ++index) {
    th.write_field(rec, index, values[index]);
  }

  Field<std::string> tgt;
  tgt.read(th.db_handle(), rec, 0);
  ASSERT_EQ(values[0], tgt.value());
  tgt.read(th.db_handle(), rec, 1);
  ASSERT_EQ(values[1], tgt.value());
  tgt.read(th.db_handle(), rec, 2);
  ASSERT_EQ(values[2], tgt.value());
}

TEST_F(FieldTest, read__reads_boolean_values)
{
  TestHelper th;
  char values[] = {'T', '7', 'j', 'F'};
  void* rec = th.create_record(4);
  for (int index = 0; index < 4; ++index) {
    th.write_field(rec, index, values[index]);
  }

  Field<bool> tgt;
  tgt.read(th.db_handle(), rec, 0);
  ASSERT_TRUE(tgt.value());
  tgt.read(th.db_handle(), rec, 1);
  ASSERT_FALSE(tgt.value());
  tgt.read(th.db_handle(), rec, 2);
  ASSERT_FALSE(tgt.value());
  tgt.read(th.db_handle(), rec, 3);
  ASSERT_FALSE(tgt.value());
}

TEST_F(FieldTest, read__reads_record_handle_values)
{
  TestHelper th;
  RecordHandle ref_rh1 {th.create_record(1)};
  RecordHandle ref_rh2 {th.create_record(1)};
  void* rec = th.create_record(2);
  th.write_field(rec, 0, ref_rh1.handle());
  th.write_field(rec, 1, ref_rh2.handle());

  Field<RecordHandle> tgt;
  tgt.read(th.db_handle(), rec, 0);
  ASSERT_EQ(ref_rh1, tgt.value());
  tgt.read(th.db_handle(), rec, 1);
  ASSERT_EQ(ref_rh2, tgt.value());
}

TEST_F(FieldTest, read_value__reads_integer_values)
{
  TestHelper th;
  int values[] = {-104, -983562, -1};
  void* rec = th.create_record(3);
  for (int index = 0; index < 3; ++index) {
    th.write_field(rec, index, values[index]);
  }

  ASSERT_EQ(values[0], Field<int>::read_value(th.db_handle(), rec, 0));
  ASSERT_EQ(values[1], Field<int>::read_value(th.db_handle(), rec, 1));
  ASSERT_EQ(values[2], Field<int>::read_value(th.db_handle(), rec, 2));
}

TEST_F(FieldTest, read_value__reads_string_values)
{
  TestHelper th;
  std::string values[] = {"7 sven SEvEN", "abcd0987", "Qwerty uiop."};
  void* rec = th.create_record(3);
  for (int index = 0; index < 3; ++index) {
    th.write_field(rec, index, values[index]);
  }

  ASSERT_EQ(values[0], Field<std::string>::read_value(th.db_handle(), rec, 0));
  ASSERT_EQ(values[1], Field<std::string>::read_value(th.db_handle(), rec, 1));
  ASSERT_EQ(values[2], Field<std::string>::read_value(th.db_handle(), rec, 2));
}

TEST_F(FieldTest, read_value__reads_boolean_values_from_char)
{
  TestHelper th;
  char values[] = {'T', '7', 'j'};
  void* rec = th.create_record(3);
  for (int index = 0; index < 3; ++index) {
    th.write_field(rec, index, values[index]);
  }

  ASSERT_TRUE(Field<bool>::read_value(th.db_handle(), rec, 0));
  ASSERT_FALSE(Field<bool>::read_value(th.db_handle(), rec, 1));
  ASSERT_FALSE(Field<bool>::read_value(th.db_handle(), rec, 2));
}

TEST_F(FieldTest, read_value__reads_record_handle_values)
{
  TestHelper th;
  RecordHandle ref_rh1 {th.create_record(0)};
  RecordHandle ref_rh2 {th.create_record(0)};
  void* rec = th.create_record(2);
  th.write_field(rec, 0, ref_rh1.handle());
  th.write_field(rec, 1, ref_rh2.handle());

  ASSERT_EQ(ref_rh1, Field<RecordHandle>::read_value(th.db_handle(), rec, 0));
  ASSERT_EQ(ref_rh2, Field<RecordHandle>::read_value(th.db_handle(), rec, 1));
}

TEST_F(FieldTest, write__writes_integer_values)
{
  TestHelper th;
  int values[] = {47, 1099, 1398674, -37894, 0};
  void* rec = th.create_record(5);
  Field<int> tgt;
  for (int index = 0; index < 5; ++index) {
    tgt.set_value(values[index]);
    tgt.write(th.db_handle(), rec, index);
  }

  int data {};
  ASSERT_EQ(values[0], th.read_field(rec, 0, data));
  ASSERT_EQ(values[1], th.read_field(rec, 1, data));
  ASSERT_EQ(values[2], th.read_field(rec, 2, data));
  ASSERT_EQ(values[3], th.read_field(rec, 3, data));
  ASSERT_EQ(values[4], th.read_field(rec, 4, data));
}

TEST_F(FieldTest, write__writes_string_values)
{
  TestHelper th;
  std::string values[] = {"", "1234asdf", "Paris in the spring"};
  void* rec = th.create_record(3);
  Field<std::string> tgt;
  for (int index = 0; index < 3; ++index) {
    tgt.set_value(values[index]);
    tgt.write(th.db_handle(), rec, index);
  }

  std::string data {};
  ASSERT_EQ(values[0], th.read_field(rec, 0, data));
  ASSERT_EQ(values[1], th.read_field(rec, 1, data));
  ASSERT_EQ(values[2], th.read_field(rec, 2, data));
}

TEST_F(FieldTest, write__writes_boolean_values_to_char)
{
  TestHelper th;
  void* rec = th.create_record(2);
  Field<bool> tgt;
  tgt.set_value(true);
  tgt.write(th.db_handle(), rec, 0);
  tgt.set_value(false);
  tgt.write(th.db_handle(), rec, 1);

  char data {};
  ASSERT_EQ('T', th.read_field(rec, 0, data));
  ASSERT_EQ('F', th.read_field(rec, 1, data));
}

TEST_F(FieldTest, write__writes_record_handle_values)
{
  TestHelper th;
  RecordHandle ref_rh1 {th.create_record(1)};
  RecordHandle ref_rh2 {th.create_record(1)};
  void* rec = th.create_record(2);
  Field<RecordHandle> tgt;
  tgt.set_value(ref_rh1);
  tgt.write(th.db_handle(), rec, 0);
  tgt.set_value(ref_rh2);
  tgt.write(th.db_handle(), rec, 1);

  void* data {};
  ASSERT_EQ(ref_rh1.handle(), th.read_field(rec, 0, data));
  ASSERT_EQ(ref_rh2.handle(), th.read_field(rec, 1, data));
}

} // namespace whitedb
} // namespace archive_reader
