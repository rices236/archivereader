/*******************************************************************************************************************************
 * src/test/archive_reader/whitedb/record_handle_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include "archive_reader/whitedb/record_handle.hpp"

namespace archive_reader {
namespace whitedb {

class RecordHandleTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(RecordHandleTest, default_constructor_initializes_zero_handle)
{
  RecordHandle tgt;
  ASSERT_EQ(0, tgt.handle());
}

TEST_F(RecordHandleTest, constructor_initializes_given_handle)
{
  void* handle = reinterpret_cast<void*>(12354);
  RecordHandle tgt(handle);
  ASSERT_EQ(handle, tgt.handle());
}

TEST_F(RecordHandleTest, copy_constructor_initializes_handle)
{
  void* handle = reinterpret_cast<void*>(0xcd3e);
  RecordHandle other {handle};
  RecordHandle tgt(other);
  ASSERT_EQ(handle, tgt.handle());
}

TEST_F(RecordHandleTest, move_constructor_initializes_handle)
{
  void* handle = reinterpret_cast<void*>(0xcd3e);
  RecordHandle other {handle};
  RecordHandle tgt {std::move(other)};
  ASSERT_EQ(handle, tgt.handle());
}

TEST_F(RecordHandleTest, copy_assignment_operator_initializes_handle)
{
  void* handle = reinterpret_cast<void*>(379674);
  RecordHandle other {handle};
  RecordHandle tgt = other;
  ASSERT_EQ(handle, tgt.handle());
}

TEST_F(RecordHandleTest, move_assignment_operator_initializes_handle)
{
  void* handle = reinterpret_cast<void*>(98332);
  RecordHandle other {handle};
  RecordHandle tgt = std::move(other);
  ASSERT_EQ(handle, tgt.handle());
}

TEST_F(RecordHandleTest, bool_operator_returns_correct_value)
{
  RecordHandle tgt1 {};
  ASSERT_FALSE(tgt1);

  RecordHandle tgt2 {0};
  ASSERT_FALSE(tgt2);

  RecordHandle tgt3;
  ASSERT_FALSE(tgt3);

  RecordHandle tgt4 {reinterpret_cast<void*>(0xfffa)};
  ASSERT_TRUE(tgt4);
}

} // namespace whitedb
} // namespace archive_reader
