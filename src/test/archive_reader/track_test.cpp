/*******************************************************************************************************************************
 * src/test/archive_reader/track_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include <nlohmann/json.hpp>

#include "archive_reader/exception.hpp"
#include "archive_reader/track.hpp"

using nlohmann::json;

namespace archive_reader {

class TrackTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

TEST_F(TrackTest, default_ctor_creates_empty_values)
{
  Track tgt;
  EXPECT_TRUE(tgt.title().empty());
  EXPECT_TRUE(tgt.path().empty());
  EXPECT_TRUE(tgt.artists().empty());
  EXPECT_TRUE(tgt.composer().empty());
  EXPECT_TRUE(tgt.genre().empty());
  EXPECT_TRUE(tgt.styles().empty());
  EXPECT_TRUE(tgt.label().empty());
  EXPECT_EQ(0, tgt.original_year());
  EXPECT_EQ(0, tgt.performance_year());
  EXPECT_TRUE(tgt.comments().empty());
}

TEST_F(TrackTest, requires_title_and_path)
{
  // okay if only title and path provided
  json j = R"({"title":"Are Ja Re Hat Natkhat", "path":"./track07.wav"})"_json;
  EXPECT_NO_THROW(j.get<Track>());

  // throws if title missing
  j = R"({"path":"./track07.wav"})"_json;
  EXPECT_THROW(j.get<Track>(), JsonSchemaException);

  // throws if path missing
  j = R"({"title":"Telstar"})"_json;
  EXPECT_THROW(j.get<Track>(), JsonSchemaException);
}

TEST_F(TrackTest, json_to_title_and_path)
{
  json j = R"({"title":"Señor", "path":"./cd03/track13.wav"})"_json;
  Track tgt = j.get<Track>();
  EXPECT_EQ("Señor", tgt.title());
  EXPECT_EQ("./cd03/track13.wav", tgt.path());
}

TEST_F(TrackTest, json_to_artists)
{
  json j = R"({"title":"t", "path":"p", "artists":["Tom Waits"]})"_json;
  Track tgt = j.get<Track>();
  EXPECT_EQ(1, tgt.artists().size());
  EXPECT_EQ("Tom Waits", tgt.artists()[0]);

  j = R"({"title":"t", "path":"p", "artists":["Slacktone","Dave Wronski","Dusty Watson"]})"_json;
  tgt = j.get<Track>();
  EXPECT_EQ(3, tgt.artists().size());
  EXPECT_EQ("Slacktone", tgt.artists()[0]);
  EXPECT_EQ("Dave Wronski", tgt.artists()[1]);
  EXPECT_EQ("Dusty Watson", tgt.artists()[2]);

  j = R"({"title":"t", "path":"p", "artist":"The Ventures"})"_json;
  tgt = j.get<Track>();
  EXPECT_EQ(1, tgt.artists().size());
  EXPECT_EQ("The Ventures", tgt.artists()[0]);
}

TEST_F(TrackTest, throws_if_artists_not_an_array)
{
  json j = R"({"title":"t", "path":"p", "artists":"Danny Partridge"})"_json;
  EXPECT_THROW(j.get<Track>(), JsonSchemaException);
}

TEST_F(TrackTest, throws_if_artist_not_a_string)
{
  json j = R"({"title":"t", "path":"p", "artist":["Laurie Partridge"]})"_json;
  EXPECT_THROW(j.get<Track>(), JsonSchemaException);
}

TEST_F(TrackTest, json_to_composer)
{
  json j = R"({"title":"t", "path":"p", "composer":null})"_json;
  EXPECT_NO_THROW(j.get<Track>());

  j = R"({"title":"t", "path":"p", "composer":"Bach, Johann Sebastian"})"_json;
  Track tgt = j.get<Track>();
  EXPECT_EQ("Bach, Johann Sebastian", tgt.composer());
}

TEST_F(TrackTest, json_to_genre)
{
  json j = R"({"title":"t", "path":"p", "genre":null})"_json;
  EXPECT_NO_THROW(j.get<Track>());

  j = R"({"title":"t", "path":"p", "genre":"Classical"})"_json;
  Track tgt = j.get<Track>();
  EXPECT_EQ("Classical", tgt.genre());
}

TEST_F(TrackTest, json_to_styles)
{
  json j = R"({"title":"t", "path":"p", "styles":["Surf"]})"_json;
  Track tgt = j.get<Track>();
  EXPECT_EQ(1, tgt.styles().size());
  EXPECT_EQ("Surf", tgt.styles()[0]);

  j = R"({"title":"t", "path":"p", "styles":["Instrumental","Surf","Space"]})"_json;
  tgt = j.get<Track>();
  EXPECT_EQ(3, tgt.styles().size());
  EXPECT_EQ("Instrumental", tgt.styles()[0]);
  EXPECT_EQ("Surf", tgt.styles()[1]);
  EXPECT_EQ("Space", tgt.styles()[2]);

  j = R"({"title":"t", "path":"p", "style":"Polyphonic"})"_json;
  tgt = j.get<Track>();
  EXPECT_EQ(1, tgt.styles().size());
  EXPECT_EQ("Polyphonic", tgt.styles()[0]);
}

TEST_F(TrackTest, throws_if_styles_not_an_array)
{
  json j = R"({"title":"t", "path":"p", "styles":"Celtic"})"_json;
  EXPECT_THROW(j.get<Track>(), JsonSchemaException);
}

TEST_F(TrackTest, throws_if_style_not_a_string)
{
  json j = R"({"title":"t", "path":"p", "style":["Liturgical"]})"_json;
  EXPECT_THROW(j.get<Track>(), JsonSchemaException);
}

TEST_F(TrackTest, json_to_label)
{
  json j = R"({"title":"t", "path":"p", "label":null})"_json;
  EXPECT_NO_THROW(j.get<Track>());

  j = R"({"title":"t", "path":"p", "label":"Rhino Records"})"_json;
  Track tgt = j.get<Track>();
  EXPECT_EQ("Rhino Records", tgt.label());
}

TEST_F(TrackTest, json_to_original_year)
{
  json j = R"({"title":"t", "path":"p", "originalYear":null})"_json;
  EXPECT_NO_THROW(j.get<Track>());

  j = R"({"title":"t", "path":"p", "originalYear":1978})"_json;
  Track tgt = j.get<Track>();
  EXPECT_EQ(1978, tgt.original_year());
}

TEST_F(TrackTest, json_to_performance_year)
{
  json j = R"({"title":"t", "path":"p", "performanceYear":null})"_json;
  EXPECT_NO_THROW(j.get<Track>());

  j = R"({"title":"t", "path":"p", "performanceYear":1955})"_json;
  Track tgt = j.get<Track>();
  EXPECT_EQ(1955, tgt.performance_year());
}

TEST_F(TrackTest, json_to_comments)
{
  json j = R"({"title":"t", "path":"p", "comments":["Got errors ripping track from CD"]})"_json;
  Track tgt = j.get<Track>();
  EXPECT_EQ(1, tgt.comments().size());
  EXPECT_EQ("Got errors ripping track from CD", tgt.comments()[0]);

  j = R"({"title":"t", "path":"p", "comments":["Jeff Beck - guitar","Rod Stewart - vocals"]})"_json;
  tgt = j.get<Track>();
  EXPECT_EQ(2, tgt.comments().size());
  EXPECT_EQ("Jeff Beck - guitar", tgt.comments()[0]);
  EXPECT_EQ("Rod Stewart - vocals", tgt.comments()[1]);

  j = R"({"title":"t", "path":"p", "comment":"See Curtis Mayfield & The Impressions - 1965"})"_json;
  tgt = j.get<Track>();
  EXPECT_EQ(1, tgt.comments().size());
  EXPECT_EQ("See Curtis Mayfield & The Impressions - 1965", tgt.comments()[0]);
}

TEST_F(TrackTest, throws_if_comments_not_an_array)
{
  json j = R"({"title":"t", "path":"p", "comments":"Converted from .mp3"})"_json;
  EXPECT_THROW(j.get<Track>(), JsonSchemaException);
}

TEST_F(TrackTest, throws_if_comment_not_a_string)
{
  json j = R"({"title":"t", "path":"p", "style":["From Silsila (1981), lyrics by Javed Akhtar"]})"_json;
  EXPECT_THROW(j.get<Track>(), JsonSchemaException);
}

} // namespace archive_reader
