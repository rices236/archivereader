/*******************************************************************************************************************************
 * src/test/archive_reader/json_helper_test.cpp
 ******************************************************************************************************************************/

#include <gtest/gtest.h>

#include <nlohmann/json.hpp>

#include "archive_reader/exception.hpp"
#include "archive_reader/json_helper.hpp"

using nlohmann::json;

namespace archive_reader {

class JsonHelperTest : public ::testing::Test
{
protected:
  virtual void SetUp() override {}
  virtual void TearDown() override {}
};

const json jrel = R"#!({
  "artists": [
    "Slacktone",
    "Dave Wronski"
  ],
  "catalogNo": "1002",
  "coverArt": "./cover-1002.jpg",
  "genre": "Rock",
  "isCompilation": false,
  "label": "GoBoy Records",
  "source": "CD (RB0347SLACKTONE)",
  "somethingNull":null,
  "somethingEmpty":"",
  "styles": [
    "Instrumental",
    "Surf"
  ],
  "title": "Into the Blue Sparkle",
  "tracks": [
    {
      "path": "./track01.wav",
      "title": "Coffin Closer"
    },
    {
      "path": "./track02.wav",
      "title": "Longboard Legato"
    },
    {
      "path": "./track03.wav",
      "title": "Bird Bone"
    }
  ],
  "year": 2000
})#!"_json;


// Tests for required_str_field()
TEST_F(JsonHelperTest, required_str_field__gets_value)
{
  EXPECT_EQ("Rock", required_str_field(jrel, "genre"));
  EXPECT_EQ("Into the Blue Sparkle", required_str_field(jrel, "title"));
  EXPECT_EQ("GoBoy Records", required_str_field(jrel, "label"));
}

TEST_F(JsonHelperTest, required_str_field__gets_empty_string)
{
  EXPECT_EQ("", required_str_field(jrel, "somethingEmpty"));
}

TEST_F(JsonHelperTest, required_str_field__throws_on_no_field)
{
  EXPECT_THROW(required_str_field(jrel, "arteest"), JsonSchemaException);
}

TEST_F(JsonHelperTest, required_str_field__throws_on_null_value)
{
  EXPECT_THROW(required_str_field(jrel, "somethingNull"), JsonSchemaException);
}

TEST_F(JsonHelperTest, required_str_field__throws_on_value_not_string)
{
  EXPECT_THROW(required_str_field(jrel, "year"), JsonSchemaException);
}


// Tests for optional_plural_str_field()
TEST_F(JsonHelperTest, optional_plural_str_field__gets_by_singular_key)
{
  const str_list expected = {{"Tom Waits"}};
  const json j = R"({"artist":"Tom Waits","title":"Who Are You?","year":1992})"_json;
  EXPECT_EQ(expected, optional_plural_str_field(j, "artist", "artists"));
}

TEST_F(JsonHelperTest, optional_plural_str_field__gets_by_plural_key)
{
  // Test one value in an array
  str_list expected = {{"Tom Waits"}};
  json j = R"({"artists":["Tom Waits"],"title":"Who Are You?","year":1992})"_json;
  EXPECT_EQ(expected, optional_plural_str_field(j, "artist", "artists"));
  // Test multiple values in an array
  expected = {{"Humphrey Bogart","Walter Huston","Tim Holt"}};
  j = R"({"actors":["Humphrey Bogart","Walter Huston","Tim Holt"]})"_json;
  EXPECT_EQ(expected, optional_plural_str_field(j, "actor", "actors"));
}

TEST_F(JsonHelperTest, optional_plural_str_field__gets_empty_string)
{
  const str_list expected = {{""}};
  const json j = R"({"artist":"","title":"Who Are You?","year":1992})"_json;
  EXPECT_EQ(expected, optional_plural_str_field(j, "artist", "artists"));
}

TEST_F(JsonHelperTest, optional_plural_str_field__gets_empty_array)
{
  const json j = R"({"artists":[],"title":"Who Are You?","year":1992})"_json;
  EXPECT_TRUE(optional_plural_str_field(j, "artist", "artists").empty());
}

TEST_F(JsonHelperTest, optional_plural_str_field__throws_if_both_keys_exist)
{
  const json j = R"({"actor":"Bruce Bennett","actors":["Barton MacLane","Alphonso Bedoya"]})"_json;
  EXPECT_THROW(optional_plural_str_field(j, "actor", "actors"), JsonSchemaException);
}

TEST_F(JsonHelperTest, optional_plural_str_field__throws_if_plural_field_not_array)
{
  const json j = R"({"actors":"Barton MacLane"})"_json;
  EXPECT_THROW(optional_plural_str_field(j, "actor", "actors"), JsonSchemaException);
}

TEST_F(JsonHelperTest, optional_plural_str_field__throws_if_singular_field_is_array)
{
  const json j = R"({"actor":["Manuel Dondé","José Torvay"]})"_json;
  EXPECT_THROW(optional_plural_str_field(j, "actor", "actors"), JsonSchemaException);
}

TEST_F(JsonHelperTest, optional_plural_str_field__throws_if_singular_field_not_string)
{
  const json j = R"({"actor":12})"_json;
  EXPECT_THROW(optional_plural_str_field(j, "actor", "actors"), JsonSchemaException);
}

TEST_F(JsonHelperTest, optional_plural_str_field__throws_if_plural_field_not_string_array)
{
  const json j = R"({"actors":[99, 42, 129]})"_json;
  EXPECT_THROW(optional_plural_str_field(j, "actor", "actors"), JsonSchemaException);
}


// Tests for optional_str_field()
TEST_F(JsonHelperTest, optional_str_field__gets_value)
{
  EXPECT_EQ("Rock", optional_str_field(jrel, "genre"));
  EXPECT_EQ("Into the Blue Sparkle", optional_str_field(jrel, "title"));
  EXPECT_EQ("GoBoy Records", optional_str_field(jrel, "label"));
}

TEST_F(JsonHelperTest, optional_str_field__gets_existing_value_not_default)
{
  EXPECT_EQ("Rock", optional_str_field(jrel, "genre", "Roll"));
  EXPECT_EQ("Into the Blue Sparkle", optional_str_field(jrel, "title", "no-title"));
  EXPECT_EQ("GoBoy Records", optional_str_field(jrel, "label", "no-label"));
}

TEST_F(JsonHelperTest, optional_str_field__returns_empty_string_if_empty)
{
  // Unless the field is non-existant or null, return the field value, even if an empty string.
  EXPECT_EQ("", optional_str_field(jrel, "somethingEmpty"));
  EXPECT_EQ("", optional_str_field(jrel, "somethingEmpty", "42"));
}

TEST_F(JsonHelperTest, optional_str_field__returns_default_value_if_null)
{
  // If the field is null, return the default value (which is an empty string if not specified in the function call).
  EXPECT_EQ("", optional_str_field(jrel, "somethingNull"));
  EXPECT_EQ("nothing to see", optional_str_field(jrel, "somethingNull", "nothing to see"));
}

TEST_F(JsonHelperTest, optional_str_field__returns_default_value_if_field_does_not_exist)
{
  // If the field is not there, return the default value (which is an empty string if not specified in the function call).
  EXPECT_EQ("", optional_str_field(jrel, "arteesti"));
  EXPECT_EQ("nothing to see", optional_str_field(jrel, "arteesti", "nothing to see"));
}

TEST_F(JsonHelperTest, optional_str_field__throws_if_value_not_string)
{
  EXPECT_THROW(optional_str_field(jrel, "year"), JsonSchemaException);
  EXPECT_THROW(optional_str_field(jrel, "isCompilation"), JsonSchemaException);
}


// Tests for int optional_uint_field()
TEST_F(JsonHelperTest, optional_uint_field__gets_value)
{
  EXPECT_EQ(2000, optional_uint_field(jrel, "year"));
}

TEST_F(JsonHelperTest, optional_uint_field__gets_existing_value_not_default)
{
  EXPECT_EQ(2000, optional_uint_field(jrel, "year", 1000));
}

TEST_F(JsonHelperTest, optional_uint_field__returns_default_value_if_null)
{
  // If the field is null, return the default value (which is 0 if not specified in the function call).
  const json j = R"({"artists":["Tom Waits"],"title":"Who Are You?","year":null})"_json;
  EXPECT_EQ(0, optional_uint_field(j, "year"));
  EXPECT_EQ(99, optional_uint_field(j, "year", 99));
}

TEST_F(JsonHelperTest, optional_uint_field__returns_default_value_if_field_does_not_exist)
{
  // If the field is not there, return the default value (which 0 if not specified in the function call).
  EXPECT_EQ(0, optional_uint_field(jrel, "yaar"));
  EXPECT_EQ(127, optional_uint_field(jrel, "yaar", 127));
}

TEST_F(JsonHelperTest, optional_uint_field__throws_if_value_not_uint)
{
  EXPECT_THROW(optional_uint_field(jrel, "label"), JsonSchemaException);
  EXPECT_THROW(optional_uint_field(jrel, "isCompilation"), JsonSchemaException);
  // Test with signed int
  const json j = R"({"artists":["Tom Waits"],"title":"Who Are You?","year":-33})"_json;
  EXPECT_THROW(optional_uint_field(j, "year"), JsonSchemaException);
}


// Tests for int optional_bool_field()
TEST_F(JsonHelperTest, optional_bool_field__gets_value)
{
  EXPECT_EQ(false, optional_bool_field(jrel, "isCompilation"));
  const json j = R"({"artist":"The Ventures","title":"Walk Don't Run'","year":1988,"isReissue":true})"_json;
  EXPECT_EQ(true, optional_bool_field(j, "isReissue"));
}

TEST_F(JsonHelperTest, optional_bool_field__gets_existing_value_not_default)
{
  EXPECT_EQ(false, optional_bool_field(jrel, "isCompilation", true));
  const json j = R"({"artist":"T-Bone Burnett","title":"Truth Decay","year":2004,"isReissue":true})"_json;
  EXPECT_EQ(true, optional_bool_field(j, "isReissue", false));
}

TEST_F(JsonHelperTest, optional_bool_field__returns_default_value_if_null)
{
  // If the field is null, return the default value (which is false if not specified in the function call).
  const json j = R"({"artists":["Tom Waits"],"title":"Bone Machine","year":1996,"isReissue":null})"_json;
  EXPECT_EQ(false, optional_bool_field(j, "isReissue"));
  EXPECT_EQ(true, optional_bool_field(j, "isReissue", true));
}

TEST_F(JsonHelperTest, optional_bool_field__returns_default_value_if_field_does_not_exist)
{
  // If the field is not there, return the default value (which false if not specified in the function call).
  const json j = R"({"artists":["Tom Waits"],"title":"Bone Machine","year":1996})"_json;
  EXPECT_EQ(false, optional_bool_field(j, "isReissue"));
  EXPECT_EQ(true, optional_bool_field(j, "isReissue", true));
}

TEST_F(JsonHelperTest, optional_bool_field__throws_if_value_not_boolean)
{
  EXPECT_THROW(optional_bool_field(jrel, "artists"), JsonSchemaException);
  EXPECT_THROW(optional_bool_field(jrel, "label"), JsonSchemaException);
  EXPECT_THROW(optional_bool_field(jrel, "year"), JsonSchemaException);
}

} // namespace archive_reader
