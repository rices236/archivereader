/*******************************************************************************************************************************
 * src/source/main.cpp
 ******************************************************************************************************************************/

#include <iostream>
#include <thread>

#include "archive_reader/config.hpp"
#include "archive_reader/constant/constant.hpp"
#include "archive_reader/constant/version.hpp"
#include "archive_reader/exception.hpp"
#include "archive_reader/interrupt/interrupt.hpp"
#include "archive_reader/library_scanner.hpp"
#include "archive_reader/logger/logger.hpp"
#include "archive_reader/service/service.hpp"

using namespace archive_reader;

int main(int argc, char** argv)
try {
  Config config(argc, argv);

  logger::init();

  logger::get()->info("Starting the {} service v{}, build timestamp: {}, commit: {}.", constant::ServiceName,
                      constant::ServiceSemanticVersion, constant::ServiceBuildTimestamp, constant::ServiceCommitHash);

  interrupt::hook_signals();

  // Initialize the service and then run wait() in a separate thread, which will allow it to respond to health checks.
  service::Service svc(config);
  svc.init();
  svc.set_serving_status(false);
  std::thread svc_t(&service::Service::wait, std::ref(svc));

  // With the service running, scan the library.
  LibraryScanner ls(config);
  ls.scan();

  // Now that the library has been scanned, set the service status so that it will respond to health checks that it is ready to
  // serve.
  svc.set_serving_status(true);

  // Pause the main thread while waiting for interrupt signals; the service will continue serving on its own thread.
  interrupt::wait_for_signal();

  // Post-interrupt, signal the service to shut down.
  svc.shutdown();

  // Allow the service to shut down before terminating the program.
  svc_t.join();

  return 0;
}
catch (const ArchiveReaderException& e) {
  std::cout << "ArchiveReader exception: " << e.what() << std::endl;
}
catch (const std::exception& e) {
  std::cout << "Standard library exception: " << e.what() << std::endl;
}
catch (...) {
  std::cout << "Unknown exception caught." << std::endl;
}
