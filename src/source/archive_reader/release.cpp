/*******************************************************************************************************************************
 * src/source/archive_reader/release.cpp
 ******************************************************************************************************************************/

#include "archive_reader/release.hpp"

#include <iostream>

#include "archive_reader/exception.hpp"
#include "archive_reader/json_helper.hpp"

namespace bfs = boost::filesystem;

using nlohmann::json;

namespace archive_reader {

const char* const title = "title";
const char* const artist = "artist";
const char* const artists = "artists";
const char* const composer = "composer";
const char* const performer = "performer";
const char* const performers = "performers";
const char* const catalogNo = "catalogNo";
const char* const coverArt = "coverArt";
const char* const genre = "genre";
const char* const style = "style";
const char* const styles = "styles";
const char* const isCompilation = "isCompilation";
const char* const isReissue = "isReissue";
const char* const label = "label";
const char* const source = "source";
const char* const year = "year";
const char* const originalYear = "originalYear";
const char* const performanceYear = "performanceYear";
const char* const comment = "comment";
const char* const comments = "comments";
const char* const tracks = "tracks";

Release::Release()
  : _title {},
    _artists {},
    _composer {},
    _performers {},
    _catalog_no {},
    _cover_art {},
    _genre {},
    _styles {},
    _is_compilation {},
    _is_reissue {},
    _label {},
    _source {},
    _year {},
    _original_year {},
    _performance_year {},
    _comments {},
    _tracks {},
    _release_file_path {},
    _cover_art_path {}
{
}

std::string Release::title() const noexcept
{
  return _title;
}

str_list Release::artists() const noexcept
{
  return _artists;
}

std::string Release::composer() const noexcept
{
  return _composer;
}

str_list Release::performers() const noexcept
{
  return _performers;
}

std::string Release::catalog_no() const noexcept
{
  return _catalog_no;
}

std::string Release::cover_art() const noexcept
{
  return _cover_art;
}

std::string Release::genre() const noexcept
{
  return _genre;
}

str_list Release::styles() const noexcept
{
  return _styles;
}

bool Release::is_compilation() const noexcept
{
  return _is_compilation;
}

bool Release::is_reissue() const noexcept
{
  return _is_reissue;
}

std::string Release::label() const noexcept
{
  return _label;
}

std::string Release::source() const noexcept
{
  return _source;
}

int Release::year() const noexcept
{
  return _year;
}

int Release::original_year() const noexcept
{
  return _original_year;
}

int Release::performance_year() const noexcept
{
  return _performance_year;
}

str_list Release::comments() const noexcept
{
  return _comments;
}

track_list Release::tracks() const noexcept
{
  return _tracks;
}

std::string Release::release_file_path() const noexcept
{
  return _release_file_path;
}

std::string Release::cover_art_path() const noexcept
{
  return _cover_art_path;
}

void from_json(const json& j, Release& r)
{
  r._title = required_str_field(j, title);
  r._artists = optional_plural_str_field(j, artist, artists);
  r._composer = optional_str_field(j, composer);
  r._performers = optional_plural_str_field(j, performer, performers);
  r._catalog_no = optional_str_field(j, catalogNo);
  r._cover_art = optional_str_field(j, coverArt);
  r._genre = optional_str_field(j, genre);
  r._styles = optional_plural_str_field(j, style, styles);
  r._is_compilation = optional_bool_field(j, isCompilation, false);
  r._is_reissue = optional_bool_field(j, isReissue, false);
  r._label = optional_str_field(j, label);
  r._source = optional_str_field(j, source);
  r._year = optional_uint_field(j, year, 0);
  r._original_year = optional_uint_field(j, originalYear, 0);
  r._performance_year = optional_uint_field(j, performanceYear, 0);
  r._comments = optional_plural_str_field(j, comment, comments);

  if (j.contains(tracks) && !j[tracks].is_null()) {
    if (j.at(tracks).is_array()) {
      j.at(tracks).get_to(r._tracks);
    }
    else {
      throw JsonSchemaException("Field \"" + std::string(tracks) + "\" must be an array of tracks.");
    }
  }
}

void from_path(const bfs::path& path, Release& r)
{
  // Full path of the release file
  r._release_file_path = path.c_str();
  // Full path of the cover art
  bfs::path ca_path = path.parent_path() / bfs::path(r._cover_art);
  r._cover_art_path = ca_path.lexically_normal().c_str();
  // Set the track information
  for (auto& tr : r.tracks()) {
    from_path(path, tr);
  }
}

std::ostream& operator<<(std::ostream& os, const Release& r)
{
  os << "Release: { " << r._title;
  if (r._artists.size() > 0) {
    os << ", " << r._artists[0];
  }
  else {
    os << ", (no artists)";
  }
  os << ", " << r._tracks.size() << " tracks }";

  return os;
}

} // namespace archive_reader
