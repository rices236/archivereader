/*******************************************************************************************************************************
 * src/source/archive_reader/file_operations.cpp
 ******************************************************************************************************************************/

#include "archive_reader/file_operations.hpp"

#include <fstream>

#include <nlohmann/json.hpp>

#include "archive_reader/exception.hpp"

using nlohmann::json;

namespace archive_reader {

json open_json(const std::string& file_path)
{
  std::ifstream ifs(file_path, std::ios_base::in);
  if (!ifs.is_open()) {
    throw FileOpenException("Failed to open file: " + file_path);
  }

  json j;
  try {
    ifs >> j;
  }
  catch (const json::parse_error& e) {
    throw JsonParseException("JSON parse error: ", e);
  }

  return j;
}

} // namespace archive_reader
