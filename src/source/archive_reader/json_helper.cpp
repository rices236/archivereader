/*******************************************************************************************************************************
 * src/source/archive_reader/json_helper.cpp
 ******************************************************************************************************************************/

#include "archive_reader/json_helper.hpp"

#include "archive_reader/exception.hpp"

using nlohmann::json;

namespace archive_reader {

std::string required_str_field(const json& j, const std::string& key)
{
  if (!j.contains(key) || j[key].is_null()) {
    throw JsonSchemaException("Field \"" + key + "\" is required.");
  }

  if (!j[key].is_string()) {
    throw JsonSchemaException("Field \"" + key + "\" must contain a string value.");
  }

  return j.at(key);
}

// These are only here as supporting functions for optional_plural_str_field() so they are not declared in the header file.
str_list optional_str_array_from_field(const json&, const std::string&);
std::string optional_str_from_field(const json&, const std::string&);

str_list optional_plural_str_field(const json& j, const std::string& skey, const std::string& pkey)
{
  if (j.contains(skey) && j.contains(pkey)) {
    throw JsonSchemaException("Either field \"" + skey + "\" (string), or \"" + pkey +
                              "\" (string array) is acceptable, but not both.");
  }

  str_list ret_value {};
  if (j.contains(pkey)) {
    ret_value = optional_str_array_from_field(j.at(pkey), pkey);
  }
  else if (j.contains(skey)) {
    ret_value.push_back(optional_str_from_field(j.at(skey), skey));
  }

  return ret_value;
}

std::string optional_str_field(const json& j, const std::string& key, const std::string& default_value)
{
  if (j.contains(key) && !j[key].is_null()) {
    if (j[key].is_string()) {
      return j.at(key);
    }
    else {
      throw JsonSchemaException("Field \"" + key + "\" must be a string.");
    }
  }

  return default_value;
}

int optional_uint_field(const json& j, const std::string& key, const int default_value)
{
  if (j.contains(key) && !j[key].is_null()) {
    if (j[key].is_number_unsigned()) {
      return j.at(key);
    }
    else {
      throw JsonSchemaException("Field \"" + key + "\" must be an unsigned integer.");
    }
  }

  return default_value;
}

bool optional_bool_field(const json& j, const std::string& key, const bool default_value)
{
  if (j.contains(key) && !j[key].is_null()) {
    if (j[key].is_boolean()) {
      return j.at(key);
    }
    else {
      throw JsonSchemaException("Field \"" + key + "\" must be a Boolean value.");
    }
  }

  return default_value;
}

str_list optional_str_array_from_field(const json& j, const std::string& key)
{
  if (!j.is_array() || (!j.empty() && !j[0].is_string())) {
    throw JsonSchemaException("Field \"" + key + "\" must be a string array.");
  }

  return j;
}

std::string optional_str_from_field(const json& j, const std::string& key)
{
  if (!j.is_null()) {
    if (!j.is_string()) {
      throw JsonSchemaException("Field \"" + key + "\" must be a string.");
    }

    return j;
  }

  return {};
}

} // namespace archive_reader
