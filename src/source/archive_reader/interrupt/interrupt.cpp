/*******************************************************************************************************************************
 * src/source/archive_reader/interrupt/interrupt.cpp
 ******************************************************************************************************************************/

#include <condition_variable>
#include <mutex>
#include <signal.h>

// TODO: Digest and respond to the following (From GNU standards, 4.2 Writing Robust Programs):
// The preferred signal handling facilities are the BSD variant of signal, and the POSIX sigaction function; the alternative
// USG signal interface is an inferior design.
// Nowadays, using the POSIX signal functions may be the easiest way to make a program portable. If you use signal, then on
// GNU/Linux systems running GNU libc version 1, you should include bsd/signal.h instead of signal.h, so as to get BSD
// behavior. It is up to you whether to support systems where signal has only the USG behavior, or give up on them.

#include "archive_reader/interrupt/interrupt.hpp"
#include "archive_reader/logger/logger.hpp"

namespace archive_reader {
namespace interrupt {

std::condition_variable _condition;
std::mutex _mutex;

void handle_signal(const int);

void hook_signals()
{
  signal(SIGHUP, handle_signal);
  signal(SIGINT, handle_signal);
  signal(SIGTERM, handle_signal);
}

void handle_signal(const int signal)
{
  switch (signal) {
  case SIGHUP:
    logger::get()->info("SIGHUP trapped ...");
    break;
  case SIGINT:
    logger::get()->info("SIGINT trapped ...");
    _condition.notify_one();
    break;
  case SIGTERM:
    logger::get()->info("SIGTERM trapped ...");
    _condition.notify_one();
    break;
  }
}

void wait_for_signal()
{
  std::unique_lock<std::mutex> lock {_mutex};
  _condition.wait(lock);
  logger::get()->info("Received signal to interrupt program ...");
  lock.unlock();
}

} // namespace interrupt
} // namespace archive_reader
