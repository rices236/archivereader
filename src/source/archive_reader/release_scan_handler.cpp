/*******************************************************************************************************************************
 * src/source/archive_reader/release_scan_handler.cpp
 ******************************************************************************************************************************/

#include "archive_reader/release_scan_handler.hpp"

#include <nlohmann/json.hpp>

#include "archive_reader/db_writer.hpp"
#include "archive_reader/exception.hpp"
#include "archive_reader/file_operations.hpp"
#include "archive_reader/release.hpp"
#include "archive_reader/logger/logger.hpp"
#include "archive_reader/whitedb/exception.hpp"

namespace bfs = boost::filesystem;

using nlohmann::json;

namespace archive_reader {

ReleaseScanHandler::ReleaseScanHandler(const std::string& library_root, const std::string& release_file, DBWriter* dbwriter)
  : _library_root {library_root},
    _release_file {release_file},
    _dbwriter {dbwriter},
    _scan_count {0},
    _err_count {0}
{
}

bool ReleaseScanHandler::operator()(const bfs::directory_entry& dir_entry)
{
  if (bfs::is_regular_file(dir_entry) && dir_entry.path().filename() == _release_file) {
    ++_scan_count;
    logger::get()->info("Found: {}", dir_entry.path().c_str());
    Release rel;
    try {
      json j = open_json(dir_entry.path().c_str());
      rel = j.get<Release>();
      from_path(dir_entry.path().c_str(), rel);
      _dbwriter->write(rel);
    }
    catch (const whitedb::WhitedbException& e) {
      ++_err_count;
      std::string msg {"WhitedbException while scanning " + std::string(dir_entry.path().c_str())};
      logger::get()->error("{}: {}", msg, e.what());
    }
    catch (const ArchiveReaderException& e) {
      ++_err_count;
      std::string msg {"ArchiveReaderException while scanning " + std::string(dir_entry.path().c_str())};
      logger::get()->error("{}: {}", msg, e.what());
    }
    catch (const std::exception& e) {
      ++_err_count;
      std::string msg {"Exception while scanning " + std::string(dir_entry.path().c_str())};
      logger::get()->error("{}: {}", msg, e.what());
    }

    if (logger::is_at_least(logger::LogLevel::Info)) {
      if (rel.artists().size() > 0) {
        logger::get()->info("Release: {} - {} ({} tracks, {}, {})", rel.artists()[0], rel.title(), rel.tracks().size(),
                            rel.label(), rel.year());
      }
      else {
        logger::get()->info("Release: {} ({} tracks, {}, {})", rel.title(), rel.tracks().size(), rel.label(), rel.year());
      }
    }
  }

  return true;
}

int ReleaseScanHandler::scan_count() const noexcept
{
  return _scan_count;
}

int ReleaseScanHandler::err_count() const noexcept
{
  return _err_count;
}

void ReleaseScanHandler::reset() noexcept
{
  _scan_count = 0;
  _err_count = 0;
}

} // namespace archive_reader
