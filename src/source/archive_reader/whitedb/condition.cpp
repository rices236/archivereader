/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/condition.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/condition.hpp"

namespace archive_reader {
namespace whitedb {

wg_int to_wgdb_condition(const Condition cond)
{
  return static_cast<wg_int>(cond);
}

std::string condition_to_name(const Condition cond) noexcept
{
  switch (cond) {
  case Condition::Equal:
    return "Equal";
  case Condition::NotEqual:
    return "NotEqual";
  case Condition::LessThan:
    return "LessThan";
  case Condition::GreaterThan:
    return "GreaterThan";
  case Condition::EqualOrLess:
    return "EqualOrLess";
  case Condition::EqualOrGreater:
    return "EqualOrGreater";
  default:
    break;
  }

  return "(Unknown Condition)";
}

std::ostream& operator<<(std::ostream& os, const Condition& cond)
{
  os << condition_to_name(cond);

  return os;
}

} // namespace whitedb
} // namespace archive_reader
