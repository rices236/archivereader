/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/record_handle.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/record_handle.hpp"

#include <utility>

namespace archive_reader {
namespace whitedb {

RecordHandle::RecordHandle()
  : _handle {nullptr}
{
}

RecordHandle::RecordHandle(void* handle)
  : _handle {handle}
{
}

RecordHandle::RecordHandle(const RecordHandle& other)
  : _handle {other._handle}
{
}

RecordHandle::RecordHandle(RecordHandle&& other)
  : _handle {std::move(other._handle)}
{
}

RecordHandle& RecordHandle::operator=(const RecordHandle& other)
{
  _handle = other._handle;

  return *this;
}

RecordHandle& RecordHandle::operator=(RecordHandle&& other)
{
  _handle = std::move(other._handle);

  return *this;
}

void* RecordHandle::handle() const noexcept
{
  return _handle;
}

RecordHandle::operator bool() const noexcept
{
  return _handle != 0;
}

bool operator==(const RecordHandle& lhs, const RecordHandle& rhs) noexcept
{
  return lhs._handle == rhs._handle;
}

bool operator!=(const RecordHandle& lhs, const RecordHandle& rhs) noexcept
{
  return lhs._handle != rhs._handle;
}

} // namespace whitedb
} // namespace archive_reader
