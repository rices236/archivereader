/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/table_id.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/table_id.hpp"

namespace archive_reader {
namespace whitedb {

std::string table_id_to_name(const TableID tid) noexcept
{
  switch (tid) {
    case TableID::Release: return "Release";
    case TableID::Track: return "Track";
    case TableID::Composer: return "Composer";
    case TableID::Genre: return "Genre";
    case TableID::Label: return "Label";
    case TableID::Artist: return "Artist";
    case TableID::ReleaseArtist: return "ReleaseArtist";
    case TableID::TrackArtist: return "TrackArtist";
    case TableID::Performer: return "Performer";
    case TableID::ReleasePerformer: return "ReleasePerformer";
    case TableID::TrackPerformer: return "TrackPerformer";
    case TableID::Style: return "Style";
    case TableID::ReleaseStyle: return "ReleaseStyle";
    case TableID::TrackStyle: return "TrackStyle";
    case TableID::ReleaseComment: return "ReleaseComment";
    case TableID::TrackComment: return "TrackComment";
    default: break;
  }

  return "(Unknown TableID";
}

std::ostream& operator<<(std::ostream& os, const TableID& tid)
{
  os << table_id_to_name(tid);

  return os;
}

} // namespace whitedb
} // namespace archive_reader
