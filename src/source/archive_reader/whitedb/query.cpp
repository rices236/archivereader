/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/query.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/query.hpp"

#include "archive_reader/logger/logger.hpp"
#include "archive_reader/whitedb/exception.hpp"

namespace archive_reader {
namespace whitedb {

Query::Query(const void* dbh, const QueryParameterList& qpl)
  : _dbh {dbh},
    _parameter_list {qpl._params},
    _query {nullptr},
    _resource_list {}
{
}

Query::~Query()
{
  free();
}

RecordHandle Query::fetch()
{
  void* rec {nullptr};
  if (_query) {
    rec = wg_fetch(const_cast<void*>(_dbh), _query);
    if (!rec) {
      // If there are no more records then free the resources.
      free();
    }
  }

  return {rec};
}

void Query::free() noexcept
{
  if (_query) {
    // Free the query resource
    wg_free_query(const_cast<void*>(_dbh), _query);
    _query = nullptr;
  }

  if (_resource_list.size() > 0) {
    // Free each of the encoded resources
    for (wg_int res : _resource_list) {
      wg_free_query_param(const_cast<void*>(_dbh), res);
    }
    _resource_list.clear();
  }
}

void Query::make()
{
  if (_query != nullptr) {
    std::string msg {"Attempting 'make()' when a query resource already exists."};
    logger::get()->error(msg);
    throw WhitedbQueryException(msg);
  }

  if (_resource_list.size() != 0) {
    std::string msg {"Attempting 'make()' on query when parameter resources have already been allocated."};
    logger::get()->error(msg);
    throw WhitedbQueryException(msg);
  }

  wg_query* qry {nullptr};
  if (_parameter_list.empty()) {
    logger::get()->debug("Creating a query using an empty parameter list.");
    qry = wg_make_query(const_cast<void*>(_dbh), NULL, 0, NULL, 0);
  }
  else {
    logger::get()->debug("Creating a query using a parameter list of size {}.", _parameter_list.size());
    // Encode the query parameters and save the resource as we will need to free it later
    for (auto tup : _parameter_list) {
      wg_int res = std::get<2>(tup)->encode_query_param(const_cast<void*>(_dbh));
      _resource_list.push_back(res);
    }

    wg_query_arg arglist[_parameter_list.size()];
    for (int index = 0; index < static_cast<int>(_parameter_list.size()); ++index) {
      arglist[index].column = static_cast<wg_int>(std::get<0>(_parameter_list[index]));
      arglist[index].cond = to_wgdb_condition(std::get<1>(_parameter_list[index]));
      // Refer to the previously encoded resource
      arglist[index].value = _resource_list[index];
    }
    qry = wg_make_query(const_cast<void*>(_dbh), NULL, 0, arglist, static_cast<wg_int>(_parameter_list.size()));
  }

  if (!qry) {
    // Call free() to de-allocate any arglist resources
    free();
    std::string msg {"A query object was not created."};
    logger::get()->error(msg);
    throw WhitedbQueryException(msg);
  }

  _query = qry;
}

} // namespace whitedb
} // namespace archive_reader
