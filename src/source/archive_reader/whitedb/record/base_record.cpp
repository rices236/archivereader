/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/record/base_record.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/record/base_record.hpp"

namespace archive_reader {
namespace whitedb {
namespace record {

BaseRecord::BaseRecord(const TableID tid)
  : _f_table_id {tid},
    _f_urid {}
{
}

rec_id BaseRecord::next_urid() noexcept
{
  return _next_urid++;
}

void get_fields(std::vector<BaseField*>& fld_list, BaseRecord& br)
{
  // Add the table ID and unique record ID fields, which are common to all subclasses
  fld_list.push_back((BaseField*)&(br._f_table_id));
  br._f_urid = BaseRecord::next_urid();
  fld_list.push_back((BaseField*)&(br._f_urid));
  // Call the virtual function to get the rest of the fields
  br.get_fields(fld_list);
}

} // namespace record
} // namespace whitedb
} // namespace archive_reader
