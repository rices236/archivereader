/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/record/release.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/record/release.hpp"

namespace archive_reader {
namespace whitedb {
namespace record {

Release::Release()
  : BaseRecord {TableID::Release},
    _f_title {},
    _f_catalog_no {},
    _f_is_compilation {},
    _f_is_reissue {},
    _f_source {},
    _f_year {},
    _f_original_year {},
    _f_performance_year {},
    _f_release_file_path {},
    _f_cover_art_path {},
    _f_composer_rec {},
    _f_genre_rec {},
    _f_label_rec {}
{
}

Release::Release(const archive_reader::Release& ar_rel)
  : BaseRecord {TableID::Release},
    _f_title {ar_rel.title()},
    _f_catalog_no {ar_rel.catalog_no()},
    _f_is_compilation {ar_rel.is_compilation()},
    _f_is_reissue {ar_rel.is_reissue()},
    _f_source {ar_rel.source()},
    _f_year {ar_rel.year()},
    _f_original_year {ar_rel.original_year()},
    _f_performance_year {ar_rel.performance_year()},
    _f_release_file_path {ar_rel.release_file_path()},
    _f_cover_art_path {ar_rel.cover_art_path()},
    _f_composer_rec {},
    _f_genre_rec {},
    _f_label_rec {}
{
}

std::string Release::title() const noexcept
{
  return _f_title.value();
}

std::string Release::catalog_no() const noexcept
{
  return _f_catalog_no.value();
}

bool Release::is_compilation() const noexcept
{
  return _f_is_compilation.value();
}

bool Release::is_reissue() const noexcept
{
  return _f_is_reissue.value();
}

std::string Release::source() const noexcept
{
  return _f_source.value();
}

int Release::year() const noexcept
{
  return _f_year.value();
}

int Release::original_year() const noexcept
{
  return _f_original_year.value();
}

int Release::performance_year() const noexcept
{
  return _f_performance_year.value();
}

std::string Release::release_file_path() const noexcept
{
  return _f_release_file_path.value();
}

std::string Release::cover_art_path() const noexcept
{
  return _f_cover_art_path.value();
}

RecordHandle Release::composer() const noexcept
{
  return _f_composer_rec.value();
}

RecordHandle Release::genre() const noexcept
{
  return _f_genre_rec.value();
}

RecordHandle Release::label() const noexcept
{
  return _f_label_rec.value();
}

void Release::set_title(const std::string& title)
{
  _f_title.set_value(title);
}

void Release::set_catalog_no(const std::string& catalog_no)
{
  _f_catalog_no.set_value(catalog_no);
}

void Release::set_is_compilation(const bool is_compilation)
{
  _f_is_compilation.set_value(is_compilation);
}

void Release::set_is_reissue(const bool is_reissue)
{
  _f_is_reissue.set_value(is_reissue);
}

void Release::set_source(const std::string& source)
{
  _f_source.set_value(source);
}

void Release::set_year(const int year)
{
  _f_year.set_value(year);
}

void Release::set_original_year(const int original_year)
{
  _f_original_year.set_value(original_year);
}

void Release::set_performance_year(const int performance_year)
{
  _f_performance_year.set_value(performance_year);
}

void Release::set_release_file_path(const std::string& release_file_path)
{
  _f_release_file_path.set_value(release_file_path);
}

void Release::set_cover_art_path(const std::string& cover_art_path)
{
  _f_cover_art_path.set_value(cover_art_path);
}

void Release::set_composer(const RecordHandle& composer)
{
  _f_composer_rec.set_value(composer);
}

void Release::set_genre(const RecordHandle& genre)
{
  _f_genre_rec.set_value(genre);
}

void Release::set_label(const RecordHandle& label)
{
  _f_label_rec.set_value(label);
}

void Release::get_fields(std::vector<BaseField*>& fld_list)
{
  fld_list.push_back(static_cast<BaseField*>(&_f_title));
  fld_list.push_back(static_cast<BaseField*>(&_f_catalog_no));
  fld_list.push_back(static_cast<BaseField*>(&_f_is_compilation));
  fld_list.push_back(static_cast<BaseField*>(&_f_is_reissue));
  fld_list.push_back(static_cast<BaseField*>(&_f_source));
  fld_list.push_back(static_cast<BaseField*>(&_f_year));
  fld_list.push_back(static_cast<BaseField*>(&_f_original_year));
  fld_list.push_back(static_cast<BaseField*>(&_f_performance_year));
  fld_list.push_back(static_cast<BaseField*>(&_f_release_file_path));
  fld_list.push_back(static_cast<BaseField*>(&_f_cover_art_path));
  fld_list.push_back(static_cast<BaseField*>(&_f_composer_rec));
  fld_list.push_back(static_cast<BaseField*>(&_f_genre_rec));
  fld_list.push_back(static_cast<BaseField*>(&_f_label_rec));
}

} // namespace record
} // namespace whitedb
} // namespace archive_reader
