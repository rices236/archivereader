/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/record/track.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/record/track.hpp"

namespace archive_reader {
namespace whitedb {
namespace record {

Track::Track()
  : BaseRecord {TableID::Track},
    _f_release_rec {},
    _f_order {},
    _f_title {},
    _f_original_year {},
    _f_performance_year {},
    _f_composer_rec {},
    _f_genre_rec {},
    _f_label_rec {},
    _f_file_path {}
{
}

Track::Track(const archive_reader::Track& ar_track)
  : BaseRecord {TableID::Track},
    _f_release_rec {},
    _f_order {},
    _f_title {ar_track.title()},
    _f_original_year {ar_track.original_year()},
    _f_performance_year {ar_track.performance_year()},
    _f_composer_rec {},
    _f_genre_rec {},
    _f_label_rec {},
    _f_file_path {ar_track.track_file_path()}
{
}

RecordHandle Track::release() const noexcept
{
  return _f_release_rec.value();
}

int Track::order() const noexcept
{
  return _f_order.value();
}

std::string Track::title() const noexcept
{
  return _f_title.value();
}

int Track::original_year() const noexcept
{
  return _f_original_year.value();
}

int Track::performance_year() const noexcept
{
  return _f_performance_year.value();
}

RecordHandle Track::composer() const noexcept
{
  return _f_composer_rec.value();
}

RecordHandle Track::genre() const noexcept
{
  return _f_genre_rec.value();
}

RecordHandle Track::label() const noexcept
{
  return _f_label_rec.value();
}

std::string Track::file_path() const noexcept
{
  return _f_file_path.value();
}

void Track::set_release(const RecordHandle& release)
{
  _f_release_rec.set_value(release);
}

void Track::set_order(const int order)
{
  _f_order.set_value(order);
}

void Track::set_title(const std::string& title)
{
  _f_title.set_value(title);
}

void Track::set_original_year(const int original_year)
{
  _f_original_year.set_value(original_year);
}

void Track::set_performance_year(const int performance_year)
{
  _f_performance_year.set_value(performance_year);
}

void Track::set_composer(const RecordHandle& composer)
{
  _f_composer_rec.set_value(composer);
}

void Track::set_genre(const RecordHandle& genre)
{
  _f_genre_rec.set_value(genre);
}

void Track::set_label(const RecordHandle& label)
{
  _f_label_rec.set_value(label);
}

void Track::set_file_path(const std::string& file_path)
{
  _f_file_path.set_value(file_path);
}

void Track::get_fields(std::vector<BaseField*>& fld_list)
{
  fld_list.push_back(static_cast<BaseField*>(&_f_release_rec));
  fld_list.push_back(static_cast<BaseField*>(&_f_order));
  fld_list.push_back(static_cast<BaseField*>(&_f_title));
  fld_list.push_back(static_cast<BaseField*>(&_f_original_year));
  fld_list.push_back(static_cast<BaseField*>(&_f_performance_year));
  fld_list.push_back(static_cast<BaseField*>(&_f_composer_rec));
  fld_list.push_back(static_cast<BaseField*>(&_f_genre_rec));
  fld_list.push_back(static_cast<BaseField*>(&_f_label_rec));
  fld_list.push_back(static_cast<BaseField*>(&_f_file_path));
}

} // namespace record
} // namespace whitedb
} // namespace archive_reader
