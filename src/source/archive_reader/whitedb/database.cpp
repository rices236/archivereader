/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/database.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/database.hpp"

#include <string>

#include <whitedb/dbapi.h>

#include "archive_reader/logger/logger.hpp"
#include "archive_reader/whitedb/exception.hpp"

namespace archive_reader {
namespace whitedb {

Database::Database(const int id, void* handle, const bool is_local, const bool is_attached, const bool is_deleted)
  : _id {id},
    _handle {handle},
    _is_local {is_local},
    _is_attached {is_attached},
    _is_deleted {is_deleted}
{
}

Database::Database(const Database& other)
  : _id {other._id},
    _handle {other._handle},
    _is_local {other._is_local},
    _is_attached {other._is_attached},
    _is_deleted {other._is_deleted}
{
}

Database& Database::operator=(const Database& other)
{
  _id = other._id;
  _handle = other._handle;
  _is_local = other._is_local;
  _is_attached = other._is_attached;
  _is_deleted = other._is_deleted;

  return *this;
}

int Database::id() const noexcept
{
  return _id;
}

void* Database::handle() const noexcept
{
  return _handle;
}

bool Database::is_local() const noexcept
{
  return _is_local;
}

bool Database::is_attached() const noexcept
{
  return _is_attached;
}

bool Database::is_deleted() const noexcept
{
  return _is_deleted;
}

long Database::size() const noexcept
{
  return static_cast<long>(wg_database_size(_handle));
}

long Database::free_size() const noexcept
{
  return static_cast<long>(wg_database_freesize(_handle));
}

void Database::detach()
{
  if (_is_local) {
    logger::get()->warn("Attempting to detach a local database, which can't be detached.");
  }
  else {
    wg_int result = wg_detach_database(_handle);
    if (result) {
      std::string msg {"Failed to detach database."};
      logger::get()->error(msg);
      throw WhitedbDetachException(msg);
    }
    _is_attached = false;
  }
}

void Database::delete_()
{
  if (is_local()) {
    wg_delete_local_database(_handle);
  }
  else {
    wg_int result = wg_delete_database(const_cast<char*>(std::to_string(_id).c_str()));
    if (result) {
      std::string msg {"Failed to delete database."};
      logger::get()->error(msg);
      throw WhitedbDeleteException(msg);
    }
  }
  _id = 0;
  _handle = 0;
  _is_attached = false;
  _is_deleted = true;
}

void Database::read(const RecordHandle& rh, record::BaseRecord& br)
{
  std::vector<BaseField*> v_flds {};
  get_fields(v_flds, br);

  void* rec_h = rh.handle();
  for (int index = 0; index < static_cast<int>(v_flds.size()); ++index) {
    v_flds[index]->read(_handle, rec_h, index);
  }
}

RecordHandle Database::write(record::BaseRecord& br)
{
  std::vector<BaseField*> v_flds {};
  get_fields(v_flds, br);

  void* rec = wg_create_record(_handle, static_cast<wg_int>(v_flds.size()));
  if (!rec) {
    std::string msg {"Failed to create a record."};
    logger::get()->error(msg);
    throw WhitedbWriteException(msg);
  }

  try {
    for (int index = 0; index < static_cast<int>(v_flds.size()); ++index) {
      v_flds[index]->write(_handle, rec, index);
    }
  }
  catch (const WhitedbWriteException& e) {
    logger::get()->error("Exception while writing record fields: {}", e.what());
    throw e;
  }

  return rec;
}

std::unique_ptr<Query> Database::query(const QueryParameterList& qpl)
{
  std::unique_ptr<Query> qry {new Query(_handle, qpl)};
  try {
    qry->make();
  }
  catch (const WhitedbQueryException& e) {
    logger::get()->error("Exception on attempting to create a query: {}", e.what());
    throw e;
  }

  return qry;
}

Database Database::create_local(const int size_in_bytes)
{
  void* dbh = wg_attach_local_database(static_cast<wg_int>(size_in_bytes));
  if (!dbh) {
    std::string msg = "Failed to create local database of size " + std::to_string(size_in_bytes) + ".";
    logger::get()->error(msg);
    throw WhitedbCreateException(msg);
  }

  return Database(0, dbh, true, true, false);
}

Database Database::create_shared(const int id, const int size_in_bytes)
{
  std::string dbname = std::to_string(id);
  void* dbh = wg_attach_database(const_cast<char*>(dbname.c_str()), static_cast<wg_int>(size_in_bytes));
  if (!dbh) {
    std::string msg = "Failed to create database with ID " + dbname + ", of size " + std::to_string(size_in_bytes) + ".";
    logger::get()->error(msg);
    throw WhitedbCreateException(msg);
  }

  return Database(id, dbh, false, true, false);
}

Database Database::attach(const int id)
{
  std::string dbname = std::to_string(id);
  void* dbh = wg_attach_existing_database(const_cast<char*>(dbname.c_str()));
  if (!dbh) {
    std::string msg = "Failed to attach to existing database with ID " + dbname + ".";
    logger::get()->error(msg);
    throw WhitedbAttachException(msg);
  }

  return Database(id, dbh, false, true, false);
}

} // namespace whitedb
} // namespace archive_reader
