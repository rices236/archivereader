/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/whitedb_writer.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/whitedb_writer.hpp"

#include "archive_reader/release.hpp"
#include "archive_reader/logger/logger.hpp"
#include "archive_reader/whitedb/exception.hpp"
#include "archive_reader/whitedb/record/one_to_many_ordered_string_record.hpp"
#include "archive_reader/whitedb/record/release.hpp"
#include "archive_reader/whitedb/record/track.hpp"

namespace archive_reader {
namespace whitedb {

WhitedbWriter::WhitedbWriter(Database& db)
  : _db {db}
{
}

WhitedbWriter::~WhitedbWriter()
{
}

void WhitedbWriter::write(const Release& rel)
try {
  RecordHandle h_release_rec = write_release(rel);
  write_linked_records<TableID::Artist, TableID::ReleaseArtist>(rel.artists(), h_release_rec);
  write_linked_records<TableID::Performer, TableID:: ReleasePerformer>(rel.performers(), h_release_rec);
  write_linked_records<TableID::Style, TableID::ReleaseStyle>(rel.styles(), h_release_rec);
  write_comments<TableID::ReleaseComment>(rel.comments(), h_release_rec);

  int order = 0;
  for (const Track& track : rel.tracks()) {
    write_track(track, h_release_rec, ++order);
  }
}
catch (const WhitedbWriteException& e) {
  std::string title {rel.title()};
  std::string msg {"Exception while writing database records for release \"" + title + ".\""};
  logger::get()->error(msg);
  throw e;
}

RecordHandle WhitedbWriter::write_release(const Release& rel)
{
  // First add the following first to get their record handles.
  RecordHandle h_composer_rec = write_unique_str_record<TableID::Composer>(rel.composer());
  RecordHandle h_genre_rec = write_unique_str_record<TableID::Genre>(rel.genre());
  RecordHandle h_label_rec = write_unique_str_record<TableID::Label>(rel.label());

  record::Release r_rel(rel);
  r_rel.set_composer(h_composer_rec);
  r_rel.set_genre(h_genre_rec);
  r_rel.set_label(h_label_rec);
  RecordHandle h_rel_rec = _db.write(r_rel);

  return h_rel_rec;
}

void WhitedbWriter::write_track(const Track& track, const RecordHandle& h_release_rec, const int order)
{
  // Add the following first to get their record handles.
  RecordHandle h_composer_rec = write_unique_str_record<TableID::Composer>(track.composer());
  RecordHandle h_genre_rec = write_unique_str_record<TableID::Genre>(track.genre());
  RecordHandle h_label_rec = write_unique_str_record<TableID::Label>(track.label());

  record::Track r_track(track);
  r_track.set_release(h_release_rec);
  r_track.set_order(order);
  r_track.set_composer(h_composer_rec);
  r_track.set_genre(h_genre_rec);
  r_track.set_label(h_label_rec);
  RecordHandle h_track_rec = _db.write(r_track);

  write_linked_records<TableID::Artist, TableID::TrackArtist>(track.artists(), h_track_rec);
  write_linked_records<TableID::Style, TableID::TrackStyle>(track.styles(), h_track_rec);
  write_comments<TableID::TrackComment>(track.comments(), h_track_rec);
}

} // namespace whitedb
} // namespace archive_reader
