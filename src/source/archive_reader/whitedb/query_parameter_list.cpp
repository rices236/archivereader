/*******************************************************************************************************************************
 * src/source/archive_reader/whitedb/query_parameter_list.cpp
 ******************************************************************************************************************************/

#include "archive_reader/whitedb/query_parameter_list.hpp"

namespace archive_reader {
namespace whitedb {

QueryParameterList::QueryParameterList()
  : _params {}
{
}

QueryParameterList::QueryParameterList(const QueryParameterList& other)
  : _params {other._params}
{
}

QueryParameterList::QueryParameterList(QueryParameterList&& other)
  : _params {std::move(other._params)}
{
}

QueryParameterList::~QueryParameterList()
{
}

QueryParameterList& QueryParameterList::operator=(const QueryParameterList& other)
{
  _params = other._params;

  return *this;
}

QueryParameterList& QueryParameterList::operator=(QueryParameterList&& other)
{
  _params = std::move(other._params);

  return *this;
}

} // namespace whitedb
} // namespace archive_reader
