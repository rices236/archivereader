/*******************************************************************************************************************************
 * src/source/archive_reader/logger/logger.cpp
 ******************************************************************************************************************************/

#include <spdlog/sinks/stdout_sinks.h>

#include "archive_reader/exception.hpp"
#include "archive_reader/logger/logger.hpp"

namespace archive_reader {
namespace logger {

void set_format(const LogFormat);
void set_level(const LogLevel);

void init(const LogFormat format, const LogLevel level)
try {
  logger_impl::_initialized = true;
  spdlog::stdout_logger_mt(logger_impl::_std_logger);
  spdlog::stderr_logger_mt(logger_impl::_err_logger);
  set_format(format);
  set_level(level);
  get()->info("Logging initialized.");
}
catch (const ArchiveReaderException& e) {
  throw e;
}
catch (const std::exception& e) {
  throw LogInitializationException("Exception on log initialization: ", e);
}

LogLevel level() noexcept
{
  return logger_impl::_level;
}

bool is_off() noexcept
{
  return logger_impl::_level == LogLevel::Off;
}

bool is_at_least(const LogLevel level) noexcept
{
  return logger_impl::_level >= level;
}

void set_format(const LogFormat format)
try {
  switch (format) {
  case LogFormat::Default:
    spdlog::set_pattern("[%Y-%m-%d %H:%M:%S.%f] [%n] [%l] %v");
    break;
  case LogFormat::Debug:
    spdlog::set_pattern("[%n] [%l] %v");
    break;
  case LogFormat::Json:
    spdlog::set_pattern(
      "{\"source\":\"%n\","
      "\"details\":{"
      "\"level\":\"%l\","
      "\"name\":\"%n\","
      "\"timestamp\":\"%Y-%m-%d %H:%M:%S.%f\","
      "\"message\":\"%v\"}}");
    break;
  default:
    throw "LogFormat option not implemented";
  }
}
catch (const std::exception& e) {
  throw LogInitializationException("Exception on setting log format: ", e);
}

void set_level(const LogLevel level)
try {
  logger_impl::_level = level;
  switch (level) {
  case LogLevel::Off:
    spdlog::get(logger_impl::_std_logger)->set_level(spdlog::level::off);
    spdlog::get(logger_impl::_err_logger)->set_level(spdlog::level::off);
    break;
  case LogLevel::Critical:
    spdlog::get(logger_impl::_std_logger)->set_level(spdlog::level::critical);
    spdlog::get(logger_impl::_err_logger)->set_level(spdlog::level::critical);
    break;
  case LogLevel::Error:
    spdlog::get(logger_impl::_std_logger)->set_level(spdlog::level::err);
    spdlog::get(logger_impl::_err_logger)->set_level(spdlog::level::err);
    break;
  case LogLevel::Warn:
    spdlog::get(logger_impl::_std_logger)->set_level(spdlog::level::warn);
    spdlog::get(logger_impl::_err_logger)->set_level(spdlog::level::warn);
    break;
  case LogLevel::Info:
    spdlog::get(logger_impl::_std_logger)->set_level(spdlog::level::info);
    spdlog::get(logger_impl::_err_logger)->set_level(spdlog::level::info);
    break;
  case LogLevel::Debug:
    spdlog::get(logger_impl::_std_logger)->set_level(spdlog::level::debug);
    spdlog::get(logger_impl::_err_logger)->set_level(spdlog::level::debug);
    break;
  case LogLevel::Trace:
    spdlog::get(logger_impl::_std_logger)->set_level(spdlog::level::trace);
    spdlog::get(logger_impl::_err_logger)->set_level(spdlog::level::trace);
    break;
  default:
    throw LogInitializationException("LogLevel option not implemented.");
  }
}
catch (const ArchiveReaderException& e) {
  throw e;
}
catch (const std::exception& e) {
  throw LogInitializationException("Exception on setting log level: ", e);
}

} // namespace logger
} // namespace archive_reader
