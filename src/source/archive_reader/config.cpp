/*******************************************************************************************************************************
 * src/source/archive_reader/config.cpp
 ******************************************************************************************************************************/

#include "archive_reader/config.hpp"

#include <boost/program_options.hpp>

namespace bpo = boost::program_options;

namespace archive_reader {

std::string Config::_release_file_default {"release.json"};
int Config::_db_id_default {1001};
int Config::_db_size_default {0x01000000};
std::string Config::_service_ip_default {"localhost"};
int Config::_service_port_default {8013};

Config::Config(int argc, char** argv)
  : _library_root {},
    _release_file {},
    _db_id {},
    _db_size {},
    _service_ip {},
    _service_port {}
{
  bpo::options_description desc("Allowed options");
  desc.add_options()
  ("help", "show help message")
  ("library-root,l", bpo::value<std::string>(), "library root path")
  ("release-file,r", bpo::value<std::string>(), "case-sensitive name of release files")
  ("db-id,d", bpo::value<int>(), "WhiteDB database identifier")
  ("db-size,s", bpo::value<int>(), "WhiteDB database size in bytes")
  ("service-ip,i", bpo::value<std::string>(), "service IP address")
  ("service-port,p", bpo::value<int>(), "service port number")
  ;

  bpo::variables_map vm;
  bpo::store(bpo::parse_command_line(argc, argv, desc), vm);
  bpo::notify(vm);

  _library_root = (vm.count("library-root")) ? vm["library-root"].as<std::string>() : "";
  _release_file = (vm.count("release-file")) ? vm["release-file"].as<std::string>() : Config::_release_file_default;
  _db_id = (vm.count("db-id")) ? vm["db-id"].as<int>() : Config::_db_id_default;
  _db_size = (vm.count("db-size")) ? vm["db-size"].as<int>() : Config::_db_size_default;
  _service_ip = (vm.count("service-ip")) ? vm["service-ip"].as<std::string>() : Config::_service_ip_default;
  _service_port = (vm.count("service-port")) ? vm["service-port"].as<int>() : Config::_service_port_default;
}

Config::Config(const Config& other)
  : _library_root {other._library_root},
    _release_file {other._release_file},
    _db_id {other._db_id},
    _db_size {other._db_size},
    _service_ip {other._service_ip},
    _service_port {other._service_port}
{
}

Config::Config(Config&& other)
  : _library_root {std::move(other._library_root)},
    _release_file {std::move(other._release_file)},
    _db_id {std::move(other._db_id)},
    _db_size {std::move(other._db_size)},
    _service_ip {std::move(other._service_ip)},
    _service_port {std::move(other._service_port)}
{
}

Config& Config::operator=(const Config& other)
{
  _library_root = other._library_root;
  _release_file = other._release_file;
  _db_id = other._db_id;
  _db_size = other._db_size;
  _service_ip = other._service_ip;
  _service_port = other._service_port;

  return *this;
}

Config& Config::operator=(Config&& other)
{
  _library_root = std::move(other._library_root);
  _release_file = std::move(other._release_file);
  _db_id = std::move(other._db_id);
  _db_size = std::move(other._db_size);
  _service_ip = std::move(other._service_ip);
  _service_port = std::move(other._service_port);

  return *this;
}

std::string Config::library_root() const noexcept
{
  return _library_root;
}

std::string Config::release_file() const noexcept
{
  return _release_file;
}

int Config::db_id() const noexcept
{
  return _db_id;
}

int Config::db_size() const noexcept
{
  return _db_size;
}

std::string Config::service_ip() const noexcept
{
  return _service_ip;
}

int Config::service_port() const noexcept
{
  return _service_port;
}

std::string Config::release_file_default() noexcept
{
  return _release_file_default;
}

int Config::db_id_default() noexcept
{
  return _db_id_default;
}

int Config::db_size_default() noexcept
{
  return _db_size_default;
}

std::string Config::service_ip_default() noexcept
{
  return _service_ip_default;
}

int Config::service_port_default() noexcept
{
  return _service_port_default;
}

} // namespace archive_reader
