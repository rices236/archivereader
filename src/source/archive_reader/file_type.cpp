/*******************************************************************************************************************************
 * src/source/archive_reader/file_type.cpp
 ******************************************************************************************************************************/

#include "archive_reader/file_type.hpp"

#include <unordered_map>

namespace archive_reader {

FileType find_file_type_for_extension(const std::string&) noexcept;

FileType file_type_from_path(const boost::filesystem::path& path) noexcept
{
  return find_file_type_for_extension(path.extension().c_str());
}

FileType file_type_from_extension(const std::string& file_extension) noexcept
{
  return find_file_type_for_extension(file_extension);
}

std::string file_type_to_string(const FileType file_type) noexcept
{
  switch (file_type) {
  case FileType::Flac:
    return "Flac";
  case FileType::Jpeg:
    return "Jpeg";
  case FileType::Json:
    return "Json";
  case FileType::Wave:
    return "Wave";
  case FileType::Mp3:
    return "Mp3";
  default:
    return "Other";
  }
}

typedef std::unordered_map<std::string, FileType> file_type_map;

inline static const file_type_map _ftmap = {
  {".flac", FileType::Flac}, {".FLAC", FileType::Flac},
  {".jpeg", FileType::Jpeg}, {".JPEG", FileType::Jpeg}, {".jpg", FileType::Jpeg}, {".JPG", FileType::Jpeg},
  {".json", FileType::Json}, {".JSON", FileType::Json},
  {".wave", FileType::Wave}, {".WAVE", FileType::Wave}, {".wav", FileType::Wave}, {".WAV", FileType::Wave},
  {".mp3", FileType::Mp3}, {".MP3", FileType::Mp3}
};

FileType find_file_type_for_extension(const std::string& ext) noexcept
{
  const file_type_map::const_iterator it = _ftmap.find(ext);
  if (it != _ftmap.cend()) {

    return it->second;
  }

  return FileType::Other;
}

} // namespace archive_reader
