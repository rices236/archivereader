/*******************************************************************************************************************************
 * src/source/archive_reader/service/info_service_impl.cpp
 ******************************************************************************************************************************/

#include "archive_reader/service/info_service_impl.hpp"

#include <memory>

#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>
#include <grpcpp/ext/proto_server_reflection_plugin.h>

#include "archive_reader/logger/logger.hpp"
#include "archive_reader/service/util.hpp"

namespace archive_reader {
namespace service {

InfoServiceImpl::InfoServiceImpl(const std::string& db_id)
  : _db_id {db_id}
{
}

grpc::Status InfoServiceImpl::DbInfo(grpc::ServerContext* ctx, const DbInfoRequest* req, DbInfoResponse* rep)
{
  auto db = attached_db(_db_id);
  if (!db) {

    return grpc::Status(grpc::StatusCode::INTERNAL, "Failed to attach database.");
  }

  rep->set_id(std::to_string(db->id()));
  rep->set_shared(!db->is_local());
  rep->set_size(db->size());
  rep->set_free(db->free_size());

  return grpc::Status::OK;
}

} // namespace service;
} // namespace archive_reader;
