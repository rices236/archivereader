/*******************************************************************************************************************************
 * src/source/archive_reader/service/service.cpp
 ******************************************************************************************************************************/

#include "archive_reader/service/service.hpp"

#include <grpcpp/health_check_service_interface.h>
#include <grpcpp/ext/proto_server_reflection_plugin.h>

#include "archive_reader/logger/logger.hpp"

namespace archive_reader {
namespace service {

Service::Service(const ConfigInterface& config)
  : _db_id {std::to_string(config.db_id())},
    _ip_address {config.service_ip()},
    _port {config.service_port()},
    _info_svc {},
    _search_svc {},
    _server {}
{
}

void Service::init()
{
  _info_svc.reset(new InfoServiceImpl(_db_id));
  _search_svc.reset(new SearchServiceImpl(_db_id));

  grpc::EnableDefaultHealthCheckService(true);
  grpc::reflection::InitProtoReflectionServerBuilderPlugin();
  grpc::ServerBuilder builder;
  std::string address {_ip_address + std::string(":") + std::to_string(_port)};
  builder.AddListeningPort(address, grpc::InsecureServerCredentials());
  builder.RegisterService(_info_svc.get());
  builder.RegisterService(_search_svc.get());
  _server = builder.BuildAndStart();
  logger::get()->info("gRPC service will be listening on {}.", address);
}

void Service::wait()
{
  if (!_server) {
    logger::get()->warn("Attempted wait from uninitialized server.");

    return;
  }

  _server->Wait();
}

void Service::shutdown()
{
  if (!_server) {
    logger::get()->warn("Attempted shutdown from uninitialized server.");

    return;
  }

  logger::get()->info("Initiating service shutdown.");

  _server->Shutdown();
}

void Service::set_serving_status(const bool status) noexcept
{
  if (!_server) {
    logger::get()->warn("Attempted to set serving status from uninitialized server.");

    return;
  }

  grpc::HealthCheckServiceInterface* hcs = _server->GetHealthCheckService();
  if (hcs == nullptr) {
    logger::get()->warn("No health check service interface for setting status.");

    return;
  }

  logger::get()->info("Setting service status to {}.", status ? "SERVING" : "NOT SERVING");

  hcs->SetServingStatus(status);
}

} // namespace service;
} // namespace archive_reader;
