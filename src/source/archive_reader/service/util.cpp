/*******************************************************************************************************************************
 * src/source/archive_reader/service/util.cpp
 ******************************************************************************************************************************/

#include "archive_reader/service/util.hpp"

#include "archive_reader/logger/logger.hpp"

namespace archive_reader {
namespace service {

std::unique_ptr<archive_reader::whitedb::Database> attached_db(const std::string& db_id) noexcept
{
  std::unique_ptr<archive_reader::whitedb::Database> pdb;

  try {
    archive_reader::whitedb::Database db = archive_reader::whitedb::Database::attach(std::stoi(db_id));

    pdb.reset(new archive_reader::whitedb::Database(db));
  }
  catch (const archive_reader::whitedb::WhitedbAttachException& e) {
    logger::get()->warn(e.what());
  }

  return pdb;
}

} // namespace service;
} // namespace archive_reader;
