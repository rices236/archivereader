/*******************************************************************************************************************************
 * src/source/archive_reader/service/search_service_impl.cpp
 ******************************************************************************************************************************/

#include "archive_reader/service/search_service_impl.hpp"

#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>
#include <grpcpp/ext/proto_server_reflection_plugin.h>

namespace archive_reader {
namespace service {

SearchServiceImpl::SearchServiceImpl(const std::string& db_id)
  : _db_id {db_id}
{
}

grpc::Status SearchServiceImpl::Search(grpc::ServerContext* ctx, const SearchRequest* req, SearchResponse* rep)
{

  return grpc::Status::OK;
}

} // namespace service;
} // namespace archive_reader;
