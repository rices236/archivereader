/*******************************************************************************************************************************
 * src/source/archive_reader/track.cpp
 ******************************************************************************************************************************/

#include "archive_reader/track.hpp"

#include "archive_reader/exception.hpp"
#include "archive_reader/json_helper.hpp"

namespace bfs = boost::filesystem;

using nlohmann::json;

namespace archive_reader {

const char* const title = "title";
const char* const path = "path";
const char* const artist = "artist";
const char* const artists = "artists";
const char* const composer = "composer";
const char* const genre = "genre";
const char* const style = "style";
const char* const styles = "styles";
const char* const label = "label";
const char* const originalYear = "originalYear";
const char* const performanceYear = "performanceYear";
const char* const comment = "comment";
const char* const comments = "comments";


Track::Track()
  : _title {},
    _path {},
    _artists {},
    _composer {},
    _genre {},
    _styles {},
    _label {},
    _original_year {},
    _performance_year {},
    _comments {},
    _track_file_path {}
{
}

std::string Track::title() const noexcept
{
  return _title;
}

std::string Track::path() const noexcept
{
  return _path;
}

str_list Track::artists() const noexcept
{
  return _artists;
}

std::string Track::composer() const noexcept
{
  return _composer;
}

std::string Track::genre() const noexcept
{
  return _genre;
}

str_list Track::styles() const noexcept
{
  return _styles;
}

std::string Track::label() const noexcept
{
  return _label;
}

int Track::original_year() const noexcept
{
  return _original_year;
}

int Track::performance_year() const noexcept
{
  return _performance_year;
}

str_list Track::comments() const noexcept
{
  return _comments;
}

std::string Track::track_file_path() const noexcept
{
  return _track_file_path;
}

void from_json(const json& j, Track& t)
{
  t._title = required_str_field(j, title);
  t._path = required_str_field(j, path);
  t._artists = optional_plural_str_field(j, artist, artists);
  t._composer = optional_str_field(j, composer);
  t._genre = optional_str_field(j, genre);
  t._styles = optional_plural_str_field(j, style, styles);
  t._label = optional_str_field(j, label);
  t._original_year = optional_uint_field(j, originalYear, 0);
  t._performance_year = optional_uint_field(j, performanceYear, 0);
  t._comments = optional_plural_str_field(j, comment, comments);
}

void from_path(const boost::filesystem::path& path, Track& t)
{
  bfs::path t_path = path.parent_path() / bfs::path(t._path);
  t._track_file_path = t_path.lexically_normal().c_str();
}

std::ostream& operator<<(std::ostream& os, const Track& t)
{
  os << "Track: { " << t._title << ", "
     << t._path
     << " }";

  return os;
}

} // namespace archive_reader
