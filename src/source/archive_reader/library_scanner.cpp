/*******************************************************************************************************************************
 * src/source/archive_reader/library_scanner.cpp
 ******************************************************************************************************************************/

#include "archive_reader/library_scanner.hpp"

#include "archive_reader/file_operations.hpp"
#include "archive_reader/logger/logger.hpp"
#include "archive_reader/release_scan_handler.hpp"
#include "archive_reader/whitedb/database.hpp"
#include "archive_reader/whitedb/whitedb_writer.hpp"

namespace archive_reader {

LibraryScanner::LibraryScanner(const ConfigInterface& config)
  : _library_root {config.library_root()},
    _release_file {config.release_file()},
    _db_id {config.db_id()},
    _db_size {config.db_size()}
{
}

void LibraryScanner::scan()
{
  logger::get()->info("Creating WhiteDB shared database with ID {}, size of {} bytes.", _db_id, _db_size);
  whitedb::Database db = whitedb::Database::create_shared(_db_id, _db_size);
  whitedb::WhitedbWriter dbwriter(db);
  ReleaseScanHandler handler(_library_root, _release_file, &dbwriter);

  logger::get()->info("Beginning scan; searching in {} for release files {}.", _library_root, _release_file);
  scan_recursive(boost::filesystem::path(_library_root), handler);
  std::string err_msg {"there were no errors"};
  if (handler.err_count() == 1) {
    err_msg = "there was 1 error";
  }
  else if (handler.err_count() > 1) {
    err_msg = "there were " + std::to_string(handler.err_count()) + " errors";
  }
  logger::get()->info("Scan completed: {} releases were scanned; {}.", handler.scan_count(), err_msg);
}

} // namespace archive_reader
